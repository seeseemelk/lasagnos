#!/bin/lua
require("makel.makel")
require("makel.c")
require("makel.nasm")

-- Configurable settings.
var.iso = "lasagnos.iso"

-- Builds settings.
dir.src = "src"
dir.kernel = "src/kernel"
dir.libc = "src/libc"
dir.lua = "src/lua"
dir.usr = "src/usr"
dir.build = "build"
dir.iso = "isodir"
dir.initrd = fs.concat(dir.build, "initrd")

var.c.cc = "i386-elf-gcc"
var.c.ld = "i386-elf-ld"
var.c.ar = "i386-elf-ar"
var.c.cflags = "-Wall -Werror -Wextra -ffreestanding -std=gnu11 -nostdlib -ggdb"
--var.c.ldexeflags = "-ffreestanding -std=gnu11 -nostdlib -static-libgcc"
var.c.ldexeflags = "-static-libgcc"
var.c.ldscript = "src/kernel/linker.ld"
var.c.libraries = {"gcc"}
var.c.includes = {
	dir.kernel,
	dir.libc,
	dir.lua
}
var.programs = {}
var.initrd = {}
var.initrd.files = {} -- Files to include in the archive
var.initrd.names = {} -- The names the files should have in the archive.
var.initrd.tar = fs.concat(dir.build, "initrd.tar")
var.ulibc = fs.concat(dir.build, "ulibc.a")
var.crt = fs.concat(dir.build, "crt.o")

var.objects = {}
var.kernel = nil --The kernel.elf file

tasks.new("build", "Builds kernel.elf")
tasks.new("build_initrd", "Builds the initrd.tar archive")
tasks.new("build_iso", "Builds a bootable ISO image")

tasks.add("build", function()
	tasks.run("build_programs")
	tasks.run("generate_all_lua")
	tasks.run("build_kernel")
	tasks.run("build_libc")
	tasks.run("build_lua")
	var.kernel = action.c.linkExe(fs.concat(dir.build, "kernel.elf"), var.objects)
	tasks.run("build_initrd")
end)

tasks.add("build_iso", function()
	tasks.run("build")
	
	local kernelFile = fs.concat(dir.iso, "boot/grub/kernel.elf")
	local initrdFile = fs.concat(dir.iso, "boot/grub/initrd.tar")
	local grubFile = fs.concat(dir.iso, "boot/grub/grub.cfg")

	action.cp(var.kernel, kernelFile)
	action.cp(var.initrd.tar, initrdFile)

	if fs.needsBuild(var.iso, {kernelFile, initrdFile, grubFile}) then
		action.execute("grub-mkrescue -o", var.iso, dir.iso)
	end
end)

tasks.add("test", function()
	print("CC: " .. var.c.cc)
	local overlay = {
		c = {
			cc = "wow"
		}
	}
	tasks.withVar(overlay, function()
		print("Overlayed: " .. var.c.cc)
	end)
end)

makel(...)
