# Introduction
LasagnOS is a 32-bit x86 operating system that is primarily written in the high-level language Lua.

# Building
In order to build the operating system, make sure an `i386-elf-gcc` cross-compiler and compatible binutils are installed.

In order to compile, run the following command in the project root:
```bash
./make.lua build
```
A file called kernel.elf will be placed in the build directory.

In order to build an iso image, run the following command:
```bash
./make.lua buildiso
```
