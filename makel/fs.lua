local lfs = require("lfs")
local fs = {}

-- Import lfs functions.
for name, value in pairs(lfs) do
	fs[name] = value
end

--
-- Modified lfs functions
--
local oldList = fs.list
function fs.list(path)
	if not path or #path == 0 then
		path = "."
	end
	return oldList(path)
end

--
-- Functions added by makel.
--
local fileDates = {}
local newFileDates = {}
local fileDependencies = {}
local fileUpdatedByTask = {}

function fs._load()
	-- Load file modification times.
	local cache = fs.concat(dir.build, "filecache")
	if fs.exists(cache) then
		for line in io.lines(cache) do
			local file, time = line:match("^([%w%p]-): (%d+)$")
			fileDates[file] = tonumber(time)
			newFileDates[file] = fileDates[file]
		end
	end

	-- Load file dependencies.
	local depends = fs.concat(dir.build, "depends")
	if fs.exists(depends) then
		for line in io.lines(depends) do
			--local file = line:match("^([%w%p]-): ")
			--local file, dependency = line:match(": ([%w%p]+)$")
			local file, dependency = line:match("^([%w%p]-): ([%w%p]*)$")
			if #dependency == 0 then
				fileDependencies[file] = {}
			else
				fileDependencies[file] = fileDependencies[file] or {}
				fileDependencies[file][#fileDependencies[file] + 1] = dependency
			end
		end
	end

	tasks.add("clean", function()
		fileDates = {}
		newFileDates = {}
		fileDependencies = {}
		fileUpdatedByTask = {}
	end)
end

function fs._unload()
	if fs.exists(dir.build) then
		-- Save file modification times.
		local cache = fs.concat(dir.build, "filecache")
		local fh = assert(io.open(cache, "w"))
		for file, time in pairs(newFileDates) do
			if not tasks.currentTask or fileUpdatedByTask[file] ~= tasks.currentTask then
				fh:write(file .. ": " .. time .. "\n")
			elseif fileDates[file] then
				fh:write(file .. ": " .. fileDates[file] .. "\n")
			end
		end
		fh:close()

		-- Save file dependencies.
		local depends = fs.concat(dir.build, "depends")
		fh = assert(io.open(depends, "w"))
		for source, dependencies in pairs(fileDependencies) do
			if #dependencies > 0 then
				for _, dependency in ipairs(dependencies) do
					fh:write(source .. ": " .. dependency .. "\n")
				end
			else
				fh:write(source .. ": \n")
			end
		end
		fh:close()
	end
end

--- Concatenates directory and filenames to each other
-- and returns a single path string.
-- @param ... Each path argument as a string.
function fs.concat(...)
	local args = {...}
	assert(#args >= 2, "fs.concat(...) needs at least two arguments")

	local parts = {}
	for i, v in ipairs(args) do
		if type(v) == "string" then
			parts[#parts + 1] = v:match("^/?(.-)/?$")
		else
			error("Argument " .. i .. " must be a string (was a " .. type(v) .. ")")
		end
	end
	return table.concat(parts, "/")
end

--- Returns the parent directory of a given path.
-- @param path The path for which to return the parent.
-- @return The parent directory of the directory.
function fs.parent(path)
	if path:find("/") == -1 then
		return ""
	else
		return path:match("^(.*)/(.-)$")
	end
end

--- Turns a path into an absolute path (relative to the root directory).
-- In other words, if the path does not start with dir.current, it
-- prefixes it with dir.current.
function fs.makeAbsolute(path)
	if path:find(dir.current, 0, true) then
		return path
	else
		return fs.concat(dir.current, path)
	end
end

--- Strips a prefix from a set of paths.
-- The output paths will also NEVER begin with a '/'
-- @param dir The prefix to remove from the paths.
-- @param paths The paths to strip it from.
-- @return The paths with the directory stripped from it.
function fs.strip(dir, paths)
	assert(type(dir) == "string", "Directory must be a string")
	assert(type(paths) == "string" or type(paths) == "table", "Paths must be either a string or a table")

	local newPaths = {}

	if dir:sub(-1) ~= "/" then
		dir = dir .. "/"
	end

	if type(paths) == "table" then
		for _, path in ipairs(paths) do
			newPaths[#newPaths + 1] = path:gsub("^"..dir, "") --:sub(#dir + 1)
		end
		return newPaths
	else
		return paths:gsub("^"..dir, "") --:sub(#dir + 1)
	end
end

--- Reads the entire contents of a file and returns
-- it as a single long string.
-- @param file The file to read.
-- @return The contents of the file as a string.
function fs.readContents(file)
	local fh = action.assert(io.open(file, "r"))
	local data = action.assert(fh:read("*a"))
	action.assert(fh:close())
	return data
end

--- Finds a list of files in a directory.
-- @param path The directory to search in.
-- @param filter A lua pattern that matches each file.
-- @param matches[opt] A table in which each result will be stored.
-- @param recurse[opt] A boolean that can be set to true to
-- search for files recursively.
-- @return A table of all matches.
function fs.find(path, filter, matches, recurse)
	if type(matches) == "boolean" then
		recurse = matches
		matches = {}
	end

	matches = matches or {}
	local iter, dir = fs.dir(path)
	local file = dir:next()
	while file do
		local filepath = fs.concat(path, file)
		if filepath:match(filter) then
			matches[#matches+1] = filepath
		end
		if recurse and fs.attributes(filepath).mode == "directory"
				and file ~= "." and file ~= ".." then
			fs.find(filepath, filter, matches, recurse)
		end
		file = dir:next()
	end
	dir:close()
	return matches
end

--- Checks if the files have been changed since the last invocation
-- of makel. Calling this function will automatically track the file
-- for this invocation of makel.
-- @param files The files to check.
-- @return true if a file has been changed, false if none have changed.
function fs.changed(files)
	if type(files) == "string" then
		files = {files}
	end

	local changed = false
	for _, file in ipairs(files) do
		file = fs.strip(fs.currentdir(), file)
		local attr = fs.attributes(file)
		local abstractName = file
		if var.fsprefix then
			abstractName = var.fsprefix .. ";" .. file
		end
		if not attr then
			changed = true
			newFileDates[abstractName] = nil
			fileUpdatedByTask[abstractName] = tasks.currentTask
		else
			local modtime = attr.modification
			if not fileDates[abstractName] or modtime > fileDates[abstractName] then
				changed = true
			end
			newFileDates[abstractName] = modtime
			fileUpdatedByTask[abstractName] = tasks.currentTask
		end
	end
	return changed
end

--- Check if a file exists.
-- @param file The file to check for.
-- @return true if the file exists, false if it doens't.
function fs.exists(file)
	local attr = fs.attributes(file)
	if attr then
		return true
	else
		return false
	end
end

--- Checks if a given file needs to be built.
-- @param file The file to check if it needs to be built.
-- @param depends Files the main file depends on.
-- @return true if the file needs to be build, false if
-- it does not need to be built.
function fs.needsBuild(file, depends)
	return fs.changed(depends) or not fs.exists(file)
end

--- Sets the dependencies of a source file.
-- @param source The source file.
-- @param dependencies The files it depends on.
function fs.setDependencies(source, dependencies)
	if var.fsprefix then
		source = var.fsprefix .. ";" .. source
	end

	fileDependencies[source] = dependencies
	fileUpdatedByTask[source] = tasks.currentTask
end

--- Gets the dependencies of a source file.
-- @param source The source file.
-- @param depends[opt] A table in which to store the dependencies.
-- @return A table of all dependencies of the file.
-- This does not include the source file itself.
-- Can be nil if there are no recorded dependencies.
function fs.getDependencies(source, depends)
	if var.fsprefix then
		source = var.fsprefix .. ";" .. source
	end

	depends = depends or {}
	for _, depend in ipairs(fileDependencies[source]) do
		depends[#depends + 1] = depend
	end
	return depends
end

--- Check if the dependencies need to be recheck for a given source file.
-- @param source The source file.
-- @return true if the dependencies should be check again, false
-- if the dependencies from getDependencies() are still good.
function fs.needsDependencyCheck(source)
	local prefixed = source
	if var.fsprefix then
		prefixed = var.fsprefix .. ";" .. source
	end

	if not fileDependencies[prefixed] then
		return true
	end

	if fs.changed(source) then
		return true
	end

	for _, depend in ipairs(fileDependencies[prefixed]) do
		if fs.changed(depend) then
			return true
		end
	end

	return false
end

return fs
