-- Adds actions useful for C development.
action.c = {}

-- Adds C specific var options
var.c = {}
var.c.cc = "gcc"
var.c.ld = "ld"
var.c.ar = "ar"
var.c.cflags = ""
var.c.ldflags = ""
var.c.ldexeflags = ""
var.c.ldsccript = ""
var.c.includes = {}
var.c.libraries = {}

--- Invokes the C compiler.
function action.c.cc(...)
	return action.executeOutput(var.c.cc, var.c.cflags, ...)
end

--- Invokes the linker.
function action.c.ld(...)
	return action.executeOutput(var.c.ld, var.c.ldflags, ...)
end

--- Invokes the archiver.
function action.c.ar(...)
	return action.executeOutput(var.c.ar, ...)
end

--- Invokes the linker for exe generation (uses var.c.ccc).
function action.c.ldexe(...)
	return action.executeOutput(var.c.cc, var.c.cflags, {...}, var.c.ldflags, var.c.ldexeflags)
end

--- Finds C source files.
-- @param path The path to find files in.
-- @param files[opt] A table to store all found files in.
function action.c.findSources(path, files)
	assert(type(path) == "string", "Search path has to be a string")
	files = files or {}

	fs.find(path, "%.c$", files)
	return files
end

--- Finds dependencies for a C file.
-- @param file The file for which to find dependencies.
function action.c.findDepends(file, depends, flags)
	depends = depends or {}
	local output = action.c.cc(flags or "", "-MM", file)
	for header in output:gmatch("([%w%p]+%.h)") do
		depends[#depends + 1] = header
	end
	return depends
end

--- Builds a C file.
-- @param file The file or files to build.
-- @param objects A table in which to store a list of all generated object files.
-- @return The objects table.
function action.c.build(file, objects, options)
	options = options or {}
	objects = objects or {}
	if type(file) == "table" then
		for _, f in ipairs(file) do
			action.c.build(f, objects, options)
		end
	else
		-- Prepare the include statements.
		local includes = {}
		for _, include in ipairs(options.includes or var.c.includes) do
			includes[#includes + 1] = "-I" .. include
		end

		-- Prepare the define statements.
		local defines = {}
		if options.defines then
			assert(type(options.defines), "Defines are not a table")
			for _, define in ipairs(options.defines) do
				defines[#defines + 1] = "-D" .. define
			end
		end

		-- Resolve all dependencies.
		if fs.needsDependencyCheck(file) then
			local flags = table.concat(includes, " ") .. " " .. table.concat(defines, " ") .. " " .. (options.cflags or "")
			fs.setDependencies(file, action.c.findDepends(file, {}, flags))
		end

		local depends = {file}
		fs.getDependencies(file, depends)

		-- Find the directories it will be build in.
		local outputBaseDir = options.build or dir.build
		local outputPath = fs.concat(outputBaseDir, file):match("^(.+%.c)$") .. ".o"
		local outputDir = fs.parent(outputPath)
		objects[#objects + 1] = outputPath

		-- Build the file.
		if fs.needsBuild(outputPath, depends) then
			action.mkdir(outputDir)
			action.c.cc("-c", "-o", outputPath, defines, includes, options.cflags or {}, file)
		end
	end
	return objects
end

--- Links several object files into a single object file.
function action.c.linkObj(output, objects)
	assert(type(output) == "string", "Output path must be a string")
	assert(type(objects) == "table", "Objects argument must be a table")

	if fs.needsBuild(output, objects) then
		print("Linking to object " .. output)
		action.c.ld("-o", output, "-r", objects)
	end
	return output
end

--- Links several object files into a single executable file.
function action.c.linkExe(output, objects)
	assert(type(output) == "string", "Output path must be a string")
	assert(type(objects) == "table", "Objects argument must be a table")

	local linkerChanged = false
	if var.c.ldscript and fs.changed(var.c.ldscript) then
		linkerChanged = true
	end

	if fs.needsBuild(output, objects) or linkerChanged then
		print("Linking to exe " .. output)

		local libraries = {}
		for _, lib in ipairs(var.c.libraries) do
			libraries[#libraries + 1] = "-l" .. lib
		end

		if var.c.ldscript then
			action.c.ldexe("-T", var.c.ldscript, "-o", output, objects, libraries)
		else
			action.c.ldexe("-o", output, objects, libraries)
		end
	end
	return output
end

--- Uses xxd to convert a file into a header file.
function action.c.toHeader(file, output, dir)
	-- xxd's header variables are dependend on the
	-- directory xxd is executed at.
	local absFile = file:sub(1,1) == "/" and dir or fs.concat(dir, file)
	local absOutput = output:sub(1,1) == "/" and output or fs.concat(dir, output)

	if fs.needsBuild(absOutput, absFile) then
		local currentDir = fs.currentdir()
		if dir then
			fs.chdir(dir)
		end

		if #fs.parent(output) > 0 then
			action.mkdir(fs.parent(output))
		end
		action.execute("xxd -i", file, output)

		if dir then
			fs.chdir(currentDir)
		end
	end
end

--- Turns an object file into a static library.
-- @param output The location the static library will be written to.
-- @param object The object file to convert.
function action.c.staticLib(output, object)
	if fs.needsBuild(output, {object}) then
		action.c.ar("rcs", output, object)
	end
end
