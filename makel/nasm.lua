action.nasm = {}

var.nasm = {}
var.nasm.nasm = "nasm"
var.nasm.format = "elf32"

--- Builds using nasm.
function action.nasm.nasm(...)
	return action.executeOutput(var.nasm.nasm, "-f", var.nasm.format, ...)
end

--- Finds asm source files.
-- @param path The path to find files in.
-- @param files[opt] A table to store all found files in.
function action.nasm.findSources(path, files)
	assert(type(path) == "string", "Search path has to be a string")
	files = files or {}

	fs.find(path, "%.asm$", files)
	return files
end

--- Finds dependencies for a nasm file.
-- @param file The file for which to find dependencies.
function action.nasm.findDepends(file, depends)
	depends = depends or {}
	local output = action.nasm.nasm("-M", file)
	for header in output:gmatch("([%w%p]+%.asm)") do
		depends[#depends + 1] = header
	end
	return depends
end

--- Builds asm file.
-- @param file The file or files to build.
-- @param objects A table in which to store a list of all generated object files.
-- @return The objects table.
function action.nasm.build(file, objects)
	objects = objects or {}
	if type(file) == "table" then
		for _, f in ipairs(file) do
			action.nasm.build(f, objects)
		end
	else
		if fs.needsDependencyCheck(file) then
			fs.setDependencies(file, action.nasm.findDepends(file))
		end

		local depends = {file}
		fs.getDependencies(file, depends)

		local outputPath = fs.concat(dir.build, file):match("^(.+%.asm)$") .. ".o"
		local outputDir = fs.parent(outputPath)
		objects[#objects + 1] = outputPath

		if fs.needsBuild(outputPath, depends) then
			action.mkdir(outputDir)
			action.nasm.nasm("-o", outputPath, file)
		end
	end
	return objects
end
