-- Create modules.
fs = require("makel.fs")
util = require("makel.util")

--- Contains methods for performing actions on tasks and tasklists.
tasks = {
	current = nil
}

--- Contains common actions that can be executed by a task.
action = {}

-- Create required structures.
dir = {
	current = ""
}
settings = {
	noaction = false
}
var = {}

-- Local variables for core operations.
local taskList = {}
local executed = {}
local finalTasks = {}
local taskStack = {}

--- Executes a command. All parameters are concatenated with spaces.
-- Errors are thrown if the command has a non-zero output.
-- @param ... Command parameters.
function action.execute(...)
	local command = table.concat(util.flatten(...), " ")
	print(command)
	if not settings.noaction then
		if not os.execute(command) then
			action.error("Build failed")
		end
	end
end

--- Executes a command. All parameters are concatenated with spaces.
-- The output is captured as a string and returned.
-- @param ... Command parameters.
function action.executeOutput(...)
	local command = table.concat(util.flatten(...), " ")
	print(command)
	local fh = io.popen(command)
	local str = fh:read("*a")
	local status, _, signal = fh:close()
	if not status then
		action.error("Build failed with code " .. tostring(signal))
	end
	return str
end

--- Removes a file.
-- @param file The file to remove.
function action.rm(file)
	action.execute("rm -fv", file)
end

--- Removes a directory.
-- @param dir The directory to remove.
function action.rmdir(dir)
	if fs.exists(dir) then
		action.execute("rm -rfv", dir)
	end
end

--- Creates a directory.
-- @param dir The directory to create.
function action.mkdir(dir)
	if not fs.exists(dir) then
		action.execute("mkdir -p", dir)
	end
end

--- Copies a file.
-- @param src The file to copy.
-- @param dest The location to copy the file to.
function action.cp(src, dest)
	if fs.needsBuild(dest, src) then
		action.execute("cp", src, dest)
	end
end

--- Stops makel.
function action.stop()
	fs._unload()
end

--- Throws an error.
function action.error(...)
	action.stop()
	print("")
	print("Compilation failed during task " .. table.concat(taskStack, "/"))
	error(...)
end

--- Works the same as the normal assert, but uses
-- the makel specific error handlers.
function action.assert(v, message)
	if not v then
		assert(v, message)
	else
		return v
	end
end

--- Creates a new tasklist with a given name and
-- optionally a description.
-- @param name The name to give to the tasklist.
-- @param description[opt] A description to give to the tasklist.
function tasks.new(name, description)
	if not taskList[name] then
		taskList[name] = {
			name = name,
			description = description,
			tasks = {}
		}
	end
end

--- Adds a task to a tasklist.
-- @param name The tasklist to add the task to.
-- @param callback The callback to execute when
-- the task is executed.
function tasks.add(name, callback)
	if not callback then
		callback = description
		description = nil
	end

	if not taskList[name] then
		tasks.new(name)
	end

	local list = taskList[name].tasks
	list[#list + 1] = {
		dir = dir.current,
		callback = callback
	}
end

--- Runs a tasklist.
-- @param name The name of the tasklist to run.
function tasks.run(name)
	if taskList[name] then
		if not executed[name] then
			taskStack[#taskStack + 1] = name
			print(" [" .. table.concat(taskStack, "/") .. "]")

			executed[name] = true
			local currentDir = dir.current
			local currentTask = tasks.currentTask
			for _, task in ipairs(taskList[name].tasks) do
				tasks.currentTask = task
				dir.current = task.dir
				task.callback()
			end
			dir.current = currentDir
			tasks.currentTask = currentTask

			print("![" .. table.concat(taskStack, "/") .. "]")
			taskStack[#taskStack] = nil
		end
	else
		error("Task " .. tostring(name) .. " was not found")
	end
end

--- Runs a task after the current tasklist ended.
-- @param name The name of the task to run.
function tasks.runAfter(name)
	for _, task in ipairs(finalTasks) do
		if task == name then
			return
		end
	end
	finalTasks[#finalTasks+1] = name
end

--- Executes a callback with an overlayed var.
-- @param varOverlay The table overlay to overlay var with.
-- @param callback The function to call with the overlay applied.
function tasks.withVar(varOverlay, callback)
	local oldVar = var
	local overlay = util.overlay(var, varOverlay)

	var = overlay
	callback(oldVar)
	var = oldVar

	return overlay
end

--
-- Default task initialisation.
--
tasks.new("tasks_all", "Show all discovered tasks")
tasks.add("tasks_all", function()
	print("Tasks:")
	for name, task in pairs(taskList) do
		if task.description then
			print(" - " .. task.name .. ": " .. task.description)
		else
			print(" - " .. task.name)
		end
	end
end)

tasks.new("tasks", "Show all discovered tasks that have a description")
tasks.add("tasks", function()
	print("Tasks:")
	for name, task in pairs(taskList) do
		if task.description then
			print(" - " .. task.name .. ": " .. task.description)
		end
	end
end)

tasks.new("discover_makers", "Find make.lua files in subdirectories")
tasks.add("discover_makers", function()
	local oldCurrent = dir.current
	for _, file in ipairs(fs.find(dir.src, "make.lua$", true)) do
		dir.current = fs.parent(file)
		dofile(file)
	end
	dir.current = oldCurrent
end)

tasks.new("setup", "Setups the required directory structure for building")
tasks.add("setup", function()
	action.mkdir(dir.build)
end)

tasks.new("clean", "Clean up any files created by building the project")
tasks.add("clean", function()
	action.rmdir(dir.build)
end)

local function doFinalTasks()
	local tasks = finalTasks
	finalTasks = {}
	for _, task in ipairs(tasks) do
		tasks.run(task)
	end
end

--
-- Main function.
--
function makel(...)
	local doTasks = {...}
	if #doTasks > 0 then
		fs._load()
		tasks.run("discover_makers")
		tasks.run("setup")
		for _, task in ipairs(doTasks) do
			tasks.run(task)
		end
		while #finalTasks > 0 do
			doFinalTasks()
		end
		action.stop()
	else
		error("No tasks")
	end
end
