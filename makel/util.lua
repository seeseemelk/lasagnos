local util = {}

local function doFlatten(flat, tbl)
	for _, v in ipairs(tbl) do
		if type(v) == "table" then
			doFlatten(flat, v)
		else
			flat[#flat + 1] = v
		end
	end
end

function util.flatten(...)
	local flat = {}
	doFlatten(flat, {...})
	return flat
end

function util.concat(dest, ...)
	local args = {...}

	for _, arg in ipairs(args) do
		if type(arg) == "table" then
			for _, v in ipairs(arg) do
				dest[#dest + 1] = v
			end
		else
			dest[#dest + 1] = arg
		end
	end

	return dest
end

local OverlayMT = {}
function util.overlay(original, overlay)
	local tbl = {}
	tbl.__original = original
	tbl.__overlay = overlay
	setmetatable(tbl, OverlayMT)
	return tbl
end

function OverlayMT:__index(name)
	if self.__overlay[name] then
		if type(self.__overlay[name]) == "table" then
			self[name] = util.overlay(self.__original[name], self.__overlay[name])
			return self[name]
		else
			return self.__overlay[name]
		end
	else
		return self.__original[name]
	end
end

return util
