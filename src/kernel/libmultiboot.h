#ifndef LIBMULTIBOOT_H_
#define LIBMULTIBOOT_H_

#include "multiboot.h"
#include <lua.h>

void luaopen_multiboot(lua_State* L, multiboot_info_t* mbd);

#endif /* LIBMULTIBOOT_H_ */
