#include "memory.h"
#include "kernel.h"
#include "interrupt.h"
#include "paging.h"
#include <errno.h>
#include <string.h>
#include <math.h>
#include "../libc/cdefs.h"

#define ENABLE_SAFETY
#define BLOCK_SIZE 16
#define FREE 0

size_t* blocklist;
size_t blocklist_len;
void* mem;

size_t blocklist_last = 0;

void* unsafe_malloc(size_t bytes)
{
	size_t blocks_needed = ceildiv(bytes, BLOCK_SIZE);

	size_t block_test_count = 0;
	size_t block_free_count = 0;
	size_t block_first_free = 0;
	while (block_test_count < blocklist_len)
	{
		if (blocklist[blocklist_last] == FREE)
		{
			if (block_free_count == 0)
				block_first_free = blocklist_last;
			block_free_count++;
			if (block_free_count >= blocks_needed)
				break;
		}
		else
			block_free_count = 0;

		block_test_count++;
		blocklist_last = (blocklist_last + 1) % blocklist_len;
		if (blocklist_last == 0)
			block_free_count = 0;
	}

	if (block_free_count < blocks_needed)
	{
		errno = ENOMEM;
		return NULL;
	}

	for (size_t i = block_first_free; i < block_first_free + blocks_needed; i++)
	{
#ifdef ENABLE_SAFETY
		if (blocklist[i] != 0)
			kernel_panic("Bad alloc");
#endif
		blocklist[i] = blocks_needed - (i - block_first_free);
	}

	return mem + block_first_free * BLOCK_SIZE;
}

void* malloc(size_t bytes)
{
	cli();
	void* addr = unsafe_malloc(bytes);
	sti();
	return addr;
}

void unsafe_free(void* addr)
{
	if (addr == NULL)
		return;

	size_t index = (addr - mem) / BLOCK_SIZE;
#ifdef ENABLE_SAFETY
	if (addr != mem + index * BLOCK_SIZE)
		kernel_panic("Bad memory address in free: 0x%X (expected 0x%X)", addr, mem + index * BLOCK_SIZE);
#endif
	while (blocklist[index] != 1)
		blocklist[index++] = 0;
	blocklist[index] = 0;
	blocklist_last = index;
}

void free(void* addr)
{
	cli();
	unsafe_free(addr);
	sti();
}

void* unsafe_realloc(void* addr, size_t new_size)
{
	if (addr == NULL)
		return unsafe_malloc(new_size);
	else if (new_size == 0)
	{
		unsafe_free(addr);
		return NULL;
	}

	size_t index = (addr - mem) / BLOCK_SIZE;
	size_t old_size = blocklist[index];
	void* new_addr = unsafe_malloc(new_size);
	if (new_addr == NULL)
		return NULL;
	new_size = blocklist[(new_addr - mem) / BLOCK_SIZE];
	memcpy(new_addr, addr, (new_size > old_size ? old_size : new_size) * BLOCK_SIZE);
	unsafe_free(addr);
	return new_addr;
}

void* realloc(void* addr, size_t new_size)
{
	cli();
	addr = unsafe_realloc(addr, new_size);
	sti();
	return addr;
}

void memory_init(size_t len)
{
	blocklist_len = len / (BLOCK_SIZE + 4);

	blocklist = page_allocmem(blocklist_len * 4);
	for (size_t i = 0; i < blocklist_len; i++)
		blocklist[i] = FREE;
	mem = page_allocmem(blocklist_len * BLOCK_SIZE);

	kernel_log("Each block has a size of %d bytes", BLOCK_SIZE);
	kernel_log("Number of blocks: %d", blocklist_len);
	kernel_log("Amount of memory available for kernel: %d MB", (blocklist_len * BLOCK_SIZE) / 1024 / 1024);
	kernel_log("Memory allocations start at: 0x%X", mem);
}






