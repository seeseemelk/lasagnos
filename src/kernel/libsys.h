#ifndef LIBSYS_H_
#define LIBSYS_H_
#include <lua.h>

void luaopen_sys(lua_State* L);
void interrupt_handle(int irq, int code);

#endif /* LIBSYS_H_ */
