#include "libpmem.h"
#include "pmem.h"
#include <lua.h>
#include <lauxlib.h>

#define lua_setconstant(L, n, c) \
	lua_pushinteger(L, c); \
	lua_setfield(L, -2, n)

static int l_alloc2(lua_State* L)
{
	size_t amount = luaL_checkinteger(L, 1);
	int state = luaL_checkinteger(L, 2);
	void* addr = pmem_alloc2(amount, state);
	lua_pushinteger(L, (lua_Integer) (size_t) addr);
	return 1;
}

static int l_alloc(lua_State* L)
{
	size_t amount = luaL_checkinteger(L, 1);
	void* addr = pmem_alloc(amount);
	lua_pushinteger(L, (lua_Integer) (size_t) addr);
	return 1;
}

static int l_set(lua_State* L)
{
	void* start = (void*) (size_t) luaL_checkinteger(L, 1);
	size_t amount = luaL_checkinteger(L, 2);
	int state = luaL_checkinteger(L, 3);
	pmem_set(start, amount, state);
	return 0;
}

static const struct luaL_Reg libpmem[] =
{
	{"alloc2", l_alloc2},
	{"alloc", l_alloc},
	{"set", l_set},
	{NULL, NULL}
};

void luaopen_pmem(lua_State* L)
{
	luaL_newlib(L, libpmem);

	lua_setconstant(L, "NORMAL", PMEM_NORMAL);
	lua_setconstant(L, "SUB16M", PMEM_SUB16M);
	lua_setconstant(L, "SUB1M", PMEM_SUB1M);

	lua_setglobal(L, "pmem");
}
