/*
 * tty.h
 *
 *  Created on: Jun 25, 2018
 *      Author: seeseemelk
 */

#ifndef TTY_H_
#define TTY_H_

void tty_init();
void tty_put_char(char);
void tty_read_char(char);

#endif /* TTY_H_ */
