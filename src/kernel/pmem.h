/*
 * pmem.h
 * This header has several functions to interact with the physical memory allocator.
 * The physical memory allocator keeps track of physical memory.
 *
 *  Created on: Jan 5, 2018
 *      Author: seeseemelk
 */
#ifndef PMEM_H
#define PMEM_H

#include <stddef.h>

#include "../libc/cdefs.h"

/** How much memory should the system have so that PMEM_NORMAL and PMEM_SUB16M won't get merged. */
#define MEMORY_NO_MERGE_NORMAL_AND_SUB16M_THRESSHOLD MB(24)
#define PMEM_BLOCK_SIZE KB(4)
#define PMEM_FREE 0
#define PMEM_USED 1
#define PMEM_RESERVED 2

typedef char state_t;
typedef enum
{
	PMEM_NORMAL = 0,
	PMEM_SUB16M = 1,
	PMEM_SUB1M = 2,
} pmem_option_t;

void* pmem_alloc2(size_t amount, pmem_option_t option);
#define pmem_alloc(amount) pmem_alloc2(amount, PMEM_NORMAL);
void pmem_set(void* start, size_t amount, state_t state);
void pmem_init(void* start, size_t mem_size);

#endif
