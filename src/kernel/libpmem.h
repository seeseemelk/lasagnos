/*
 * lpmem.h
 *
 *  Created on: Aug 15, 2018
 *      Author: seeseemelk
 */

#ifndef SRC_KERNEL_LIBPMEM_H_
#define SRC_KERNEL_LIBPMEM_H_
#include <lua.h>

void luaopen_pmem(lua_State* L);

#endif /* SRC_KERNEL_LIBPMEM_H_ */
