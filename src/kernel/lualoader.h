/*
 * lualoader.h
 *
 *  Created on: Jun 30, 2018
 *      Author: seeseemelk
 */

#ifndef LUALOADER_H_
#define LUALOADER_H_

#include <stddef.h>
#include <lua.h>

typedef struct LuaReader LuaReader;
struct LuaReader
{
	const char* buff;
	size_t len;
	size_t index;
};

//LuaReader open_lua_reader(const unsigned char* buff, size_t len);
LuaReader open_lua_reader(const char* name);
const char* read_lua_reader(lua_State* L, void* data, size_t* size);
void lua_register_files(lua_State* L);

#endif /* LUALOADER_H_ */
