#include "libsys.h"
#include "io.h"
#include "kernel.h"
#include "pmem.h"
#include "threads.h"
#include "cpu.h"
#include <stddef.h>
#include <string.h>
#include <lua.h>
#include <lauxlib.h>
#include <stdlib.h>
#include "../libc/cdefs.h"

typedef struct Interrupt Interrupt;
struct Interrupt
{
	Interrupt* volatile next;
	u32 code;
	u8 irq;
};

Interrupt* volatile next_interrupt = NULL;
Interrupt* last_interrupt;

static int l_peek(lua_State* L)
{
	int address = luaL_checkinteger(L, 1);
	u8 value = *((u8*)address);
	lua_pushinteger(L, value);
	return 1;
}

static int l_peekw(lua_State* L)
{
	int address = luaL_checkinteger(L, 1);
	u16 value = *((u16*)address);
	lua_pushinteger(L, value);
	return 1;
}

static int l_peekd(lua_State* L)
{
	int address = luaL_checkinteger(L, 1);
	u32 value = *((u32*)address);
	lua_pushinteger(L, value);
	return 1;
}

static int l_poke(lua_State* L)
{
	int address = luaL_checkinteger(L, 1);
	int value = luaL_checkinteger(L, 2);
	*((u8*)address) = (u8) value;
	return 0;
}

static int l_pokew(lua_State* L)
{
	int address = luaL_checkinteger(L, 1);
	int value = luaL_checkinteger(L, 2);
	*((u16*)address) = (u16) value;
	return 0;
}

static int l_poked(lua_State* L)
{
	int address = luaL_checkinteger(L, 1);
	int value = luaL_checkinteger(L, 2);
	*((u32*)address) = (u32) value;
	return 0;
}

static int l_inb(lua_State* L)
{
	int address = luaL_checkinteger(L, 1);
	u8 result = inb((short) address);
	lua_pushinteger(L, result);
	return 1;
}

static int l_inw(lua_State* L)
{
	int address = luaL_checkinteger(L, 1);
	u16 result = inw((short) address);
	lua_pushinteger(L, result);
	return 1;
}

static int l_inl(lua_State* L)
{
	int address = luaL_checkinteger(L, 1);
	u32 result = inl((short) address);
	lua_pushinteger(L, result);
	return 1;
}

static int l_outb(lua_State* L)
{
	int address = luaL_checkinteger(L, 1);
	int value = luaL_checkinteger(L, 2);
	outb((short)address, (char)value);
	return 0;
}

static int l_outw(lua_State* L)
{
	int address = luaL_checkinteger(L, 1);
	int value = luaL_checkinteger(L, 2);
	outw((short)address, (short)value);
	return 0;
}

static int l_outl(lua_State* L)
{
	int address = luaL_checkinteger(L, 1);
	int value = luaL_checkinteger(L, 2);
	outl((short)address, (int)value);
	return 0;
}

static int l_memset(lua_State* L)
{
	size_t dest = luaL_checkinteger(L, 1);
	size_t value = luaL_checkinteger(L, 2);
	int num = luaL_checkinteger(L, 3);
	size_t r = (size_t) memset((void*) dest, value, num);
	lua_pushinteger(L, r);
	return 1;
}

typedef struct __attribute__((packed))
{
	u8 b;
	u8 g;
	u8 r;
} color_t;

static int l_memset24(lua_State* L)
{
	size_t dest = luaL_checkinteger(L, 1);
	u32 value = luaL_checkinteger(L, 2);
	size_t num = luaL_checkinteger(L, 3);

	color_t color = {
			.r = (u8) (value >> 16),
			.g = (u8) (value >> 8),
			.b = (u8) value
	};

	color_t* c_ptr = (color_t*) dest;
	for (size_t i = 0; i < num; i++)
	{
		c_ptr[i] = color;
	}

	lua_pushinteger(L, dest);
	return 1;
}

static int l_memcpy(lua_State* L)
{
	size_t dest = luaL_checkinteger(L, 1);
	size_t src = luaL_checkinteger(L, 2);
	size_t num = luaL_checkinteger(L, 3);
	size_t r = (size_t) memcpy((void*) dest, (void*) src, num);
	lua_pushinteger(L, r);
	return 1;
}

static int l_memmove(lua_State* L)
{
	size_t dest = luaL_checkinteger(L, 1);
	size_t src = luaL_checkinteger(L, 2);
	size_t num = luaL_checkinteger(L, 3);
	size_t r = (size_t) memmove((void*) dest, (void*) src, num);
	lua_pushinteger(L, r);
	return 1;
}

static int l_malloc(lua_State* L)
{
	size_t amount = luaL_checkinteger(L, 1);
	void* address = malloc(amount);
	lua_pushinteger(L, (size_t) address);
	return 1;
}

static int l_free(lua_State* L)
{
	void* address = (void*) (size_t) luaL_checkinteger(L, 1);
	free(address);
	return 0;
}

static int l_log(lua_State* L)
{
	const char* str = luaL_checkstring(L, 1);
	kernel_log(str);
	return 0;
}

static int l_panic(lua_State* L)
{
	const char* str = luaL_checkstring(L, 1);
	kernel_panic(str);
	return 0;
}

static int l_interrupt(lua_State* L)
{
	Interrupt* interrupt = next_interrupt;
	if (interrupt == NULL)
		return 0;
	else
	{
		lua_pushinteger(L, interrupt->irq);
		lua_pushinteger(L, interrupt->code);
		next_interrupt = interrupt->next;
		free(interrupt);
		return 2;
	}
}

static int l_hinterrupt(lua_State* L)
{
	Interrupt* interrupt;
	while ((interrupt = next_interrupt) == NULL)
		asm("hlt");

	lua_pushinteger(L, interrupt->irq);
	lua_pushinteger(L, interrupt->code);
	next_interrupt = (Interrupt* volatile) interrupt->next;
	free(interrupt);
	return 2;
}

static int l_halt(lua_State* L)
{
	UNUSED(L);
	asm("hlt");
	return 0;
}

static int l_set_returnpoint(lua_State* L)
{
	size_t address = luaL_checkinteger(L, 1);
	cpu_set_returnpoint(address);
	return 0;
}

static const struct luaL_Reg libsys[] =
{
	{"peek", l_peek},
	{"peekw", l_peekw},
	{"peekd", l_peekd},
	{"poke", l_poke},
	{"pokew", l_pokew},
	{"poked", l_poked},
	{"inb", l_inb},
	{"inw", l_inw},
	{"inl", l_inl},
	{"outb", l_outb},
	{"outw", l_outw},
	{"outl", l_outl},
	{"memset", l_memset},
	{"memset24", l_memset24},
	{"memcpy", l_memcpy},
	{"memmove", l_memmove},
	{"malloc", l_malloc},
	{"free", l_free},
	{"log", l_log},
	{"panic", l_panic},
	{"interrupt", l_interrupt},
	{"hinterrupt", l_hinterrupt},
	{"halt", l_halt},
	{"setReturnpoint", l_set_returnpoint},
	{NULL, NULL}
};

void luaopen_sys(lua_State* L)
{
	luaL_newlib(L, libsys);
	lua_setglobal(L, "sys");
}

void interrupt_handle(int irq, int code)
{
	Interrupt* interrupt = unsafe_malloc(sizeof(Interrupt));
	interrupt->irq = irq;
	interrupt->code = code;
	interrupt->next = NULL;
	if (next_interrupt == NULL)
		next_interrupt = interrupt;
	else
		last_interrupt->next = interrupt;
	last_interrupt = interrupt;

	// Switch back to the Lua thread.
	if (current_thread->id != 1)
		thread_switch(thread_get(1));
}




