var.lua = {}
var.lua.pattern = "([%w%p]-%.lua)$"
var.lua.sources = var.lua.sources or {}
var.lua.headers = var.lua.headers or {}
var.lua.prgfs = {}

dir.klua = {}
dir.klua.base = fs.concat(dir.kernel, "lua")
dir.klua.headers = fs.concat(dir.klua.base, "headers")

var.lua.prgfs_list = fs.concat(dir.klua.base, "prgfs_list.lua")

tasks.add("generate_font", function()
	local file = fs.concat(dir.klua.base, "font.png")
	local output = fs.concat(dir.klua.base, "font.lua")
	if fs.needsBuild(output, file) then
		action.execute("tools/makefont/makefont.lua", file, output)
	end
end)

tasks.add("generate_lua", function()
	-- Generate the header files.
	local files = fs.find(dir.klua.base, var.lua.pattern)
	local stripped = fs.strip(dir.klua.base, files)

	util.concat(var.lua.sources, files)

	for _, file in ipairs(stripped) do
		local output = fs.concat("headers", file .. ".h")
		action.c.toHeader(file, output, dir.klua.base)
		var.lua.headers[#var.lua.headers + 1] = file .. ".h"
	end
end)

tasks.add("generate_lua_filesh", function()
	local filesH = fs.concat(dir.klua.headers, "files.h")
	if fs.needsBuild(filesH, var.lua.sources) then
		print("Generating " .. filesH)
		local prelude = fs.readContents(fs.concat(dir.klua.base, "1_files.h"))
		local postlude = fs.readContents(fs.concat(dir.klua.base, "2_files.h"))
		
		local fh = action.assert(io.open(filesH, "w"))
		action.assert(fh:write(prelude))

		for _, file in ipairs(var.lua.headers) do
			local header = fs.concat("lua/headers", file)
			action.assert(fh:write("#include \"" .. header .. "\"\n"))
		end

		action.assert(fh:write(postlude))
		action.assert(fh:close())
	end
end)

tasks.add("generate_lua_listh", function()
	local listH = fs.concat(dir.klua.headers, "list.h")
	if fs.needsBuild(listH, var.lua.sources) then
		print("Generating " .. listH)
		local fh = action.assert(io.open(listH, "w"))
		for _, file in ipairs(var.lua.headers) do
			local base = file:match("([%w/_]+)%."):gsub("[%./]", "_")
			action.assert(fh:write("FILE(\"" .. base .. "\", "
				.. base .. "_lua, " .. base .. "_lua_len)\n"))
		end
		action.assert(fh:close())
	end
end)

tasks.add("generate_all_lua", function()
	tasks.run("generate_font")
	tasks.run("generate_lua")
	tasks.run("generate_lua_listh")
	tasks.run("generate_lua_filesh")
end)

tasks.add("clean", function()
	action.rmdir(dir.klua.headers)
end)

tasks.add("build_kernel", function()
	local kernelOutput = fs.concat(dir.build, "kernel.o")
	local sources = dir.kernel

	local objects = {}
	action.c.build(action.c.findSources(sources), objects, {
		cflags = "-DKERNEL"	
	})
	action.nasm.build(action.nasm.findSources(sources), objects)

	util.concat(var.objects, action.c.linkObj(kernelOutput, objects))
end)
