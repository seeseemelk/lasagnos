#ifndef KERNEL_H
#define KERNEL_H

void __attribute__((noreturn)) kernel_panic(const char* format, ...);
void kernel_log(const char* format, ...);

#endif
