#include "lualoader.h"
#include "lua/headers/files.h"
#include <kernel.h>
#include <stdbool.h>
#include <string.h>
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#include "../libc/cdefs.h"

LuaReader open_buffer(const unsigned char* buff, size_t len)
{
	LuaReader reader = {
			.buff = (const char*) buff,
			.len = len,
			.index = 0
	};
	return reader;
}

LuaReader open_lua_reader(const char* filename)
{
	size_t i = 0;
	while (lua_filenames[i] != NULL)
	{
		const char* name = lua_filenames[i];
		unsigned char* data = lua_filedata[i];
		size_t len = *lua_filelengths[i];

		if (strcmp(filename, name) == 0)
			return open_buffer(data, len);

		i++;
	}
	LuaReader reader = {0};
	return reader;
}

const char* read_lua_reader(lua_State* L, void* data, size_t* size)
{
	UNUSED(L);
	LuaReader* reader = (LuaReader*) data;

	*size = reader->len - reader->index;
	if (*size > 0)
	{
		reader->index = reader->len;
		return reader->buff;
	}
	else
	{
		return NULL;
	}
}

static int l_require(lua_State* L)
{
	const char* name = luaL_checkstring(L, -1);
	char chunkname[1 + sizeof(".lua") + strlen(name)];
	chunkname[0] = '=';
	chunkname[1] = 0;
	strcat(chunkname, name);
	strcat(chunkname, ".lua");

	LuaReader reader = open_lua_reader(name);

	int error = lua_load(L, read_lua_reader, &reader, chunkname, "bt");
	if (error == 0)
	{
		error = lua_pcall(L, 0, LUA_MULTRET, 0);
		if (error != LUA_OK)
		{
			const char* err = lua_tostring(L, -1);
			kernel_panic("Failed to execute lua code: (%d) %s", error, (err == NULL) ? "No error string" : err);
			return 0;
		}
		else
		{
			return lua_gettop(L) - 1;
		}
	}
	else
	{
		const char* err = lua_tostring(L, -1);
		kernel_panic("Failed to load lua code: (%d) %s", error, (err == NULL) ? "No error string" : err);
		return 0;
	}
}

static int l_require_string(lua_State* L)
{
	const char* name = luaL_checkstring(L, -1);
	LuaReader reader = open_lua_reader(name);
	if (reader.buff == 0)
	{
		return luaL_error(L, "File not found");
	}
	else
	{
		lua_pushlstring(L, reader.buff, reader.len);
		return 1;
	}
}

void lua_register_files(lua_State* L)
{
	lua_pushcfunction(L, l_require);
	lua_setglobal(L, "require");

	lua_pushcfunction(L, l_require_string);
	lua_setglobal(L, "requireString");
}

























