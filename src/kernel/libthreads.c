#include "libthreads.h"
#include <lua.h>
#include <lauxlib.h>
#include "../libc/cdefs.h"

#define luaL_setconstant(L, n, c) \
	lua_pushinteger(L, c); \
	lua_setfield(L, -2, n)

static int l_create(lua_State* L)
{
	void* entry_point = (void*) (size_t) luaL_checkinteger(L, 1);
	thread_t* thread = thread_create(entry_point);
	lua_pushlightuserdata(L, thread);
	return 1;
}

static int l_create_special(lua_State* L)
{
	void* entry_point = (void*) (size_t) luaL_checkinteger(L, 1);
	char* stack = (char*) (size_t) luaL_checkinteger(L, 2);
	size_t stack_size = (size_t) (size_t) luaL_checkinteger(L, 3);
	short cs = (short) luaL_checkinteger(L, 4);
	short ds = (short) luaL_checkinteger(L, 5);
	short ss = (short) luaL_checkinteger(L, 6);
	thread_t* thread = thread_create_special(entry_point, stack, stack_size, cs, ds, ss);
	lua_pushlightuserdata(L, thread);
	return 1;
}

static int l_free(lua_State* L)
{
	thread_t* thread = lua_touserdata(L, 1);
	thread_free(thread);
	return 0;
}

static int l_leave(lua_State* L)
{
	UNUSED(L);
	thread_leave();
	return 0;
}

static int l_switch(lua_State* L)
{
	thread_t* thread = lua_touserdata(L, 1);
	thread_switch(thread);
	return 0;
}

static int l_create_user(lua_State* L)
{
	thread_t* thread = thread_create_user();
	lua_pushlightuserdata(L, thread);
	return 1;
}

static int l_get_registers(lua_State* L)
{
	thread_t* thread = lua_touserdata(L, 1);

	lua_createtable(L, 0, 0);
	luaL_setconstant(L, "esp", thread->data.esp);
	luaL_setconstant(L, "ebp", thread->data.ebp);
	luaL_setconstant(L, "ds", thread->data.ds);
	luaL_setconstant(L, "cs", thread->data.cs);

	return 1;
}

static const struct luaL_Reg libthreads[] =
{
	{"create", l_create},
	{"createUser", l_create_user},
	{"createSpecial", l_create_special},
	{"free", l_free},
	{"leave", l_leave},
	{"switch", l_switch},
	{"getRegisters", l_get_registers},
	{NULL, NULL}
};

void luaopen_threads(lua_State* L)
{
	luaL_newlib(L, libthreads);
	lua_setglobal(L, "threads");
}
