#ifndef MEMORY_H
#define MEMORY_H

#include <stddef.h>

void* unsafe_malloc(size_t bytes);
void* malloc(size_t bytes);
void* realloc(void* mem, size_t bytes);
void* unsafe_realloc(void* mem, size_t bytes);
void unsafe_free(void*);
void free(void*);
void memory_init(size_t len);

#endif
