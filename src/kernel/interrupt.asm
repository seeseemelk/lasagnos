[cpu 386]
[bits 32]

extern cpu_int_handler

my_loop:
	jmp $

; ISR template
; Note that if this is changed you might need to also change the line
;     *((u32*)(handler+N)) = i;
; in the function init_handlers() in i386_interrupt.c
section .data
global inth_start
inth_start:
	pushad
	push strict dword 0
	mov eax, cpu_int_handler
	call eax
	pop eax
	popad
	iretd
global inth_end
inth_end:
