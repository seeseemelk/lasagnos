#include "libpaging.h"
#include "paging.h"
#include "kernel.h"
#include "memory.h"
#include <lua.h>
#include <lauxlib.h>

#define luaL_setconstant(L, n, c) \
	lua_pushinteger(L, c); \
	lua_setfield(L, -2, n)

static int l_phys_addr(lua_State* L)
{
	virt_addr_t addr = (virt_addr_t) (size_t) luaL_checkinteger(L, 1);
	phys_addr_t phys = page_phys_addr(addr);
	lua_pushinteger(L, (size_t) phys);
	return 1;
}

static int l_query(lua_State* L)
{
	size_t align = luaL_checkinteger(L, 1);
	size_t bytes = luaL_checkinteger(L, 2);
	action_t action = luaL_checkinteger(L, 3);

	page_t page;
	if (page_query(&page, align, bytes, action))
	{
		lua_createtable(L, 0, 4);
		luaL_setconstant(L, "begin", (size_t) page.begin);
		luaL_setconstant(L, "bytesPerPage", page.bytes_per_page);
		luaL_setconstant(L, "pages", page.pages);
		return 1;
	}
	else
	{
		lua_pushnil(L);
		return 0;
	}
}

static int l_free(lua_State* L)
{
	page_t page;
	page.begin = (void*) lua_getfield(L, 1, "begin");
	page.bytes_per_page = lua_getfield(L, 1, "bytesPerPage");
	page.pages = lua_getfield(L, 1, "pages");

	page_free(&page);
	return 0;
}

static int l_assign(lua_State* L)
{
	virt_addr_t virt = (virt_addr_t) (size_t) luaL_checkinteger(L, 1);
	phys_addr_t phys = (phys_addr_t) (size_t) luaL_checkinteger(L, 2);
	page_assign(virt, phys);
	return 0;
}

static int l_assign_many(lua_State* L)
{
	virt_addr_t virt = (virt_addr_t) (size_t) luaL_checkinteger(L, 1);
	phys_addr_t phys = (phys_addr_t) (size_t) luaL_checkinteger(L, 2);
	size_t size = luaL_checkinteger(L, 3);
	page_assign_many(virt, phys, size);
	return 0;
}

static int l_copy(lua_State* L)
{
	paged_t* pdir = malloc(sizeof(paged_t));
	page_copy(pdir);
	lua_pushlightuserdata(L, pdir);
	lua_pushinteger(L, (u32) pdir->phys);
	return 2;
}

static int l_free_dir(lua_State* L)
{
	paged_t* pdir = lua_touserdata(L, 1);
	page_free_dir(pdir);
	return 0;
}

static int l_load(lua_State* L)
{
	paged_t* pdir = lua_touserdata(L, 1);
	page_load(pdir);
	return 0;
}

static int l_reloadAll(lua_State* L)
{
	UNUSED(L);
	page_reload_all();
	return 0;
}

static int l_allocate(lua_State* L)
{
	virt_addr_t mem_start = (virt_addr_t) (size_t) luaL_checkinteger(L, 1);
	size_t pages = (size_t) luaL_checkinteger(L, 2);
	page_allocate(mem_start, pages);
	return 0;
}

static int l_allocmem(lua_State* L)
{
	size_t bytes = (size_t) luaL_checkinteger(L, 1);
	virt_addr_t addr = page_allocmem(bytes);
	lua_pushinteger(L, (lua_Integer) (size_t) addr);
	return 1;
}

static int l_set_attributes(lua_State* L)
{
	virt_addr_t addr = (virt_addr_t) (size_t) luaL_checkinteger(L, 1);
	size_t pages = (size_t) luaL_checkinteger(L, 2);
	action_t state = (action_t) luaL_checkinteger(L, 3);
	page_set_attributes(addr, pages, state);
	return 0;
}

static const struct luaL_Reg libpaging[] =
{
	{"physAddr", l_phys_addr},
	{"query", l_query},
	{"free", l_free},
	{"assign", l_assign},
	{"assignMany", l_assign_many},
	{"copy", l_copy},
	{"freeDir", l_free_dir},
	{"load", l_load},
	{"reloadAll", l_reloadAll},
	{"allocate", l_allocate},
	{"allocmem", l_allocmem},
	{"setAttributes", l_set_attributes},
	{NULL, NULL}
};

void luaopen_paging(lua_State* L)
{
	luaL_newlib(L, libpaging);
	luaL_setconstant(L, "GLOBAL", PAGE_GLOBAL);
	luaL_setconstant(L, "USER", PAGE_USER);
	luaL_setconstant(L, "READONLY", PAGE_READONLY);
	luaL_setconstant(L, "ALLOCATE", PAGE_ALLOCATE);
	luaL_setconstant(L, "UNMAP", PAGE_UNMAP);

	lua_pushlightuserdata(L, &system_page_directory);
	lua_setfield(L, -2, "SYSTEM_PAGE_DIRECTORY");

	lua_setglobal(L, "paging");
}
















