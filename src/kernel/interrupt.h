/*
 * interrupt.h
 *
 *  Created on: Jan 24, 2018
 *      Author: seeseemelk
 */

#ifndef ARCH_I386_I386_INTERRUPT_H_
#define ARCH_I386_I386_INTERRUPT_H_
#include "asm.h"

void interrupt_init();
/*void cli();
void sti();*/

#endif /* ARCH_I386_I386_INTERRUPT_H_ */
