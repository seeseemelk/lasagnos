#include "multiboot.h"
#include "tty.h"
#include "cpu.h"
#include "fpu.h"
#include "kernel.h"
#include "memory.h"
#include "interrupt.h"
#include "pmem.h"
#include "paging.h"
#include "threads.h"
#include "lualoader.h"
#include "libsys.h"
#include "libpmem.h"
#include "libpaging.h"
#include "libthreads.h"
#include "libmultiboot.h"
#include "debug.h"
#include <string.h>
#include <stdbool.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#include "../libc/cdefs.h"

extern u8 kernel_end;
extern u8 kernel_start;

#define KERNEL_START &kernel_start
#define KERNEL_END &kernel_end
#define KERNEL_SIZE (size_t)KERNEL_END - (size_t)KERNEL_START

multiboot_info_t* multiboot;
size_t memory_available = 0;
size_t mb_minimum_addr;

void kernel_log(const char* format, ...)
{
	va_list args;
	va_start(args, format);

	vprintf(format, args);
	kputchar('\r');
	kputchar('\n');
	va_end(args);
}

void __attribute__((noreturn)) kernel_panic(const char* format, ...)
{
	kernel_log("Panicking!!! (did you eat the Lasagne?)\n");

	va_list args;
	va_start(args, format);
	vprintf(format, args);
	va_end(args);

	asm volatile ("cli");
	while (1)
		asm volatile ("hlt");
}

void kernel_init_cpu()
{
	cpu_init();
}

void kernel_init_tss()
{
	cpu_init_tss();
}

void kernel_init_int()
{
	interrupt_init();
}

void kernel_init_fpu()
{
	if (fpu_available())
		fpu_init();
	else
		kernel_panic("No FPU");
}

/**
 * Initialises the physical memory allocator and adds
 * several mappings to it.
 */
multiboot_memory_map_t* biggest_map;
void kernel_init_pmem()
{
	pmem_init(&kernel_end, memory_available);
	pmem_set(KERNEL_START - GB(3), KERNEL_SIZE, PMEM_USED);

	// First set everything below 1MB to RESERVED
	pmem_set(0, MB(1), PMEM_RESERVED);

	if ((multiboot->flags & MULTIBOOT_INFO_MEM_MAP) == 0)
			kernel_panic("No memory map");

	multiboot_memory_map_t* map = (multiboot_memory_map_t*) multiboot->mmap_addr;
	biggest_map = map;
	//multiboot_memory_map_t* biggest_map = map;
	multiboot_memory_map_t* last_map = (multiboot_memory_map_t*) (multiboot->mmap_addr + multiboot->mmap_length);
	while (map < last_map)
	{
		if (map->type == MULTIBOOT_MEMORY_AVAILABLE && map->len > biggest_map->len)
			biggest_map = map;
		map = (multiboot_memory_map_t*) ((size_t)map + map->size + 4);
	}

	if (biggest_map->addr < (u32)&kernel_end)
		biggest_map->addr = (u32)&kernel_end;
}

void kernel_init_paging()
{
	page_init();
}

void kernel_init_mem()
{
	memory_init((u32) biggest_map->len / 4);
}

void kernel_init_threads()
{
	thread_init();
}

void thread1()
{
	while (true)
	{
		kernel_log("Hello, world!");
		thread_leave();
	}
}

void thread2()
{
	while (true)
	{
		kernel_log("Thread");
		thread_leave();
	}
}

void kernel_main(multiboot_info_t* mbd, unsigned int magic)
{
	UNUSED(magic);
	tty_init();
	kernel_log("========================");
	kernel_log("=== Booting LasagnOS ===");
	kernel_log("========================");
	/*
	 * Information for the multiboot header can be found at:
	 * https://www.gnu.org/software/grub/manual/multiboot/multiboot.html
	 */
	multiboot = mbd;
	mb_minimum_addr = kernel_end;

	// Detect memory
	if ((mbd->flags & MULTIBOOT_INFO_MEMORY) > 0)
		memory_available = (mbd->mem_lower + mbd->mem_upper) * KB(1);
	else
		kernel_panic("No memory");

	kernel_log("Total memory available: %d MiB", memory_available / MB(1));
	kernel_log("Init CPU");
	kernel_init_cpu();
	kernel_log("Init FPU");
	kernel_init_fpu();
	kernel_log("Init PMEM");
	kernel_init_pmem();
	kernel_log("Init PAGING");
	kernel_init_paging();
	kernel_log("Init MEM");
	kernel_init_mem();
	kernel_log("Init INT");
	kernel_init_int();
	kernel_log("Init TSS");
	kernel_init_tss();
	kernel_log("Init THREADS");
	kernel_init_threads();
	kernel_log("System Ready");
	sti();

	// Load LuaVM.
	lua_State *L = luaL_newstate();
	luaL_openlibs(L);
	luaopen_sys(L);
	luaopen_pmem(L);
	luaopen_paging(L);
	luaopen_threads(L);
	luaopen_multiboot(L, mbd);

	lua_register_files(L);

	LuaReader reader = open_lua_reader("boot");

	int error = lua_load(L, read_lua_reader, &reader, "=boot.lua", "bt");
	bool ran = false;
	if (error == 0)
	{
		ran = true;
		error = lua_pcall(L, 0, 0, 0);
	}

	if (error)
	{
		const char* err = lua_tostring(L, -1);
		if (ran)
			kernel_panic("Failed to execute lua code: (%d) %s", error, (err == NULL) ? "No error string" : err);
		else
			kernel_panic("Failed to load lua code: (%d) %s", error, (err == NULL) ? "No error string" : err);
	}

	lua_close(L);

	kernel_panic("Reached end of mains");
}




















