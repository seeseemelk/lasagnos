/*
 * asmfn.h
 * All functions described in this header file are implemented in assembler.
 *
 *  Created on: Jan 3, 2018
 *      Author: seeseemelk
 */

#ifndef ASM_H
#define ASM_H
#include "paging.h"
#include <stddef.h>

extern void asm_load_cr3_page_dir(phys_addr_t address);
extern void cli();
extern void sti();
extern void asm_load_gdt(void*);
extern void asm_load_tss(u32);
extern u32 asm_get_cr2();
extern void asm_drop_ring3(void*);

#endif /* ASM_H */
