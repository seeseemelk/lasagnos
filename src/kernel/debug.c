#include "debug.h"

static const char CHARS[] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

void debug_byte(unsigned char b)
{
	*((char*) 0xB8000) = CHARS[(b >> 4) & 0xF];
	*((char*) 0xB8002) = CHARS[b & 0xF];
}

void debug_pause()
{
	for (unsigned int i = 0; i < 3000000; i++)
	{
		asm("pause");
	}
}

void debug_string(const char* str)
{
	char* screen = (char*) 0xB8000;
	while (*str != 0)
	{
		*screen = *str;
		str++;
		screen += 2;
	}
}
