/*
 * cpu.h
 *
 *  Created on: Jan 3, 2018
 *      Author: seeseemelk
 */

#ifndef CPU_H_
#define CPU_H_

#include <stddef.h>

void cpu_init();
void cpu_init_tss();
void cpu_set_returnpoint(size_t address);

#endif /* CPU_H_ */
