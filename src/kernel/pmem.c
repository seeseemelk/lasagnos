/*
 * pmem.c
 *
 *  Created on: Jan 5, 2018
 *      Author: seeseemelk
 */
#include "pmem.h"
#include "kernel.h"
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "../libc/cdefs.h"

state_t* pmem_map;
size_t pmem_size;

size_t pmem_begins[3];
size_t pmem_ends[3];

void* pmem_alloc2(size_t amount, pmem_option_t option)
{
	const size_t blocks_needed = ceildiv(amount, PMEM_BLOCK_SIZE);

	size_t index_found = 0;
	size_t blocks_found = 0;

	const size_t begin = pmem_begins[option];
	const size_t end = pmem_ends[option];

	for (size_t i = begin; i < end; i++)
	{
		if (pmem_map[i] == PMEM_FREE)
		{
			if (blocks_found == 0)
				index_found = i;
			blocks_found++;

			if (blocks_found == blocks_needed)
				break;
		}
		else
			blocks_found = 0;
	}

	if (blocks_found == blocks_needed)
	{
		for (size_t i = index_found; i < index_found + blocks_needed; i++)
			pmem_map[i] = PMEM_USED;
		//if ((index_found * PMEM_BLOCK_SIZE) == 0x3000)
			//kernel_panic("BAD MEM");
		return (void*) (index_found * PMEM_BLOCK_SIZE);
	}
	else
	{
		kernel_panic("WARNING! No more blocks left\n");
		return NULL;
	}
}

void pmem_free(void* start, size_t amount)
{
	const size_t index = (size_t)start / PMEM_BLOCK_SIZE;
	const size_t blocks = amount / PMEM_BLOCK_SIZE;
	for (size_t i = index; i < index + blocks; i++)
		pmem_map[i] = PMEM_FREE;
}

void pmem_set(void* start, size_t amount, state_t state)
{
	const size_t start_index = (size_t)start / PMEM_BLOCK_SIZE;
	const size_t blocks = ceildiv(amount, PMEM_BLOCK_SIZE);
	for (size_t i = start_index; i < start_index + blocks; i++)
	{
		if (i < pmem_size)
			pmem_map[i] = state;
	}
}

/**
 * Initialises the memory allocator.
 * A pointer should be passed to where it is allowed to place the memory map,
 * and the total available memory size should be passed on.
 */
void pmem_init(void* start, size_t mem_size)
{
	// Create the map and set it to FREE.
	pmem_map = (state_t*) start;
	pmem_size = mem_size / PMEM_BLOCK_SIZE;
	for (size_t i = 0; i < pmem_size; i++)
		pmem_map[i] = PMEM_FREE;

	// Then set the blocks of the map itself to USED
	size_t map_start = ((size_t)pmem_map - GB(3)) / PMEM_BLOCK_SIZE;
	size_t blocks = ceildiv(pmem_size, PMEM_BLOCK_SIZE);
	for (size_t block = 0; block < blocks; block++)
		pmem_map[block + map_start] = PMEM_USED;

	/* Finally, make sure the pmem_begins and pmem_ends array contains sensible values.
	 * We try to keep PMEM_SUB1M, PMEM_SUB16M, and PMEM_NORMAL separated unless
	 * there simply isn't enough memory available. Then we make the beginning
	 * of PMEM_NORMAL the same as PMEM_SUB16M.
	 */
	pmem_begins[PMEM_SUB1M] = 0;
	pmem_ends[PMEM_SUB1M] = MB(1) / PMEM_BLOCK_SIZE;

	pmem_begins[PMEM_SUB16M] = MB(1) / PMEM_BLOCK_SIZE;
	pmem_ends[PMEM_SUB16M] = (mem_size > MB(16)) ? (MB(16) / PMEM_BLOCK_SIZE) : pmem_size;

	pmem_begins[PMEM_NORMAL] = (mem_size > MEMORY_NO_MERGE_NORMAL_AND_SUB16M_THRESSHOLD) ? pmem_ends[PMEM_SUB16M] : pmem_begins[PMEM_SUB16M];
	pmem_ends[PMEM_NORMAL] = pmem_size;
}


































