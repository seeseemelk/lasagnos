/*
 * interrupt.c
 *
 *  Created on: Jan 24, 2018
 *      Author: seeseemelk
 */

#include "interrupt.h"
#include "kernel.h"
#include "pic.h"
#include "libsys.h"
#include "asm.h"
#include "debug.h"
#include <stdbool.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include "../libc/cdefs.h"
#include "../libc/cdefs.h"

extern void* inth_start;
extern void* inth_end;

#define GATE_MASK		0b00011111

#define T_INT_GATE 		0b00001110
#define T_TRAP_GATE 	0b00001111
#define T_TASK_GATE		0b00000101
#define BIT_PRESENT		0b10000000

#define INT_HANDLER_SIZE ((size_t)&inth_end - (size_t)&inth_start)

typedef struct int_descriptor int_descriptor;

struct int_descriptor
{
	u16 offset1;
	u16 segment;
	u8 zero;
	u8 flags;
	u16 offset2;
} __attribute__((packed));

int_descriptor idt[256];
char* handlers;

static void idt_set_type(int_descriptor* descriptor, u16 type)
{
	descriptor->flags = (descriptor->flags & ~GATE_MASK) | type;
}

static void idt_set_address(int_descriptor* descriptor, void* addr, u16 segment)
{
	descriptor->offset1 = (u16) (((u32)addr) & 0xFFFF);
	descriptor->offset2 = (u16) ((((u32)addr) >> 16) & 0xFFFF);
	descriptor->segment = segment;
}

static void idt_set_present(int_descriptor* descriptor, bool present)
{
	if (!present)
		descriptor->flags &= ~BIT_PRESENT;
	else
		descriptor->flags |= BIT_PRESENT;
}

static void idt_set_dpl(int_descriptor* descriptor, u8 dpl)
{
	descriptor->flags = (descriptor->flags & 0b10011111) | ((dpl & 3) << 5);
}

static void lidt(int_descriptor* idt)
{
	/*u64 val = 256 * sizeof(int_descriptor) - 1;
	val |= ((u64)((size_t)idt)) << 16UL;*/

	struct {
		u16 length;
		void* base;
	} __attribute((packed)) val = {256 * sizeof(int_descriptor) - 1, idt};

	asm volatile("lidt %0"
			:
			: "m" (val));
}

static void init_idt()
{
	for (int i = 0; i < 256; i++)
	{
		idt[i].flags = 0;
		idt[i].zero = 0;
		idt_set_type(idt+i, T_INT_GATE);
		idt_set_present(idt+i, true);
		idt_set_address(idt+i, (void*)(handlers+INT_HANDLER_SIZE*i), 8);
		idt_set_dpl(idt+i, 3);
	}

	lidt(idt);
}

static void init_handlers()
{
	char* original_handler = (char*)&inth_start;

	handlers = unsafe_malloc(INT_HANDLER_SIZE * 256);
	kernel_log("Handlers: 0x%X", handlers);
	for (int i = 0; i < 256; i++)
	{
		char* handler = (char*)((size_t)handlers + i*INT_HANDLER_SIZE);
		for (unsigned int b = 0; b < INT_HANDLER_SIZE; b++)
			handler[b] = original_handler[b];
		*((u32*)(handler+2)) = i;
	}
}

void interrupt_init()
{
	init_handlers();
	init_idt();
	pic_init();
}

void describe_page_fault(int code)
{
	kernel_log("Page fault description:");
	if ((code & 0x01) == 0)
		kernel_log("  Fault was caused by non-present page");
	else
		kernel_log("  Fault was caused by page-level protection violation");

	if ((code & 0x02) == 0)
		kernel_log("  Fault was caused by a read");
	else
		kernel_log("  Fault was caused by a write");

	if ((code & 0x04) == 0)
		kernel_log("  Fault was caused in supervisor mode");
	else
		kernel_log("  Fault was caused in user mode");

	if ((code & 0x08) > 0)
		kernel_log("  Fault was caused by a reserved bit violation");

	if ((code & 0x10) == 0)
		kernel_log("  Fault was not caused by an instruction fetch");
	else
		kernel_log("  Fault was caused by an instruction fetch");
}

void cpu_int_handler(int irq, int error_code)
{
	//if (irq != 0x20) // Make the timer STFU
	//	kernel_log("Encountered IRQ 0x%X", irq);

	u32 cr2;

	if (irq < 0x20)
	{
		switch (irq)
		{
		case 0:
			kernel_panic("IRQ: Divide Error");
			break;
		case 1:
			kernel_panic("IRQ: Debug Exception");
			break;
		case 2:
			kernel_log("IRQ: Non-Maskable Interrupt");
			break;
		case 3:
			kernel_panic("IRQ: Breakpoint");
			break;
		case 4:
			kernel_panic("IRQ: Overflow");
			break;
		case 5:
			kernel_panic("IRQ: BOUND Range Exceeded");
			break;
		case 6:
			kernel_panic("IRQ: Invalid Opcode");
			break;
		case 7:
			kernel_panic("IRQ: Device Not Available (No Math Coprocessor)");
			break;
		case 8:
			kernel_panic("IRQ: Double Fault (code: 0x%X)", error_code);
			break;
		case 9:
			kernel_panic("IRQ: Coprocessor Segment Overrun");
			break;
		case 10:
			kernel_panic("IRQ: Invalid TSS (code: 0x%X)", error_code);
			break;
		case 11:
			kernel_panic("IRQ: Segment Not Present (code: 0x%X)", error_code);
			break;
		case 12:
			kernel_panic("IRQ: Stack-Segment Fault (code: 0x%X)", error_code);
			break;
		case 13:
			kernel_panic("IRQ: General Protection Fault (code: 0x%X)", error_code);
			break;
		case 14:
			cr2 = asm_get_cr2();
			describe_page_fault(error_code);
			kernel_panic("IRQ: Page Fault (code: %b, address: 0x%X)", error_code, cr2);
			break;
		case 16:
			kernel_panic("IRQ: Floating-Point Error");
			break;
		case 17:
			kernel_panic("IRQ: Alignment Check (code: 0x%X)", error_code);
			break;
		case 18:
			kernel_panic("IRQ: Machine Check");
			break;
		case 19:
			kernel_panic("IRQ: SIMD Floating-Point Exception");
			break;
		case 20:
			kernel_panic("IRQ: Virtualization Exception");
			break;
		default:
			kernel_panic("Unknown IRQ: 0x%X", error_code);
			break;
		}
	}

	if (irq == 0x27)
	{
		if (!pic_is_served(irq))
		{
			kernel_log("Spurious interrupt on master");
			return;
		}
	}
	else if (irq == 0x2F)
	{
		if (!pic_is_served(irq))
		{
			// This will still send an EOI to the master PIC, but not the slave.
			pic_send_eoi(7);
			kernel_log("Spurious interrupt on slave");
			return;
		}
	}

	interrupt_handle(irq, error_code);
	if (irq >= 0x20)
		pic_send_eoi(irq);
}






