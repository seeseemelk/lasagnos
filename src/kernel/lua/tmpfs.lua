local fs = module("fs")
local tmpfs = {}
local mount = {}

function mount:_getIn(dir, subpath)
	assert(dir.type == "dir", "Not a directory")
	for i, v in ipairs(dir.content) do
		if v.name == subpath then
			return v
		end
	end
	return nil
end

function mount:_get(path)
	local dir = self.root
	local parts = util.split(path, "/")
	for i, v in ipairs(parts) do
		if v ~= "" then
			dir = self:_getIn(dir, v)
			if not dir then
				return nil
			end
		end
	end
	return dir
end

function mount:list(path)
	local dir = self:_get(path)
	local contents = {}
	for _, entry in ipairs(dir.content) do
		contents[#contents+1] = self:rawStat(entry)
	end
	return contents
end

function mount:open(path, mode, read, write, append)
	local file = self:_get(path)
	local fh = {
		file = file,
		index = 0
	}
	return fh
end

function mount:close(fh)
	fh.file = nil
	fh.index = 0
end

function mount:write(fh, buffer)
	assert(fh.file, "File is closed")
	local content = fh.file.content
	for i = 1, #buffer do
		local c = buffer:sub(i,i)
		content[fh.index + i] = c
		fh.index = fh.index + 1
	end
	return #buffer
end

function mount:read(fh, bytes)
	assert(fh.file, "File is closed")
	local content = fh.file.content
	if bytes > #content - fh.index then
		bytes = #content - fh.index
	end
	local read = {}
	for i = 1, bytes do
		read[i] = content[fh.index + i]
	end
	fh.index = fh.index + bytes
	return table.concat(content)
end

function mount:seek(fh, amount, offset)
end

function mount:rawStat(file)
	if not file then
		return nil, "File not found"
	end

	local stat = {
		name = file.name,
		type = file.type,
		device = file.device
	}
	if file.type == "dir" then
		stat.size = #file.content
	elseif file.type == "file" then
		stat.size = file.size
	end
	return stat
end

function mount:stat(path)
	local file = self:_get(path)
	return self:rawStat(file)
end

function mount:rm(path)
	local dir, base = fs.dirbasename(path)
	local parent = self:_get(dir)
	if not parent then
		return false, "Path not found"
	end
	for i, v in ipairs(parent.content) do
		if v.name == base then
			parent.content[i] = parent.content[#parent.content]
			parent.content[#parent.content] = nil
			return true
		end
	end
	return false, "File not found"
end

function mount:mkfile(path, filename)
	local parent = self:_get(path)
	local file = {
		type = "file",
		content = {},
		name = filename
	}
	parent.content[#parent.content+1] = file
end

function mount:mkdir(path, dirname)
	sys.log("path " .. dirname)
	local parent = self:_get(path)
	local file = {
		type = "dir",
		content = {},
		name = dirname
	}
	parent.content[#parent.content+1] = file
end

function mount:mkdev(path, filename, device)
	local parent = self:_get(path)
	local file = {
		type = "file",
		device = device,
		name = filename
	}
	parent.content[#parent.content+1] = file
end

function tmpfs.mount(options)
	local fs = {}
	fs.root = {
		type = "dir",
		name = "/",
		content = {}
	}
	for name, value in pairs(mount) do
		fs[name] = value
	end
	return fs
end

fs.registerFilesystem("tmpfs", tmpfs)
