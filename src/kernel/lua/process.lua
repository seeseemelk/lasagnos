-- Process types:
-- native
-- kprocess

local fs = module("fs")
local scheduler = module("scheduler")
local NativeProcess = class("NativeProcess")
local KProcess = class("KProcess")

-- Base Process class
local Process = new.class("Process", {
	dead = "paused"
})

local ps = new.module("ps", {
	processes = {},
	lastPID = 1
})

local function copyFD(fd)
	local fd = fs.open(fd.path, fd.mode)
	return fd
end

function Process.new(type, path, args, env)
	local obj = util.new(Process)
	obj.pid = ps.lastPID
	obj.type = type
	obj.state = "alive"
	obj.fd = {}
	obj.env = env
	obj.args = args
	ps.lastPID = ps.lastPID + 1

	-- If the process is spawned by another process,
	-- we need to copy certain data.
	if scheduler.lastProcess then
		obj.parentProcess = scheduler.lastProcess

		-- Copy stdin, stdout, and stderr.
		for i = 1, 3 do
			obj.fd[i] = copyFD(obj.parentProcess.fd[i])
		end

		-- Copy the environment variables.
		for name, value in pairs(scheduler.lastProcess.env) do
			obj.env[name] = value
		end
	end

	return obj
end

function Process:kill()
	local index
	for i, process in ipairs(ps.processes) do
		if process == self then
			index = i
			break
		end
	end

	for _, fd in pairs(self.fd) do
		fd:close()
	end

	local lastIndex = #ps.processes
	ps.processes[index] = ps.processes[lastIndex]
	ps.processes[lastIndex] = nil
	self.state = "dead"
end

function ps.type(path)
	local fh = assert(fs.open(path, "r"))
	local signature = fh:read(5)
	fh:close()

	if signature == NativeProcess.SIGNATURE then
		return "native"
	else
		return "kprocess"
	end
end

function ps.create(path, args, env)
	local type = ps.type(path)
	local process
	if type == "native" then
		process = NativeProcess.new(path, args, env)
	elseif type == "kprocess" then
		process = KProcess.new(path, args, env)
	else
		error("Unsupported process type " .. type)
	end

	ps.processes[#ps.processes + 1] = process

	return process
end

function ps.list()
	return ps.processes
end

--[[
local fs = module("fs")

local process = {}
local currentProcess

--- Called when a process crashes.
function process.fail(err)
	print("Process exited abnormaly:")
	print(tostring(err))
	print(debug.traceback())
end

--- Creates a process
function process.create(path, ...)
	local stat = fs.stat(path)
	local fh = fs.open(path, "r")
	local content = fs.read(fh, stat.size)
	fs.close(fh)

	local p = {
		path = path,
		status = "created",
		args = {...},
		env = {},
		g = {}
	}

	if currentProcess then
		for name, value in pairs(currentProcess.env) do
			p.env[name] = value
		end
		p.g.PENV = currentProcess.env
	end

	p.g.process = module("process")
	p.g.fs = module("fs")
	p.g.screen = module("screen")

	for name, value in pairs(_G) do
		p.g[name] = value
	end

	p.g.ENV = p.env

	p.cor = coroutine.create(function()
		p.status = "running"
		xpcall(function()
			local func = assert(load(content, "=" .. stat.name, "bt", p.g))
			local r = {func(table.unpack(p.args))}
		end, process.fail)
		p.status = "dead"
		return unpack(r)
	end)

	return p
end

--- Continues the process.
function process.start(p)
	local caller = currentProcess
	currentProcess = p
	local r = {coroutine.resume(p.cor)}
	currentProcess = caller
	return table.unpack(r)
end

--- Runs a new process.
function process.run(path, ...)
	local p = process.create(path, ...)
	process.start(p)
	return p
end

modules.register("process", process)
--]]
