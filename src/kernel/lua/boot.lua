sys.log("Entered Lua context");

-- Load additional utilities.
require("util")

-- Load basic kernel.
require("kernel")
require("initrd")

-- Load core modules.
require("mem")
require("fs")
require("process")
require("kprocess")
require("nprocess")
require("font")
require("screen")
require("interrupts")
require("ps2")
require("keyboard")
require("csysi")
require("nsysi")
require("lsysi")
require("sched")

-- Load filesystems
require("tmpfs")
require("prgfs")

-- Load disk modules
require("ata")
require("mbr")

-- Unmap old pages.
paging.setAttributes(0, 768*1024, paging.UNMAP)

-- Initialise system interfaces
module("nsysi").init()

-- Load /dev
local kthreads = module("kthreads")
local fs = module("fs")
--fs.mount("/dev", "tmpfs")
kthreads.new(fs.mount, "/dev", "tmpfs")

-- Mount disks
--[[
fs.mount("/", "tmpfs")
fs.mkdir("/dev")
fs.mkdir("/bin")
fs.mount("/bin", "prgfs")
--]]
kthreads.new(fs.mount, "/", "tmpfs")
kthreads.new(fs.mkdir, "/dev")
kthreads.new(fs.mkdir, "/bin")
kthreads.new(fs.mount, "/bin", "prgfs")

-- Initialise drivers
kthreads.new(module("ps2").init)
kthreads.new(module("ata").init)

-- These functions are here until replacement APIs are create.
local screen = module("screen")
kthreads.new(screen.init)
local keyboard = module("keyboard")
kthreads.new(keyboard.init)
local KB = keyboard.KB
local history = {}

local function erase(amount)
	for i = 1, amount do
		screen.popCursor()
		screen.print(" ")
		screen.popCursor()
	end
end
rawset(_G, "erase", erase)

local function fixHistory()
	for i = #history, 1, -1 do
		if #history[i] == 0 then
			history[i] = nil
		end
	end
end

local function clearHistory()
	history = {}
end

local function readCode()
	fixHistory()
	history[#history+1] = {}
	local str = history[#history]
	local historyIndex = #history
	while true do
		local key = keyboard.readScancode()
		if key.pressed then
			if key.char then
				screen.print(key.char)
			end
			if key.char == "\n" then
				if key.shift then
					str[#str+1] = key.char
				else
					return table.concat(str)
				end
			elseif key.scancode == KB.BACKSPACE then
				if #str > 0 then
					screen.popCursor()
					screen.print(" ")
					screen.popCursor()
					str[#str] = nil
				end
			elseif key.scancode == KB.CURUP then
				erase(#str)
				historyIndex = math.max(historyIndex - 1, 1)
				str = history[historyIndex]
				screen.print(table.concat(str))
			elseif key.scancode == KB.CURDOWN then
				erase(#str)
				historyIndex = math.min(historyIndex + 1, #history)
				str = history[historyIndex]
				screen.print(table.concat(str))
			elseif key.char then
				str[#str+1] = key.char
			end
		end
	end
end
rawset(_G, "readCode", readCode)

-- Printing routine
local function print(...)
	local str = table.concat({...}, "    ")
	sys.log(str)

	--[[
	str = str:gsub("\t", "    ")
	screen.print(str)
	screen.print("\n")
	--]]
end
rawset(_G, "print", print)

--[[
local nprocess = module("nprocess")
nprocess.runTest()

-- Start the first process.
local process = module("process")
process.run("/bin/sh")
--]]

-- Process interrupts
--ps.create("/bin/test", {}, {})

module("lsysi").init()

-- Load Initrd
module("initrd").init()

local ps = module("ps")
local scheduler = module("scheduler")
local started = false
local function startUserMode()
	--ps.create("/bin/test", {}, {})
	local process = ps.create("/bin/sh", {}, {})
	local csysi = module("csysi")
	csysi.os.setenv(process, "CWD", "/")
	csysi.io.open(process, "/dev/kbd", "r") -- FD 1 = stdin
	csysi.io.open(process, "/dev/vga", "w") -- FD 2 = stdout
	csysi.io.open(process, "/dev/vga", "w") -- FD 3 = stderr
	started = true
	--scheduler.run()
end

local function startScheduler()
	while not started do
		kthreads.yield()
	end
	scheduler.run()
end

local interrupts = module("interrupts")
--local scheduler = module("scheduler")
kthreads.new(startUserMode)
kthreads.new(startScheduler)
while true do
	kthreads.removeDead()
	sys.log("Threads: " .. #kthreads.threads)
	for _, kthread in ipairs(kthreads.threads) do
		kthread:run()
	end
	interrupts.processIRQs()
	--scheduler.tick()
end

--[[
require("util")
require("kernel")
require("process")
require("interrupts")
require("fs")
require("tmpfs")
require("prgfs")
require("fat")
require("screen")
require("keyboard")
require("ps2")
require("ata")
require("mbr")

screen.init()

io = {}
function io.write(...)
	local tbl = {}
	for _, v in ipairs({...}) do
		tbl[#tbl+1] = tostring(v)
	end
	local str = table.concat(tbl, " ")
	screen.print(str:gsub("\t", "    "))
end

function print(...)
	io.write(...)
	screen.print("\n")
end
print("OCO86")
fs.mount("/", "tmpfs")
fs.mkdir("/dev")
fs.mkdir("/etc")
fs.mkdir("/bin")
fs.mount("/bin", "prgfs")
fs.mount("/dev", "tmpfs")
fs.mkdir("/mnt")

ata.init()
mbr.probe("/dev/hda")
fs.mount("/mnt", "fat", "/dev/hda1", {"rw"})

local function f()
	print("Hello")
	coroutine.yield()
	print("World")
end

local function f2()
	pcall(f)
end

local c = coroutine.create(f2)
coroutine.resume(c)
print("Yo")
coroutine.resume(c)

print("Listing root")
function listDir(path)
	--path = fs.normalizePath(path)
	path = fs.purecanon(path)
	print("+ " .. path)
	local contents = fs.list(path)
	for i, v in ipairs(contents) do
		print("-> " .. fs.concat(path, v.name))
	end
	for i, v in ipairs(contents) do
		if v.type == "dir" then
			listDir(fs.concat(path, v.name))
		end
	end
end
listDir("/")

local oldPanic = sys.panic
function sys.panic(str)
	print("PANIC: " .. tostring(str))
	oldPanic(str)
end

ps2.init()
keyboard.init()

local history = {}

function erase(amount)
	for i = 1, amount do
		screen.popCursor()
		screen.print(" ")
		screen.popCursor()
	end
end

function fixHistory()
	for i = #history, 1, -1 do
		if #history[i] == 0 then
			history[i] = nil
		end
	end
end

function clearHistory()
	history = {}
end

function readCode()
	fixHistory()
	history[#history+1] = {}
	local str = history[#history]
	local historyIndex = #history
	while true do
		local key = keyboard.readScancode()
		if key.pressed then
			if key.char then
				screen.print(key.char)
			end
			if key.char == "\n" then
				if key.shift then
					str[#str+1] = key.char
				else
					return table.concat(str)
				end
			elseif key.scancode == KB.BACKSPACE then
				if #str > 0 then
					screen.popCursor()
					screen.print(" ")
					screen.popCursor()
					str[#str] = nil
				end
			elseif key.scancode == KB.CURUP then
				erase(#str)
				historyIndex = math.max(historyIndex - 1, 1)
				str = history[historyIndex]
				screen.print(table.concat(str))
			elseif key.scancode == KB.CURDOWN then
				erase(#str)
				historyIndex = math.min(historyIndex + 1, #history)
				str = history[historyIndex]
				screen.print(table.concat(str))
			elseif key.char then
				str[#str+1] = key.char
			end
		end
	end
end

function dumpFile(file)
	local file = fs.open(file)
	util.hexdump(file:read(512))
	file:close()
end

local function handleError(x)
	print("ERROR: " .. x)
	print(debug.traceback())
end

function ls(path)
	local content = fs.list(path)
	for i, v in ipairs(content) do
		if v.type == "dir" then
			print(v.name .. "/")
		else
			print(v.name)
		end
	end
end

process.run("/bin/sh")
print("Processes ended. Entering interpreter.")
clearHistory()

while true do
	screen.print("> ")
	local code = readCode()
	local func, err = load(code)
	if func then
		local success, err = pcall(func)
		if not success then
			print("ERROR: " .. err)
		end
	else
		print("ERROR: " .. err)
	end
end
--]]
