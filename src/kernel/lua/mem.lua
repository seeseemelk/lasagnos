local Mem = new.class("Mem")
local MemWrap = {}

function MemWrap:__index(index)
	assert(index <= self.size, "MemWrap out of bounds")
	assert(index >= 1, "MemWrap index less than 1")
	return sys.peek(self.address + index)
end

function MemWrap:__newindex(index, value)
	assert(index <= self.size, "MemWrap out of bounds")
	assert(index >= 1, "MemWrap index out of bounds")
	self.length = math.max(self.length, index)
	sys.poke(self.address + index, value)
end

function MemWrap:__len()
	return self.length
end

function Mem.wrap(address, size)
	size = size or (util.GB(4) - address)
	local tbl = {
		address = address - 1,
		size = size,
		length = 0
	}
	setmetatable(tbl, MemWrap)
	return tbl
end

function Mem.wrapBlock(address, size)
	size = size or (util.GB(4) - address)
	local tbl = {
		address = address - 1,
		size = size,
		length = size
	}
	setmetatable(tbl, MemWrap)
	return tbl
end

-- Converts a C-style null-terminated string
-- to a native Lua string.
function Mem.parseCString(address)
	local characters = {}
	local char = sys.peek(address)
	while char ~= 0 do
		characters[#characters + 1] = string.char(char)
		address = address + 1
		char = sys.peek(address)
	end
	return table.concat(characters)
end
