local modules = {
	_mods = {},
	_classes = {}
}
local new = {}
rawset(_G, "modules", modules)
rawset(_G, "new", new)

local ModuleReference = {}

function new.module(name, tbl)
	tbl = tbl or {}
	modules._mods[name] = tbl
	return tbl
end

function new.class(name, tbl)
	tbl = tbl or {}
	modules._classes[name] = tbl
	return tbl
end

function modules.requireModule(name)
	if modules._mods[name] then
		return modules._mods[name]
	else
		local moduleReference = {}
		moduleReference.__refname = name
		moduleReference.__type = "module"
		local metatable = util.shallowCopy(ModuleReference)
		setmetatable(moduleReference, metatable)
		return moduleReference
	end
end

function modules.requireClass(name)
	if modules._classes[name] then
		return modules._classes[name]
	else
		local classReference = {}
		classReference.__refname = name
		classReference.__type = "class"
		local metatable = util.shallowCopy(ModuleReference)
		setmetatable(classReference, metatable)
		return classReference
	end
end

function module(name)
	return modules.requireModule(name)
end

function class(name)
	return modules.requireClass(name)
end

function ModuleReference:__index(key)
	local ref
	if self.__type == "module" then
		ref = modules._mods[self.__refname]
	else
		ref = modules._classes[self.__refname]
	end

	if not ref then
		error(self.__type .. " " .. tostring(self.__refname) .. " does not exist")
	end
	getmetatable(self).__index = ref
	return ref[key]
end

function ModuleReference:__newindex(key, value)
	error("Not supported")
end

-- Kernel Threads
local kthreads = new.module("kthreads", {
	threads = {},
	current = nil
})
local KThread = {}

function kthreads.new(func, ...)
	local funcName = tostring(func)

	local kthread = util.new(KThread)
	local args = {...}
	kthread.coroutine = coroutine.create(function()
		func(table.unpack(args))
	end)
	kthreads.threads[#kthreads.threads + 1] = kthread
	return kthread
end

function kthreads.removeDead()
	local list = kthreads.threads
	local len = #list

	local writeIndex = 1
	for readIndex = 1, len do
		if not list[readIndex].dead then
			list[writeIndex] = list[readIndex]
			writeIndex = writeIndex + 1
		else
			sys.log("Found dead coroutine")
		end
	end

	for i = writeIndex, len do
		list[i] = nil
	end
end

function kthreads.yield(...)
	return coroutine.yield(...)
end

function KThread:run(...)
	kthreads.current = self
	local status, err = coroutine.resume(self.coroutine, ...)
	if not status then
		self.dead = true
		error("Error in kthread: " .. tostring(err))
	elseif coroutine.status(self.coroutine) == "dead" then
		self.dead = true
	end
	kthreads.current = nil
end

local ThreadBarrier = new.class("ThreadBarrier")

function ThreadBarrier.new()
	local barrier = util.new(ThreadBarrier)
	return barrier
end

function ThreadBarrier:lock()
	while barrier.thread ~= kthreads.current do
		kthreads.yield()
	end
	barrier.thread = kthreads.current
end

function ThreadBarrier:unlock()
	barrier.thread = nil
end

util.protect(_G)
