local interrupts = module("interrupts")
local ps2 = new.module("ps2", {
	numPorts = 0
})

local PS2_DAT = 0x60
local PS2_STA = 0x64
local PS2_CMD = 0x64

function ps2.waitInputClear()
	while (ps2.readStatus() & 2) > 2 do end
end

function ps2.readStatus()
	return sys.inb(PS2_STA)
end

function ps2.readResp()
	while (ps2.readStatus() & 1) == 0 do end
	return sys.inb(PS2_DAT)
end

function ps2.readData()
	return sys.inb(PS2_DAT)
end

function ps2.writeData(data)
	ps2.waitInputClear()
	sys.outb(PS2_DAT, data)
end

function ps2.writeCommand(command)
	ps2.waitInputClear()
	sys.outb(PS2_CMD, command)
end

function ps2.readRam(i)
	ps2.writeCommand(0x20 + i)
	return ps2.readResp()
end

function ps2.writeRam(i, val)
	ps2.waitInputClear()
	sys.outb(PS2_CMD, 0x60 + i)
	ps2.waitInputClear()
	sys.outb(PS2_DAT, val)
end

function ps2.stInit()
	ps2.writeCommand(0xAD)
	ps2.writeCommand(0xA7)

	while (ps2.readStatus() & 1) == 1 do
		ps2.readData()
	end

	local config = ps2.readRam(0)
	if (config & 0x20) > 0 then
		ps2.numPorts = 2
	else
		ps2.numPorts = 1
	end

	config = (config & 0xBC)
	ps2.writeRam(0, config)
end

function ps2.init()
	ps2.stInit()
	ps2.writeCommand(0xAA)

	local data
	repeat
		data = ps2.readResp()
		if data == 0xFC then
			error("PS/2 Controller Bad")
		elseif data == 0xFE then
			ps2.writeCommand(0xAA)
		elseif data ~= 0x55 then
			error("PS/2 Unknown Status Flag " .. data)
		end
	until data == 0x55

	ps2.stInit()

	ps2.writeCommand(0xAB)
	data = ps2.readResp()
	if data == 1 then
		error("PS/2 Clock Stuck Low")
	elseif data == 2 then
		error("PS/2 Clock Stuck High")
	elseif data == 3 then
		error("PS/2 Data Stuck Low")
	elseif data == 4 then
		error("PS/2 Data Stuck High")
	end
	sys.log("PS/2 Port A OK")

	ps2.writeCommand(0xAE)

	local config = ps2.readRam(0)
	config = (config | 1)
	ps2.writeRam(0, config)

	ps2.writeData(0xFF)
	local resp = ps2.readData()
	while resp ~= 0xFA do
		if resp == 0xFC then
			error("PS/2 Device 1 Reset Failure")
		elseif resp == 0xFE then
			ps2.writeData(0xFF)
		end
		resp = ps2.readData()
	end
	interrupts.wait(0x21)
	sys.log("PS/2 Device 1 Reset And Ready")
end
