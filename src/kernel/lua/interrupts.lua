local kthreads = module("kthreads")
local interrupts = new.module("interrupts", {
	handlers = {}
})

function interrupts.handleIRQ(irq, code)
	if irq ~= 0x20 then
		sys.log("Kernel IRQ " .. util.hex(irq))
	end
	if interrupts.handlers[irq] then
		for _, handler in pairs(interrupts.handlers[irq]) do
			-- If the handler returns true we don't have to check for
			-- any other interrupt handlers.
			if handler.callback(irq, code) then
				return
			end
		end
	end
end

function interrupts.processIRQs()
	local irq, code = sys.interrupt()
	while irq do
		interrupts.handleIRQ(irq, code)
		irq, code = sys.interrupt()
	end
end

function interrupts.addHandler(irq, callback)
	if not interrupts.handlers[irq] then
		interrupts.handlers[irq] = {}
	end

	local handler = {
		irq = irq,
		callback = callback
	}
	interrupts.handlers[irq][handler] = handler
	return handler
end

function interrupts.removeHandler(handler)
	interrupts.handlers[handler.irq][handler] = nil
end

function interrupts.wait(irq)
	sys.log("Waiting for irq " .. util.hex(irq))
	local thread = kthreads.current
	local handler
	handler = interrupts.addHandler(irq, function()
		sys.log("Received irq " .. util.hex(irq))
		interrupts.removeHandler(handler)
		thread:run(true)
	end)

	while not kthreads.yield() do
		sys.log("Waiting")
	end
	sys.log("Finally returning")
end

--[[
local Queue = require("queue")
local interrupts = {}
local queues = {}

for i = 0, 255 do
	queues[i] = Queue.new()
end

function interrupts.waitFor(irqNum)
	while true do
		local irq, code = sys.hinterrupt()
		if irq == irqNum then
			return irq, code
		else
			queues[irq]:push({irq=irq, code=code})
		end
	end
end

function interrupts.wait(irqNum)
	if queues[irqNum]:hasNext() then
		local irq = queues[irqNum]:pull()
		return irq.irq, irq.code
	else
		interrupts.waitFor(irqNum)
	end
end

modules.register("interrupts", interrupts)
--]]
