util = {}

function util.toShort(tbl, index)
	assert(type(tbl) == "table", "util.toShort not passed a table")
	return tbl[index]
		+ (tbl[index+1] << 8)
end

function util.toInt(tbl, index)
	assert(type(tbl) == "table", "util.toInt not passed a table")
	return util.toShort(tbl, index)
		+ (util.toShort(tbl, index+2) << 16)
end

function util.toString(tbl, start, stop)
	assert(type(tbl) == "table", "util.toString not passed a table")
	local buf = {}
	for i = start, stop do
		buf[#buf+1] = string.char(tbl[i])
	end
	return table.concat(buf, "")
end

function util.intToTable(int)
	return {
		int & 0xFF,
		(int >> 8) & 0xFF,
		(int >> 16) & 0xFF,
		(int >> 24) & 0xFF
	}
end

function util.intToStr(int)
	local tbl = util.intToTable(int)
	for i, v in ipairs(tbl) do
		tbl[i] = string.char(v)
	end
	return table.concat(tbl)
end

function util.hexdump(buffer, options)
	options = options or {
		noShowLength = false
	}
	if type(buffer) ~= "table" then
		print("Tried to hexdump a " .. type(buffer))
		return
	end
	for i = 1, #buffer do
		if buffer[i] then
			screen.print(string.format("%02x ", buffer[i]))
		else
			screen.print("NL ")
		end
		if i % 16 == 0 then
			screen.print("\n")
		end
	end

	if not options.noShowLength then
		screen.print("Length: " .. #buffer)
		screen.print("\n")
	end
end

function util.dump(tbl, depth)
	if type(tbl) ~= "table" then
		print("Tried to dump a " .. type(tbl))
		return
	end

	depth = depth or 1
	for name, value in pairs(tbl) do
		screen.print(("-"):rep(depth))
		screen.print(" ")
		screen.print(tostring(name))
		screen.print(" [")
		screen.print(type(value):sub(1,3):upper())
		screen.print("]: ")
		if type(value) == "string" then
			screen.print('"')
			screen.print(tostring(value))
			screen.print('"')
		else
			screen.print(tostring(value))
		end
		screen.print("\n")
		if type(value) == "table" then
			util.dump(value, depth+1)
		end
	end
end

function util.trim(str)
	local beginning, ending = 1, #str
	for i = 1, #str do
		local char = str:sub(i,i)
		if char == " " or char == "\t" or char == "\r" or char == "\n" or char == "\0" then
			beginning = i
		else
			break
		end
	end

	for i = #str, 1, -1 do
		local char = str:sub(i,i)
		if char == " " or char == "\t" or char == "\r" or char == "\n" or char == "\0" then
			ending = i-1
		else
			break
		end
	end

	return str:sub(beginning, ending)
end

function util.hex(num, length)
	if type(num) == "number" then
		if length then
			return string.format("0x%0" .. length .. "X", num)
		else
			return string.format("0x%X", num)
		end
	else
		return tostring(num)
	end
end

function util.split(str, splitter)
	assert(type(str) == "string", "str must be a string")
	assert(type(splitter) == "string", "splitter must be a string")
	
	local parts = {}
	local part = {}

	for i = 1, #str do
		local char = str:sub(i,i)
		if char == splitter then
			parts[#parts+1] = table.concat(part)
			part = {}
		else
			part[#part+1] = char
		end
	end

	if #part > 0 then
		parts[#parts+1] = table.concat(part)
	end
	return parts
end

function util.shallowCopy(src, dest)
	dest = dest or {}
	for name, value in pairs(src) do
		dest[name] = value
	end
	return dest
end

local _Class = {}
function _Class.__index(tbl, name)
	return tbl.__class[name] or tbl.__base[name]
end

function util.new(class, base)
	--[[
	local obj = {
		__class = class,
		__base = base or {},
	}
	setmetatable(obj, _Class)
	return obj
	--]]
	return util.shallowCopy(class, base)
end

function util.ceildiv(a, b)
	return (a - 1) // b + 1
end

function util.KB(kb)
	return kb * 1024
end

function util.MB(mb)
	return util.KB(mb * 1024)
end

function util.GB(gb)
	return util.MB(gb * 1024)
end

local _protect = {}
function util.protect(tbl)
	setmetatable(tbl, _protect)
end

function _protect:__index(name)
	error("Attempt to index nil '" .. tostring(name) .. "'")
end

function _protect:__newindex(name)
	error("Attempt to create index '" .. tostring(name) .. "'")
end
