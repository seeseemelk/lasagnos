local ps2 = module("ps2")
local keyboard = new.module("keyboard", {
	shiftDown = false,
	altDown = false,
})

local KB = {
	F1=0x05, F2=0x06, F3=0x04, F4=0x0C, F5=0x03, F6=0x0B, F7=0x83, F8=0x0A, F9=0x01, F10=0x09, F11=0x78, F12=0x07,
	A=0x1C, B=0x32, C=0x21, D=0x23, E=0x24, F=0x2B, G=0x34, H=0x33, I=0x43, J=0x3B,
	K=0x42, L=0x4B, M=0x3A, N=0x31, O=0x44, P=0x4D, Q=0x15, R=0x2D, S=0x1B, T=0x2C,
	U=0x3C, V=0x2A, W=0x1D, X=0x22, Y=0x35, Z=0x1A,

	N0=0x45, N1=0x16, N2=0x1E, N3=0x26, N4=0x25, N5=0x2E, N6=0x36, N7=0x3D, N8=0x3E, N9=0x46,

	NUMLOCK=0x77,
	KP0=0x70, KP1=0x69, KP2=0x72, KP3=0x7A, KP4=0x6B, KP5=0x73,
	KP6=0x74, KP7=0x6C, KP8=0x75, KP9=0x7D,
	KPDOT=0x71, KPPLUS=0x79, KPMINUS=0x7B, KPMUL=0x7C, KPDIV=0xCA,
	KPENTER=0xDA,

	TAB=0x0D, BACKTICK=0x0E, LALT=0x11, LSHIFT=0x12, LCONTROL=0x14, SPACE=0x29,
	COMMA=0x41, DOT=0x49, SLASH=0x4A, SEMICOLON=0x4C, MINUS=0x4E, QUOTE=0x52,
	LBRACKET=0x54, RBRACKET=0x5B, EQUALS=0x55, CAPSLOCK=0x58, RSHIFT=0x59,
	ENTER=0x5A, BACKSLASH=0x5D, BACKSPACE=0x66, ESCAPE=0x76, SCROLLLOCK=0x7E,

	WWWSEARCH=0x90, RALT=0x91, RCONTROL=0x94, MMPREVTRACK=0x95, WWWFAV=0x98,
	LGUI=0x9F, WWWREFRESH=0xA0, MMMUTE=0xA3, RGUI=0xA7, WWWSTOP=0xA8,
	MMCALC=0xAB, APPS=0xAF, WWWFORWARD=0xB0, MMVOLUP=0xB2, MMPLAY=0xB4,
	ACPIPOWER=0xB7, WWWBACK=0xB8, WWWHOME=0xBA, MMSTOP=0xBB, ACPISLEEP=0xBF,
	MMCOMPUTER=0xC0, MMEMAIL=0xC8, MMNEXT=0xCD, MMSELECT=0xD0, ACPIWAKE=0xDE,
	END=0xE9, CURLEFT=0xEB, HOME=0xEC, INSERT=0xF0, DELETE=0xF1, CURDOWN=0xF2,
	CURRIGHT=0xF4, CURUP=0xF5, PAGEDOWN=0xFA, PAGEUP=0xFD,

	EXTRA1=0x61
}

local keymapBe = {
	normal = {
		[KB.A]="q", [KB.B]="b", [KB.C]="c", [KB.D]="d",
		[KB.E]="e", [KB.F]="f", [KB.G]="g", [KB.H]="h",
		[KB.I]="i", [KB.J]="j", [KB.K]="k", [KB.L]="l",
		[KB.M]=",", [KB.N]="n", [KB.O]="o", [KB.P]="p",
		[KB.Q]="a", [KB.R]="r", [KB.S]="s", [KB.T]="t",
		[KB.U]="u", [KB.V]="v", [KB.W]="z", [KB.X]="x",
		[KB.Y]="y", [KB.Z]="w", [KB.SEMICOLON]="m",
		[KB.SPACE] = " ",
		[KB.N1] = '&',
		[KB.N2] = string.char(0x82),
		[KB.N3] = '"',
		[KB.N4] = '\'',
		[KB.N5] = '(',
		[KB.N6] = string.char(0x15),
		[KB.N7] = string.char(0x8A),
		[KB.N8] = '!',
		[KB.N9] = string.char(0x87),
		[KB.N0] = string.char(0xA0),

		[KB.ENTER] = '\n',
		[KB.BACKTICK] = string.char(0xFD),
		[KB.EXTRA1] = '<',
		[KB.MINUS] = ')',
		[KB.EQUALS] = '-',
		[KB.LBRACKET] = '^',
		[KB.RBRACKET] = '$',
		[KB.QUOTE] = string.char(0x97),
		[KB.BACKSLASH] = string.char(0xE6),

		[KB.COMMA] = ';',
		[KB.DOT] = ':',
		[KB.SLASH] = '=',

		[KB.KPDIV] = '/', [KB.KPMUL] = '*',
		[KB.KPMINUS] = '-', [KB.KPPLUS] = '+',
		[KB.KP7] = '7', [KB.KP8] = '8', [KB.KP9] = '9',
		[KB.KP4] = '4', [KB.KP5] = '5', [KB.KP6] = '6',
		[KB.KP1] = '1', [KB.KP2] = '2', [KB.KP3] = '3',
		[KB.KP0] = '0',
		[KB.KPDOT] = '.'
	},
	shift = {
		[KB.A] = 'Q', [KB.B] = 'B', [KB.C] = 'C', [KB.D] = 'D',
		[KB.E] = 'E', [KB.F] = 'F', [KB.G] = 'G', [KB.H] = 'H',
		[KB.I] = 'I', [KB.J] = 'J', [KB.K] = 'K', [KB.L] = 'L',
		[KB.M] = '?', [KB.N] = 'N', [KB.O] = 'O', [KB.P] = 'P',
		[KB.Q] = 'A', [KB.R] = 'R', [KB.S] = 'S', [KB.T] = 'T',
		[KB.U] = 'U', [KB.V] = 'V', [KB.W] = 'Z', [KB.X] = 'X',
		[KB.Y] = 'Y', [KB.Z] = 'W', [KB.SEMICOLON] = 'M',

		[KB.N1] = '1',
		[KB.N2] = '2',
		[KB.N3] = '3',
		[KB.N4] = '4',
		[KB.N5] = '5',
		[KB.N6] = '6',
		[KB.N7] = '7',
		[KB.N8] = '8',
		[KB.N9] = '9',
		[KB.N0] = '0',

		[KB.BACKTICK] = string.char(0xFC),
		[KB.EXTRA1] = '>',
		[KB.MINUS] = string.char(0xF8),
		[KB.EQUALS] = '_',
		[KB.LBRACKET] = string.char(0xF9),
		[KB.RBRACKET] = '*',
		[KB.QUOTE] = '%',
		[KB.BACKSLASH] = string.char(0x9C),

		[KB.COMMA] = '.',
		[KB.DOT] = '/',
		[KB.SLASH] = '+'
	},
	alt = {
		[KB.A] = 'q', [KB.B] = 'b', [KB.C] = 'c', [KB.D] = 'd',
		[KB.E] = 'e', [KB.F] = 'f', [KB.G] = 'g', [KB.H] = 'h',
		[KB.I] = 'i', [KB.J] = 'j', [KB.K] = 'k', [KB.L] = 'l',
		[KB.M] = ',', [KB.N] = 'n', [KB.O] = 'o', [KB.P] = 'p',
		[KB.Q] = 'a', [KB.R] = 'r', [KB.S] = 's', [KB.T] = 't',
		[KB.U] = 'u', [KB.V] = 'v', [KB.W] = 'z', [KB.X] = 'x',
		[KB.Y] = 'y', [KB.Z] = 'w', [KB.SEMICOLON] = 'm',

		[KB.N1] = '|',
		[KB.N2] = '@',
		[KB.N3] = '#',
		[KB.N4] = string.char(0xAC),
		[KB.N5] = string.char(0xAB),
		[KB.N6] = '^',
		[KB.N7] = '{',
		[KB.N8] = '[',
		[KB.N9] = '{',
		[KB.N0] = '}',

		[KB.BACKTICK] = string.char(0xAA),
		[KB.EXTRA1] = '\\',
		[KB.MINUS] = '\\',
		[KB.EQUALS] = '-',
		[KB.LBRACKET] = '[',
		[KB.RBRACKET] = ']',
		[KB.QUOTE] = '\'',
		[KB.BACKSLASH] = '`',

		[KB.COMMA] = ';',
		[KB.DOT] = string.char(0xF9),
		[KB.SLASH] = '~'
	}
}
local scancodesDown = {}

keyboard.KB = KB
keyboard.be = keymapBe

function keyboard.readRawScancode()
	while (ps2.readStatus() & 1) == 0 do
		sys.halt()
	end
	return ps2.readData()
end

function keyboard.waitRawScancode()
	local data
	repeat
		data = keyboard.readRawScancode()
	until data ~= 0x00 and data ~= 0xFF
	return data
end

function keyboard.readScancode()
	local event = {}
	local scancode = keyboard.readRawScancode()

	if scancode == 0xE0 then
		scancode = keyboard.readRawScancode()
		if scancode == 0xF0 then
			event.released = true
			scancode = keyboard.readRawScancode()
		end
		event.scancode = (scancode | 0x80)
	elseif scancode == 0xF0 then
		event.released = true
		event.scancode = keyboard.readRawScancode()
	else
		event.scancode = scancode
	end

	event.pressed = not event.released

	event.shift = keyboard.shiftDown
	event.alt = keyboard.altDown

	if event.scancode == KB.LSHIFT or event.scancode == KB.RSHIFT then
		keyboard.shiftDown = event.pressed
	elseif event.scancode == KB.LALT or event.scancode == KB.RALT then
		keyboard.altDown = event.pressed
	end

	-- Get the actual character
	if event.shift then
		event.char = keymapBe.shift[event.scancode]
			or keymapBe.alt[event.scancode]
			or keymapBe.normal[event.scancode]
	elseif event.alt then
		event.char = keymapBe.alt[event.scancode]
			or keymapBe.normal[event.scancode]
	else
		event.char = keymapBe.normal[event.scancode]
	end

	return event
end

function keyboard.readScancodeNonBlocking()
	if (ps2.readStatus() & 1) == 0 then
		return nil
	else
		return keyboard.readScancode()
	end
end

-- This functions does not yet work
function keyboard.isDown(scancode)
	assert(type(scancode) == "number", "Scancode is not a number")
	--[[
	while (ps2.readStatus() & 1) ~= do
		keyboard.readRawScancode()
	end
	return scancodesDown[scancode]
	--]]
end

function keyboard.readKey()
	local event
	repeat
		event = keyboard.readScancode()
	until event.pressed and event.char
	return event
end

function keyboard.getKey()
	return keyboard.readKey().char
end

local KeyboardFD = {}
function keyboard.init()
	ps2.writeData(0xFF)
	ps2.writeData(0xF0)
	ps2.writeData(2)
	ps2.writeData(0xF4)

	local status = ps2.readStatus()
	while (status & 1) == 1 do
		ps2.readData()
		status = ps2.readStatus()
	end

	module("fs").mkdev("/dev/kbd", KeyboardFD)
end

--
-- ckbd only sends typed characters.
--
function KeyboardFD:stat(stat)
	stat.size = 0
	stat.readKey = keyboard.readKey
	stat.readScancode = keyboard.readScancodeNonBlocking
	stat.scancodes = util.shallowCopy(KB)
	return stat
end

function KeyboardFD:open(mode, read, write, append)
	assert(read and not write and not append, "/dev/kbd must be opened as read-only")

	local fd = {}
	return fd
end

function KeyboardFD:close(fd)
end

function KeyboardFD:write(fd, buffer, start, stop)
	error("Not supported")
end

function KeyboardFD:read(fd, amount, buffer)
	local copied = 0
	while copied < amount do
		self:readEvent(fd, buffer)
		copied = copied + 1
	end
	return copied
end

function KeyboardFD:readEvent(fd, buffer)
	buffer[#buffer + 1] = keyboard.getKey()
end
