local mounts = {}
local mounts = {}
local filesystems = {}
local fs = new.module("fs")

--
-- Functions for interacting with the virtual filesystem
--

--- Mounts a filesystem at a location.
-- @param path The path to mount the filesystem at.
-- @param driver The filesystem type to mount.
-- @param disk The parent disk to read from. (if needed, otherwise null)
-- @param options Extra options to pass to the driver.
-- @raise When the driver argument is not a string or a table, or can't be found.
-- @raise When a filesystem is already mounted at the location.
function fs.mount(path, driver, disk, options)
	assert(type(driver) == "table" or type(driver) == "string",
		"Filesystem type not a string or table")
	if type(driver) == "string" then
		driver = filesystems[driver]
		assert(driver, "Filesystem driver not found")
	end

	assert(not mounts[path], "Path already contains a mount")

	local mountedDriver = driver.mount(disk, options or {})
	assert(mountedDriver.list, "Filesystem has no list function")
	assert(mountedDriver.open, "Filesystem has no open function")
	assert(mountedDriver.close, "Filesystem has no close function")
	assert(mountedDriver.read, "Filesystem has no read function")
	assert(mountedDriver.write, "Filesystem has no write function")
	assert(mountedDriver.seek, "Filesystem has no seek function")
	assert(mountedDriver.stat, "Filesystem has no stat function")
	assert(mountedDriver.mkfile, "Filesystem has no mkfile function")
	assert(mountedDriver.mkdir, "Filesystem has no mkdir function")
	assert(mountedDriver.mkdev, "Filesystem has no mkdev function")
	assert(mountedDriver.rm, "Filesystem has no rm function")
	mounts[path] = mountedDriver
end

function fs.registerFilesystem(name, driver)
	assert(type(name) == "string", "Filesystem name is not a string")
	assert(type(driver) == "table", "Filesystem is not a table")
	assert(driver.mount, "Driver does not have mount function")
	filesystems[name] = driver
end

--
-- Utility functions
--
function fs._getDriver(path, subpath)
	if path == "." then
		error()
	end
	if mounts[path] then
		return mounts[path], fs.purecanon(subpath)
	elseif path == "/" then
		error("/ not mounted")
	else
		local part, _subpath = fs.dirbasename(path)
		subpath = _subpath .. "/" .. subpath
		return fs._getDriver(part, subpath)
	end
end

function fs.getDriver(path)
	path = fs.purecanon(path)
	local drv, npath = fs._getDriver(path, "")
	return drv, npath
end

function fs.dirbasename(path)
	if path == "/" or path == "." then
		return path, path
	elseif not path:find("/") then
		return ".", path
	else
		if path:sub(-1) == "/" then
			path = path:sub(1,-2)
		end
		local dir, base = path:match("^(.*)/(.-)$")
		if #dir == 0 then
			dir = "/"
		end
		return dir, base
	end
end

function fs.purecanon(path)
	local usedParts = {}
	local parts = util.split(path, "/")
	local i = 1
	while i <= #parts do
		local str = parts[i]
		if str == ".." then
			usedParts[#usedParts] = nil
		elseif str ~= "." and str ~= "" and #str > 0 then
			usedParts[#usedParts+1] = str
		end
		i = i + 1
	end
	return "/" .. table.concat(usedParts, "/")
end

function fs.concat(dir, base)
	if base:sub(1,1) == "/" then
		return base
	elseif dir == "/" then
		return dir .. base
	elseif dir == "." then
		return base
	elseif dir:sub(-1) == "/" then
		return dir .. base
	else
		return dir .. "/" .. base
	end
end

--
-- File handle operations
--
local _fh = {}

function _fh:read(bytes, buffer)
	return fs.read(self, bytes, buffer)
end

function _fh:write(data, start, stop)
	return fs.write(self, data, start, stop)
end

function _fh:seek(amount, offset)
	return fs.seek(self, amount, offset)
end

function _fh:close()
	return fs.close(self)
end

--
-- Functions for interacting with filesystems
--
function fs.list(path)
	local driver, subpath = fs.getDriver(path)
	return driver:list(subpath)
end

function fs.open(path, mode)
	if type(mode) ~= "string" then
		sys.log(debug.traceback())
		error("Expected mode to be a string (was " .. tostring(type(mode)) .. ")")
	end
	assert(type(mode) == "string", "File mode is not a string")

	-- Parse the mode argument.
	local firstChar = mode:sub(1,1)
	local secondChar = mode:sub(2,2)
	local canWrite = (firstChar == "w") or (firstChar == "a") or (secondChar == "+")
	local canRead = (mode == "r") or secondChar == "+"
	local canAppend = (firstChar == "a")

	-- Find the driver, and stat the file.
	-- If the file does not exist, and we're in write-mode,
	-- create the file.
	local driver, subpath = fs.getDriver(path)
	local stat, err = driver:stat(subpath)
	if not stat then
		if canWrite then
			local dir, base = fs.dirbasename(subpath)
			local success, err = driver:mkfile(dir, base)
			if not success then
				return nil, err
			else
				stat, err = driver:stat(subpath)
				if not stat then
					return nil, err
				end
			end
		else
			return nil, err
		end
	end

	local object, err
	if stat.type == "file" and not stat.device then
		-- It's a real file
		object, err = driver:open(subpath, mode, canRead, canWrite, canAppend)
	elseif stat.type == "file" then
		-- It's actually a device
		object, err = stat.device:open(mode, canRead, canWrite, canAppend)
		driver = stat.device
	else
		error("Tried to open something that is not a file")
	end

	if object then
		local handle = {
			driver = driver,
			data = object,
			stat = stat,
			canRead = (mode:sub(1,1) == "r") or (mode:sub(2,2) == "+"),
			canWrite = (mode:sub(1,1) == "w") or (mode:sub(2,2) == "+"),
			path = path,
			mode = mode
		}
		for name, value in pairs(_fh) do
			handle[name] = value
		end
		return handle
	else
		return nil, err
	end
end

function fs.close(handle)
	return handle.driver:close(handle.data)
end

function fs.read(handle, bytes, buffer)
	if handle.canRead then

		if type(bytes) ~= "number" then
			if bytes == "*a" then
				bytes = handle.stat.size
			else
				error("Invalid read amount: " .. tostring(bytes))
			end
		end

		if buffer then
			local begin = #buffer
			handle.driver:read(handle.data, bytes, buffer)
			return #buffer - begin
		else
			buffer = {}
			handle.driver:read(handle.data, bytes, buffer)
			local strbuf = {}
			for i, v in ipairs(buffer) do
				strbuf[i] = string.char(v)
			end
			return table.concat(strbuf)
		end
	else
		error("File not opened for reading")
	end
end

function fs.write(handle, data, start, stop)
	if handle.canWrite then
		start = start or 1
		stop = stop or #data

		local buff
		if type(data) == "string" then
			buff = {}
			for i = 1, #data do
				buff[i] = data:sub(i, i):byte()
			end
		elseif type(data) == "table" then
			buff = data
		else
			error("Invalid buffer type for fs.write")
		end
		return handle.driver:write(handle.data, buff, start, stop)
	else
		error("File not opened for writing")
	end
end

function fs.seek(handle, offset, amount)
	offset = offset or "cur"
	amount = amount or 0
	assert(type(offset) == "string", "Offset not a string")
	assert(type(amount) == "number", "Whence not a number")
	if offset ~= "cur" and offset ~= "set" and offset ~= "end" then
		error("Invalid offset")
	end
	return handle.driver:seek(handle.data, offset, amount)
end

function fs.stat(path)
	local driver, subpath = fs.getDriver(path)
	local stat, err = driver:stat(subpath)
	if not stat then
		return nil, err
	end
	if subpath == "/" then
		local _, base = fs.dirbasename(path)
		stat.name = base
		stat.mountpoint = true
	elseif stat.device then
		stat.device:stat(stat)
	end
	assert(stat.type == "file" or stat.type == "dir", "Illegal file type")
	return stat
end

function fs.rm(path)
	local driver, subpath = fs.getDriver(path)
	local stat, err = driver:stat(subpath)
	if not stat then
		return nil, err
	end
	return driver:rm(subpath)
end

function fs.mkfile(path)
	local driver, subpath = fs.getDriver(path)
	local parent, filename = fs.dirbasename(subpath)
	sys.log("MKFILE " .. tostring(parent))
	return driver:mkfile(parent, filename)
end

function fs.mkdir(path)
	local parent, filename = fs.dirbasename(path)
	local driver, subpath = fs.getDriver(parent)
	sys.log("MKDIR " .. tostring(filename))
	return driver:mkdir(subpath, filename)
end

function fs.mkdev(path, device)
	assert(device.open, "Device does not have open function")
	assert(device.close, "Device does not have close function")
	assert(device.read, "Device does not have read function")
	assert(device.stat, "Device does not have stat function")

	local driver, subpath = fs.getDriver(path)
	local parent, filename = fs.dirbasename(subpath)
	sys.log("MKDEV " .. tostring(path))
	return driver:mkdev(parent, filename, device)
end
