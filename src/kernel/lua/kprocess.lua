local KProcess = new.class("KProcess")

local Process = class("Process")
local fs = module("fs")
local lsysi = module("lsysi")
local csysi = module("csysi")

function KProcess.new(path, args, env)
	local process = util.new(KProcess, Process.new("kprocess", path, args, env))

	local fh = fs.open(path, "r")
	local content = fh:read("*a")
	fh:close()

	local env = lsysi.createEnvironment()
	process.func = coroutine.create(function()
		assert(xpcall(function()
			local func = assert(load(content, "=" .. path, "bt", env))
			func(table.unpack(args))
		end, function(err)
			sys.log(debug.traceback())
			return err
		end))
	end)

	return process
end

function KProcess:run()
	local state, err = coroutine.resume(self.func)
	if coroutine.status(self.func) == "dead" then
		self:kill()
		if err then
			local str = "Process terminated unexpectedly: " .. tostring(err)
			sys.log("Process terminated unexpectedly: " .. tostring(err))
			csysi.io.write(self, 3, str .. "\n")
		end
	end
end

function KProcess:kill()
	sys.log("Lua Process terminated")
	Process.kill(self)
end
