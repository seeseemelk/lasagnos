local font = module("font")
local screen = new.module("screen", {
	cursor = {x=1, y=1},
	cursorEnabled = true,
	--size = {width=80, height=25},
	address = 0,
	size = {},
	textSize = {}, -- Contains the number of letters that fit on the screen (width, height)
	address = 0x0,
	font = font,
	charWidth = font.width, -- The width of each pixel, including padding.
	charHeight = font.height, -- The height of each pixel, including padding.
	scale = 1, -- Scales the display by a specified factor.
	useBuffer = false,
	buffer = nil,
	colorFG = 0xDDDDDD,
	colorBG = 0x000000
})

local function setReg(reg, value)
	sys.outb(0x3D4, reg)
	sys.outb(0x3D5, value)
end

local function getReg(reg)
	sys.outb(0x3D4, reg)
	return sys.inb(0x3D5)
end

function screen.rectangle(color, x, y, w, h)
	if screen.scale ~= 1 then
		x = x * screen.scale
		y = y * screen.scale
		w = w * screen.scale
		h = h * screen.scale
	end

	if x + w < 0 or x > screen.realSize.width or y + h < 0 or y > screen.realSize.height then
		return
	end
	x = math.max(math.min(x, screen.realSize.width - 1), 0)
	y = math.max(math.min(y, screen.realSize.height - 1), 0)
	if x + w > screen.realSize.width then
		w = screen.realSize.width - x
	end
	if y + h > screen.realSize.height then
		h = screen.realSize.height - y
	end

	x = x * 3

	local ybegin = y * screen.pitch
	if screen.useBuffer then
		ybegin = ybegin + screen.bufferAddress
	else
		ybegin = ybegin + screen.address
	end

	local yend = ybegin + (h - 1) * screen.pitch

	for iy = ybegin + x, yend + x, screen.pitch do
		sys.memset24(iy, color, w)
	end
end

--
-- This code was taken from http://www.roguebasin.com/index.php?title=Bresenham%27s_Line_Algorithm
--
function screen.line(color, x1, y1, x2, y2)
	local delta_x = x2 - x1
	local ix = delta_x > 0 and 1 or -1
	delta_x = 2 * math.abs(delta_x)

	local delta_y = y2 - y1
	local iy = delta_y > 0 and 1 or -1
	delta_y = 2 * math.abs(delta_y)

	screen.rectangle(color, x1, y1, 1, 1)

	if delta_x >= delta_y then
		local error = delta_y - delta_x / 2

		while x1 ~= x2 do
			if (error > 0) or ((error == 0) and (ix > 0)) then
				error = error - delta_x
				y1 = y1 + iy
			end

			error = error + delta_y
			x1 = x1 + ix

			screen.rectangle(color, x1, y1, 1, 1)
		end
	else
		local error = delta_x - delta_y / 2

		while y1 ~= y2 do
			if (error > 0) or ((error == 0) and (iy > 0)) then
				error = error - delta_y
				x1 = x1 + ix
			end

			error = error + delta_x
			y1 = y1 + iy

			screen.rectangle(color, x1, y1, 1, 1)
		end
	end
end

function screen.flush()
	if screen.useBuffer then
		sys.memcpy(screen.address, screen.bufferAddress, screen.bufferLength)
	end
end

function screen.setBufferEnabled(enabled)
	screen.useBuffer = enabled
end

function screen.moveCursor(x, y)
	if screen.cursorEnabled then
		screen.setCursorEnabled(false)
		screen.cursor.x, screen.cursor.y = x, y
		screen.setCursorEnabled(true)
	else
		screen.cursor.x, screen.cursor.y = x, y
	end
end

function screen.setCursorEnabled(enabled)
	screen.rectangle(enabled and 0xFFFFFF or 0x000000,
		(screen.cursor.x - 1) * screen.charWidth,
		(screen.cursor.y - 1) * screen.charHeight + screen.font.height + 1,
		screen.font.width,
		1)
	screen.cursorEnabled = enabled
end

---[[
local height = screen.font.height
local width = screen.font.width
local rectangle = screen.rectangle

function screen.setChar(x, y, c)
	local char = screen.font[c]
	if not c then
		char = screen.font[" "]
	elseif not char then
		char = screen.font.unknown
		sys.log("Could not find character '" .. tostring(c) .. "'")
	end

	-- Place at x, y
	local px, py = (x-1) * screen.charWidth, (y-1) * screen.charHeight

	local fg, bg = screen.colorFG, screen.colorBG

	local iy = 0
	for y = 1, height do
		for x = 1, width do
			if char[iy+x] == 1 then
				rectangle(char[iy+x]==1 and fg, px + x - 1, py + y - 1, 1, 1)
			end
		end
		iy = iy + width
	end
end
--]]

--[[
function screen.setChar(x, y, c)
	local char = screen.font[c]
	if not c then
		char = screen.font[" "]
	elseif not char then
		char = screen.font.unknown
		sys.log("Could not find character '" .. tostring(c) .. "'")
	end

	-- Place at x, y
	local px, py = (x-1) * screen.charWidth, (y-1) * screen.charHeight

	local fg, bg = screen.colorFG, screen.colorBG

	local iy = 0
	for y = 1, screen.font.height do
		for x = 1, screen.font.width do
			if char[iy+x] == 1 then
				screen.rectangle(char[iy+x]==1 and fg, px + x - 1, py + y - 1, 1, 1)
			end
		end
		iy = iy + screen.font.width
	end
end
--]]

--[[
function screen.getChar(x, y)
	return screen.textBuffer[y][x]
end
--]]

function screen.scroll(amount)
	if amount > 0 then
		screen.setCursorEnabled(false)

		amount = amount * screen.charHeight * screen.scale
		local lines = screen.pitch * amount
		local src = screen.address + lines
		local dest = screen.address
		local copyAmount = (screen.realSize.height - amount) * screen.pitch
		sys.memcpy(dest, src, copyAmount)
		sys.memset(dest + copyAmount, 0, lines)

		screen.setCursorEnabled(true)
	end
end

function screen.pushCursor()
	local newX, newY = screen.cursor.x, screen.cursor.y
	newX = newX + 1
	if newX > screen.textSize.width then
		newX = 1
		newY = newY + 1
		if newY > screen.textSize.height then
			screen.scroll(1)
			newY = newY - 1
		end
	end
	screen.moveCursor(newX, newY)
end

function screen.popCursor()
	local newX, newY = screen.cursor.x, screen.cursor.y
	newX = newX - 1
	if newX == 0 then
		newX = screen.textSize.width
		newY = newY - 1
		if newY == 0 then
			newY = 1
			newX = 1
		end
	end
	screen.moveCursor(newX, newY)
end

function screen.cursorToNextLine()
	local newX, newY = screen.cursor.x, screen.cursor.y
	newX = 1
	newY = newY + 1
	if newY > screen.textSize.height then
		screen.scroll(1)
		newY = newY - 1
	end
	screen.moveCursor(newX, newY)
end

function screen.putChar(c)
	screen.setChar(screen.cursor.x, screen.cursor.y, c)
	screen.pushCursor()
end

function screen.print(str)
	for i = 1, #str do
		local c = str:sub(i, i)
		if c == "\n" then
			screen.cursorToNextLine()
		else
			screen.putChar(c)
		end
	end
end

function screen.setScale(scale)
	screen.scale = scale

	screen.size.width = screen.realSize.width / screen.scale
	screen.size.height = screen.realSize.height / screen.scale
	screen.textSize = {
		width = screen.size.width // screen.charWidth,
		height = screen.size.height // screen.charHeight
	}
	--[[
	for i = 1, screen.textSize.height do
		screen.textBuffer[i] = {}
	end
	--]]
	screen.moveCursor(1, 1)
end

function screen.setTextColor(fg, bg)
	screen.colorFG = fg
	screen.colorBG = bg
end

local ScreenFD = {}

function screen.init()
	-- Get information about the display.
	screen.address, screen.pitch, screen.size.width, screen.size.height, screen.bpp = multiboot.framebuffer()
	screen.realSize = {width = screen.size.width, height = screen.size.height}
	screen.bufferLength = screen.pitch * screen.size.width
	screen.bufferAddress = sys.malloc(screen.bufferLength)
	sys.log("VBE Framebuffer Address: " .. util.hex(screen.address))
	sys.log("VBE Framebuffer Length: " .. screen.bufferLength)
	sys.log("VBE Framebuffer Pitch: " .. screen.pitch)
	sys.log("VBE Framebuffer Mode: " .. screen.size.width .. "x" .. screen.size.height .. "x" .. screen.bpp)

	-- Setup memory map
	local query = paging.query(0, screen.bufferLength, paging.GLOBAL)
	if query then
		paging.assignMany(query.begin, screen.address, util.ceildiv(screen.bufferLength, util.KB(4)))
		screen.address = query.begin
		sys.log("Screen mapped at " .. util.hex(screen.address))
	else
		sys.panic("Could not allocate VGA page")
	end

	-- Setup the text resolution and text buffer.
	screen.setScale(screen.scale)

	-- Finished
	sys.log("VGA Setup Complete")

	-- Initialize screen
	--[[
	sys.inb(0x3DA)

	local cursorLow = getReg(0x0F)
	local cursorHigh = getReg(0x0E)
	local pos = cursorLow + (cursorHigh << 8)
	screen.cursor.y = pos // screen.size.width + 1
	screen.cursor.x = pos % screen.size.width + 1

	screen.moveCursor(screen.cursor.x, screen.cursor.y)
	--]]

	module("fs").mkdev("/dev/vga", ScreenFD)
end

function ScreenFD:stat(stat)
	stat.size = 0
	stat.rectangle = screen.rectangle
	stat.line = screen.line
	stat.resolution = {
		width = screen.size.width,
		height = screen.size.height
	}
	stat.setCursorEnabled = screen.setCursorEnabled
	stat.scale = screen.scale
	stat.setScale = screen.setScale
	stat.bufferEnabled = screen.useBuffer
	stat.setBufferEnabled = screen.setBufferEnabled
	stat.flush = screen.flush
	stat.moveCursor = screen.moveCursor
	stat.write = screen.print
	stat.nextLine = screen.cursorToNextLine
	stat.setTextColor = screen.setTextColor
	return stat
end

function ScreenFD:open(mode, read, write, append)
	local fd = {}
	return fd
end

function ScreenFD:close(fd)
end

function ScreenFD:read(fd, amount, buffer)
	return nil, "Not supported"
end

function ScreenFD:write(fd, buffer, start, stop)
	for i = start, stop do
		if buffer[i] == 0x0A then
			screen.cursorToNextLine()
		elseif buffer[i] ~= 0 then
			screen.putChar(string.char(buffer[i]))
		end
	end
	screen.flush()
end
