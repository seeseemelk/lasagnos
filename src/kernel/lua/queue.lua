local Queue = {}

function Queue.new()
	local queue = {}
	queue.push = Queue.push
	queue.pull = Queue.pull
	queue.hasNext = Queue.hasNext
	return queue
end

function Queue.push(queue, val)
	local obj = {val}
	if not queue.first then
		queue.first = obj
		queue.last = obj
	end
	queue.last[2] = obj
	queue.last = obj
end

function Queue.pull(queue)
	local obj = queue.first
	if obj then
		queue.first = obj[2]
	end
	return obj
end

function Queue.hasNext(queue)
	return queue.first ~= nil
end

return Queue
