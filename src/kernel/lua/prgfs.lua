local prgfs = new.module("prgfs", {
	files = {}
})

local fs = module("fs")

local Mount = {}

function prgfs.mount(disk, options)
	local mount = util.new(Mount)
	return mount
end

function prgfs.addFile(filename, content)
	--local content = requireString(reqName)
	local file = {
		name = filename,
		size = #content,
		content = content
	}
	prgfs.files[file.name] = file
end

function Mount:list(path)
	local list = {}
	for _, file in pairs(prgfs.files) do
		list[#list + 1] = self:rawStat(file)
	end
	return list
end

function Mount:rawStat(file)
	local stat = {
		name = file.name,
		size = file.size,
		type = "file"
	}
	return stat
end

function Mount:stat(path)
	if path == "/" then
		return {
			name = "/",
			type = "dir",
			size = 0
		}
	else
		local _, base = fs.dirbasename(path)
		local file = prgfs.files[base]
		if file then
			return self:rawStat(file)
		else
			return nil, "File not found"
		end
	end
end

function Mount:rm(path)
	return false, "Not supported"
end

function Mount:mkfile(path, filename)
	return false, "Not supported"
end

function Mount:mkdir(path, dirname)
	return false, "Not supported"
end

function Mount:mkdev(path, devname, device)
	return false, "Not supported"
end

function Mount:open(path)
	local _, base = fs.dirbasename(path)
	local file = prgfs.files[base]
	local fh = {
		file = file,
		index = 0
	}
	return fh
end

function Mount:close(fh)
	return true
end

function Mount:read(fh, bytes, buffer)
	bytes = math.min(bytes, fh.file.size - fh.index)
	for i = fh.index + 1, fh.index + bytes do
		buffer[#buffer + 1] = fh.file.content:sub(i, i):byte()
	end
	fh.index = fh.index + bytes
end

function Mount:write(fh, bytes, buffer)
	error("Not supported")
end

function Mount:seek(fh, offset, amount)
	if offset == "set" then
		fh.index = amount
	elseif offset == "cur" then
		fh.index = fh.index + amount
	elseif offset == "end" then
		fh.index = fh.file.size + amount
	end

	fh.index = math.max(math.min(fh.index, fh.file.size), 0)
	return fh.index
end

--[[
rawset(_G, "prgfs", prgfs)
require("prgfs_list")
rawset(_G, "prgfs", nil)
--]]

fs.registerFilesystem("prgfs", prgfs)
