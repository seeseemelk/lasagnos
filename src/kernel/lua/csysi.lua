-- Common system interface.
-- Used by both nsysi and lsysi.
-- Note that this system interface uses Lua's 1-based index system.

local csysi = new.module("csysi", {
	io = {},
	os = {},
	sys = {}
})
local fs = module("fs")
local ps = module("ps")
local kthreads = module("kthreads")

function csysi.io.open(process, path, mode)
	local fd = assert(fs.open(path, mode))
	local id = #process.fd + 1
	sys.log("Opened file " .. path .. " as " .. id)
	process.fd[id] = fd
	return id
end

--
-- IO functions.
--
function csysi.io.close(process, fdid)
	if process.fd[fdid] then
		local fd = process.fd[fdid]
		fd:close()
	else
		error("Inalid fd " .. tostring(fdid))
	end
end

function csysi.io.read(process, fdid, amount, buffer)
	if process.fd[fdid] then
		local fd = process.fd[fdid]
		return fd:read(amount, buffer)
	else
		error("Inalid fd " .. tostring(fdid))
	end
end

function csysi.io.write(process, fdid, buffer, start, stop)
	if process.fd[fdid] then
		local fd = process.fd[fdid]
		return fd:write(buffer, start, stop)
	else
		error("Inalid fd " .. tostring(fdid))
	end
end

function csysi.io.stat(process, file)
	local stat = fs.stat(file)
	--[[
	if stat then
		for name, value in pairs(stat) do
			if type(value) == "table" or type(value) == "function" then
				stat[name] = tostring(value)
			end
		end
	end
	--]]
	return stat
end

function csysi.io.list(process, path)
	local list = fs.list(path)
	if list then
		for _, file in ipairs(list) do
			for name, value in pairs(file) do
				if type(value) == "table" or type(value) == "function" then
					file[name] = tostring(value)
				end
			end
		end
	end
	return list
end

function csysi.io.purecanon(process, path)
	return fs.purecanon(path)
end

--
-- OS functions.
--
function csysi.os.getenv(process, env)
	return process.env[env]
end

function csysi.os.getpenv(process, env)
	if process.parentProcess then
		return process.parentProcess.env[env]
	else
		return nil
	end
end

function csysi.os.setenv(process, env, value)
	process.env[env] = value
end

function csysi.os.setpenv(process, env, value)
	if process.parentProcess then
		process.parentProcess.env[env] = value
	end
end

function csysi.os.execute(process, path, args, env)
	env = env or process.env
	local process = ps.create(path, args, env)
	--kthreads.new(process.run)
	while process.state == "alive" do
		--sys.log("State is: " .. process.state)
		kthreads.yield()
	end
end

function csysi.os.kmod(process, mod)
	local str = "*** WARNING! PROCESS IS ACCESSING KERNEL MODULE '" .. tostring(mod) .. "' ***"
	sys.log(str)
	csysi.io.write(process, 3, str)
	csysi.io.write(process, 3, "\n")
	return module(mod)
end

function csysi.sys.log(process, msg)
	sys.log("[PROCESS:" .. process.pid .. "] " .. msg)
end
