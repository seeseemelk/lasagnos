-- Native System Interface module
-- Note that it uses a 0-based index and converts it to 1-based index for csysi.

local nsysi = new.module("nsysi", {
	namespaceIDs = {}, -- Contains namespace to id mappings
	functionIDs = {}, -- Contains function to id mappings
	namespaces = {}, -- Contains functions,
	numNamespaces = 0
})

local csysi = module("csysi")
local interrupts = module("interrupts")
local scheduler = module("scheduler")
local Mem = class("Mem")

-- Allow acces to system registers of the process.
-- This is done by accessing the values pushed to
-- the stack by pushad in the interrupt handler.
local _registers = {
	__index = {}
}
function nsysi.getRegisters(process)
	local offset = process.tssEsp - 24
	local registers = {
		offset = offset,
		eax = sys.peekd(offset),
		ecx = sys.peekd(offset - 0x04),
		edx = sys.peekd(offset - 0x08),
		ebx = sys.peekd(offset - 0x0C),
		esp = sys.peekd(offset - 0x10),
		ebp = sys.peekd(offset - 0x14),
		esi = sys.peekd(offset - 0x18),
		edi = sys.peekd(offset - 0x1C),
	}
	setmetatable(registers, _registers)
	return registers
end

function _registers.__index:dump()
	sys.log("EAX = " .. util.hex(self.eax,8) .. "\tEBX = " .. util.hex(self.ebx,8))
	sys.log("ECX = " .. util.hex(self.ecx,8) .. "\tEDX = " .. util.hex(self.edx,8))
	sys.log("ESI = " .. util.hex(self.esi,8) .. "\tEDI = " .. util.hex(self.edi,8))
	sys.log("ESP = " .. util.hex(self.esp,8) .. "\tEBP = " .. util.hex(self.ebp,8))
end

function _registers.__index:setEax(value)
	sys.poked(self.offset, value)
end

function _registers.__index:setAh(value)
	sys.poke(self.offset + 1, value)
end

function _registers.__index:setAl(value)
	sys.poke(self.offset, value)
end

-- These functions allow processes to access the native system interface.

function nsysi.nativeInt(irq)
	sys.log("Int " .. util.hex(irq) .. " by process " .. scheduler.lastProcess.pid)
	nsysi.getRegisters(scheduler.lastProcess):dump()
	scheduler.lastProcess.systemCall = irq
end

-- Find a module
function nsysi.handle80(process)
	local registers = nsysi.getRegisters(process)

	local namespace = Mem.parseCString(registers.edx)
	local namespaceID = nsysi.namespaceIDs[namespace] or 0
	registers:setAh(namespaceID)

	return true
end

function nsysi.handle81(process)
	local registers = nsysi.getRegisters(process)

	local namespaceID = (registers.eax >> 8) & 0xFF
	local functionName = Mem.parseCString(registers.edx)
	local functionID = nsysi.functionIDs[namespaceID][functionName] or 0
	registers:setAl(functionID)

	return true
end

function nsysi.handle82(process)
	local registers = nsysi.getRegisters(process)
	registers:dump()

	if registers.eax == 0 then
		sys.log("Process " .. process.pid .. " terminated")
		process:kill()
	else
		local namespaceID = (registers.eax >> 8) & 0xFF
		local functionID = registers.eax & 0xFF
		local func = nsysi.namespaces[namespaceID][functionID]
		if not func then
			error("Native function " .. tostring(namespaceID) .. ":" .. tostring(functionID) .. " not found")
		end
		local value = func(process, registers.edx, registers.ecx, registers.ebx)
		if value then
			if type(value) == "number" then
				registers:setEax(value)
			elseif type(value) == "boolean" then
				registers:setEax(value and 1 or 0)
			else
				error("Unsupported return value")
			end
		end
	end
	return true
end

-- These functions allow the kernel to interact with the native system interface.
function nsysi.init()
	interrupts.addHandler(0x80, nsysi.nativeInt)
	interrupts.addHandler(0x81, nsysi.nativeInt)
	interrupts.addHandler(0x82, nsysi.nativeInt)
end

function nsysi.handleSystemCall(irq, process)
	if irq == 0x80 then
		nsysi.handle80(process)
	elseif irq == 0x81 then
		nsysi.handle81(process)
	elseif irq == 0x82 then
		nsysi.handle82(process)
	end
end

function nsysi.register(namespace, tbl)
	local namespaceID = nsysi.numNamespaces + 1
	nsysi.numNamespaces = nsysi.numNamespaces + 1

	sys.log("Registered native namespace '" .. namespace .. "' as " .. namespaceID)
	nsysi.namespaceIDs[namespace] = namespaceID

	local functionIDs = {}
	local namespace = {}

	nsysi.functionIDs[namespaceID] = functionIDs
	nsysi.namespaces[namespaceID] = namespace

	local functionID = 1
	for name, func in pairs(tbl) do
		if type(func) == "function" then
			functionIDs[name] = functionID
			namespace[functionID] = func
			functionID = functionID + 1
		end
	end
end

--
-- Namespace implementations start here
--

local io = {}

function io.open(process, path, mode)
	local path = Mem.parseCString(path)
	local mode = Mem.parseCString(mode)
	local fd = csysi.io.open(process, path, mode)
	return fd - 1
end

function io.close(process, fd)
	return csysi.io.close(process, fd + 1)
end

function io.write(process, fd, buffer, length)
	local mem = Mem.wrapBlock(buffer, length)
	local length = csysi.io.write(process, fd + 1, mem, 1, length)
	return length
end

function io.read(process, fd, buffer, length)
	local mem = Mem.wrapBlock(buffer, length)
	local read = csysi.io.read(process, fd + 1, mem, length)
	return read
end

local os = {}

function os.getenv(process, env, buffer)
	local env = Mem.parseCString(env)
	local v = csysi.os.getenv(process, env)

	if v then
		v = v .. '\0'
		local mem = Mem.wrapBlock(buffer, 255)
		for i = 1, math.min(#v, #mem) do
			mem[i] = v:sub(i,i)
		end
		return #v
	else
		return 0
	end
end

function os.setenv(process, env, value)
	local env = Mem.parseCString(env)
	local value = Mem.parseCString(value)

	csysi.os.setenv(process, env, value)
end

function os.getpenv(process, env, buffer)
	local env = Mem.parseCString(env)
	local v = csysi.os.getpenv(process, env)

	if v then
		v = v .. '\0'
		local mem = Mem.wrapBlock(buffer, 255)
		for i = 1, math.min(#v, #mem) do
			mem[i] = v:sub(i,i)
		end
		return #v
	else
		return 0
	end
end

function os.setpenv(process, env, value)
	local env = Mem.parseCString(env)
	local value = Mem.parseCString(value)

	csysi.os.setpenv(process, env, value)
end

nsysi.register("io", io)
nsysi.register("os", os)
