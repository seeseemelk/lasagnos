local Elf = new.class("Elf")
local Ident = {
	0x7F, ('E'):byte(), ('L'):byte(), ('F'):byte()
}

function Elf.new(content)
	local elf = util.new(Elf)
	elf.content = content
	elf:readHeader()
	return elf
end

function Elf:readHeader()
	local h = {}
	self.header = h

	local o = 1
	local ident = {}
	for i = 1, 8 do
		ident[i], o = self:readByte(o)
	end
	h.ident = ident

	h.type, o = self:readShort(o)
	h.machine, o = self:readShort(o)
	h.version, o = self:readUInt(o)
	h.addr = self:readUInt(o)
	h.phoff = self:readUInt(o)
	h.shoff = self:readUInt(o)
	h.flags = self:readUInt(o)
	h.ehsize = self:readShort(o)
	h.phentsize = self:readShort(o)
	h.phnum = self:readShort(o)
	h.shentsize = self:readShort(o)
	h.shnum = self:readShort(o)
	h.shstrndx = self:readShort(o)
end

function Elf:readByte(offset)
	return self.content:sub(offset, offset):byte(), offset + 1
end

function Elf:readShort(offset)
	return self:readByte(offset)
	     + (self.readByte(offset + 1) << 8), offset + 2
end

function Elf:readUInt(offset)
	return self:readShort(offset)
	     + (self:readShort(offset + 2) << 16), offset + 4
end

function Elf:readInt(offset)
	local int = self:readUInt(offset)
	if int >= 2^31 then
		return int - 2^32, offset + 4
	else
		return int, offset + 4
	end
end
