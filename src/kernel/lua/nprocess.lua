local NProcess = new.class("NativeProcess", {
	SIGNATURE = "\xEB\x03LOS"
})

local fs = module("fs")
local nsysi = module("nsysi")
local csysi = module("csysi")
local Process = class("Process")
local Mem = class("Mem")

local MemSpace = {}
local lastNativeProcess = nil

function NProcess.new(path, args, env)
	local process = util.new(NProcess, Process.new("native", path, args, env))
	process.systemCall = nil

	-- Get the file size
	local size = assert(fs.stat(path)).size

	-- Create memory space
	local pages = util.ceildiv(size, 0x1000) + 1
	paging.load(paging.SYSTEM_PAGE_DIRECTORY)
	process.mem = MemSpace.new()
	process.mem:allocateAt(0x1000, pages)

	-- Load file
	local buff = Mem.wrap(0x1000, util.KB(pages*4))
	local fh = fs.open(path, "r")
	fh:read("*a", buff)
	fh:close()

	-- Create a thread
	process.thread = threads.createUser()

	-- Prepare for first execution
	process.tssEsp = threads.getRegisters(process.thread).esp

	lastNativeProcess = nil

	return process
end

function NProcess:run()
	if self.state ~= "alive" then
		error("Process state is " .. self.state)
	end

	if lastNativeProcess ~= self then
		self.mem:switch()
		sys.setReturnpoint(self.tssEsp)
	end

	if self.systemCall then
		xpcall(function()
			sys.log("Process " .. self.pid .. " executed IRQ " .. util.hex(self.systemCall))
			if lastNativeProcess ~= self then
				self.mem:switch()
				lastNativeProcess = self
			end
			nsysi.handleSystemCall(self.systemCall, self)
			self.systemCall = nil
		end, function(err)
			local str = "Process terminated unexpectedly: " .. tostring(err)
			sys.log("Process terminated unexpectedly: " .. tostring(err))
			sys.log(debug.traceback())
			csysi.io.write(self, 3, str .. "\n")
			self:kill()
		end)
	else
		if lastNativeProcess ~= self then
			self.mem:switch()
			sys.setReturnpoint(self.tssEsp)
			lastNativeProcess = self
		end
		sys.log("Entering thread of process " .. self.pid)
		threads.switch(self.thread)
		sys.log("Exited thread")
	end
end

function NProcess:kill()
	-- Remove the thread
	threads.free(self.thread)

	Process.kill(self)

	-- Remove the memory space
	self.mem:remove()

	lastNativeProcess = nil
end

function MemSpace.new()
	local mem, phys = util.new(MemSpace)
	mem.dir, phys = paging.copy()
	return mem
end

function MemSpace:switch()
	paging.load(self.dir)
end

function MemSpace:allocateAt(vaddr, pages)
	self:switch()
	paging.allocate(vaddr, pages)
	paging.setAttributes(vaddr, pages, paging.USER)
	paging.reloadAll()
end

function MemSpace:remove()
	paging.load(paging.SYSTEM_PAGE_DIRECTORY)
	paging.freeDir(self.dir)
end

--[[
--- Native process module
local nprocess = {}
nprocess.lastProcess = nil
local MemSpace = {}
local NProcess = {}

function MemSpace:init()
	self.dir = paging.copy()
end

function MemSpace:switch()
	paging.load(self.dir)
end

function MemSpace:allocateAt(vaddr, pages)
	self:switch()
	paging.allocate(vaddr, pages)
	paging.setAttributes(vaddr, pages, paging.USER)
	paging.reloadAll()
end

function nprocess.createMemorySpace()
	local ms = util.shallowCopy(MemSpace)
	ms:init()
	return ms
end

function nprocess.loadFile(file, address)
	-- Create the memory space.
	sys.log("Allocting program memory space")
	local mem = nprocess.createMemorySpace()
	mem:allocateAt(address, 100)

	-- Load the file into memory.
	sys.log("Loading file into memory")
	local buff = Mem.wrap(address, util.KB(2*4))
	local fh = fs.open(file, "r")
	fh:read("*a", buff)
	fh:close()

	-- Prepare a stack
	--mem:allocateAt(address - 0x1000, 1)

	-- Create a thread object.
	--local thread = threads.createSpecial(address, address + 1024, 8, 16, 16) --, 24, 32, 32)
	sys.log("Creating user thread")
	local thread = threads.createUser()

	-- Create a Lua object.
	sys.log("Creating lua object")
	local np = util.shallowCopy(NProcess)
	np.file = file
	np.address = address
	np.thread = thread
	np.mem = mem
	np.tssEsp = threads.getRegisters(np.thread).esp
	return np
end

function NProcess:run()
	print("Entering thread")
	if nprocess.lastProcess ~= self then
		self.mem:switch()
		sys.setReturnpoint(self.tssEsp)
	end
	threads.switch(self.thread)
	nprocess.lastProcess = self
	print("Returned from thread")
end

local ADDR = 0x40000000
function nprocess.runTest()
	local p = nprocess.loadFile("/bin/test", 0x0000000)
	p:run()
	p:run()
	while true do end
	--p:run()
	--[[
	local memSpaceA = nprocess.createMemorySpace()
	memSpaceA:allocateAt(ADDR, 16)

	local memSpaceB = nprocess.createMemorySpace()
	memSpaceB:allocateAt(ADDR, 1)

	memSpaceA:switch()
	sys.poke(ADDR, 0x55)
	assert(sys.peek(ADDR) == 0x55, "Test A failed")
	sys.poke(ADDR, 0x22)
	assert(sys.peek(ADDR) == 0x22, "Test B failed")

	memSpaceB:switch()
	sys.poke(ADDR, 0xEE)
	assert(sys.peek(ADDR) == 0xEE, "Test C failed")

	memSpaceA:switch()
	assert(sys.peek(ADDR) == 0x22, "Test D failed, was: " .. util.hex(sys.peek(ADDR)))
	--]]
--end

--modules.register("nprocess", nprocess)
