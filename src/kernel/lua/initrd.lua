local initrd = new.module("initrd")
local prgfs = module("prgfs")
local Mem = class("Mem")

function initrd.parseNumber(mem, offset, length)
	local num = 0

	for i = 1, length do
		local value = mem[offset + i]
		if value >= string.byte('0') and value <= string.byte('7') then
			value = value - string.byte('0')
			num = num * 8 + value
		end
	end

	return num
end

function initrd.parseHeader(mem, offset)
	local header = {}

	local name = {}
	for i = 1, 100 do
		local c = mem[offset + i]
		if c ~= 0 then
			name[#name + 1] = string.char(c)
		end
	end
	header.name = table.concat(name)
	header.size = initrd.parseNumber(mem, offset + 124, 12)
	header.content = util.toString(mem, offset + 513, header.size + offset + 512)

	return header
end

function initrd.parseHeaders(mem, size)
	local headers = {}

	local offset = 0
	while mem[offset + 1] ~= 0 do
		local header = initrd.parseHeader(mem, offset)
		headers[#headers + 1] = header
		local records = util.ceildiv(header.size, 512)
		offset = offset + (records + 1) * 512
	end

	return headers
end

function initrd.parsePRGFSTar(mod)
	local size = mod.modEnd - mod.modStart
	local mem = Mem.wrap(mod.modStart, size)

	local headers = initrd.parseHeaders(mem, size)

	for _, header in ipairs(headers) do
		sys.log("Found file in initrd: " .. header.name)
		prgfs.addFile(header.name, header.content)
	end
end

function initrd.init()
	local begin, count = multiboot.modules()
	begin = begin + util.GB(3)

	local module = multiboot.module(begin)
	module.modStart = module.modStart + util.GB(3)
	module.modEnd = module.modEnd + util.GB(3)

	initrd.parsePRGFSTar(module)
end
