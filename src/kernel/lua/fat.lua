local fsfat = {}
local Fat = {}
local Stream = {}

function fsfat.mount(disk, options)
	assert(type(disk) == "string", "No disk passed to FAT driver")
	local rw = false
	for _, v in ipairs(options) do
		if v == "rw" then
			rw = true
		else
			error("Unsupported option " .. v)
		end
	end
	local fat = Fat.new(disk, rw)
	return fat
end

function Fat.new(disk, rw)
	local fat = {
		disk = fs.open(disk, rw and "r+" or "r"),
		dirLookup = {["/"] = 2}
	}
	for name, value in pairs(Fat) do
		fat[name] = value
	end

	fat:loadHeader()
	return fat
end

function Fat:loadHeader()
	-- Read the FAT header at the beginning.
	self.disk:seek("set")
	local buffer = {}
	self.disk:read(512, buffer)

	-- Parse the header.
	local header = {
		oemName = util.toString(buffer, 4, 11),
		bytesPerSector = util.toShort(buffer, 12),
		sectorsPerCluster = buffer[14],
		numReservedSectors = util.toShort(buffer, 15),
		numFatCopies = util.toShort(buffer, 17),
		numRootDirEntries = util.toShort(buffer, 18),
		numHiddenSectors = util.toShort(buffer, 29),
		totalSectors = util.toInt(buffer, 33),
		sectorsPerFat = util.toInt(buffer, 37),
		firstClusterOfRoot = util.toInt(buffer, 45),
	}

	-- Calculate certain extra values.
	header.dataStart = ((header.numFatCopies * header.sectorsPerFat) + header.numReservedSectors) * header.bytesPerSector
	header.dataSectors = header.totalSectors - header.numReservedSectors - header.numFatCopies * header.sectorsPerFat
	header.totalClusters = header.dataSectors // header.sectorsPerCluster
	header.bytesPerCluster = header.bytesPerSector * header.sectorsPerCluster
	if buffer[67] == 0x29 then
		header.serialNumber = util.toInt(buffer, 68)
		header.label = util.toInt(buffer, 72)
	end

	-- Make sure it really is a FAT32 table.
	assert(header.bytesPerSector == 512, "Bad number of bytes per sector")
	assert(header.numReservedSectors == 32, "Not FAT32")

	-- Save it.
	self.header = header

	-- Reserve FAT entries.
	self.fat = {}
	for i = 1, header.sectorsPerFat do
		self.fat[i] = false
	end
end

--- Formats a filename and adds it to a buffer.
function Fat:formatFilename(buffer, name)
	local start, ext
	if name == "." or name == ".." then
		start = name
		ext = ""
	elseif name:find(".") then
		start = name:sub(1, name:find(".") - 1)
		ext = name:sub(name:find(".") + 1)
	else
		start = name
		ext = ""
	end

	if #start > 8 then
		error("Filename too long")
	else
		start = start:upper() .. (" "):rep(8 - #start)
	end

	if #ext > 3 then
		error("Filename too long")
	else
		ext = ext:upper() .. (" "):rep(3 - #ext)
	end

	for i = 1, 8 do
		buffer[#buffer + 1] = start:sub(i, i):byte()
	end
	for i = 1, 3 do
		buffer[#buffer + 1] = ext:sub(i, i):byte()
	end
end

--- Sets an entry at an index in the table to something new.
-- @param stream The directory stream to add it to.
-- @param entryLocation The index in the directory to save.
-- @param[opt] entry The entry to add. If not passed, a pure zero entry will be made.
function Fat:setEntry(stream, entryLocation, entry)
	local entryData = {}

	-- If we got an entry, format it,
	-- otherwise fill the buffer with 0s.
	if entry then
		self:formatFilename(entryData, entry.name)

		-- Store file attributes
		sys.log("Creating " .. entry.type)
		if entry.type == "dir" then
			entryData[#entryData + 1] = 0x30
		elseif entry.type == "file" then
			entryData[#entryData + 1] = 0x20
		else
			error("Invalid entry type")
		end

		entryData[#entryData + 1] = 0 -- DIR_NTRes
		
		entryData[#entryData + 1] = 0 -- DIR_CrtTimeTenth
		
		entryData[#entryData + 1] = 0 -- DIR_CrtTime
		entryData[#entryData + 1] = 0

		entryData[#entryData + 1] = 0 -- DIR_CrtDate
		entryData[#entryData + 1] = 0

		entryData[#entryData + 1] = 0 -- DIR_LstAccDate
		entryData[#entryData + 1] = 0

		-- High word of first data cluster
		entryData[#entryData + 1] = (entry.start >> 16) & 0xFF
		entryData[#entryData + 1] = (entry.start >> 24) & 0xFF

		entryData[#entryData + 1] = 0 -- DIR_WrtTime
		entryData[#entryData + 1] = 0

		entryData[#entryData + 1] = 0 -- DIR_WrtDate
		entryData[#entryData + 1] = 0

		-- Low word of first data cluster
		entryData[#entryData + 1] = entry.start & 0xFF
		entryData[#entryData + 1] = (entry.start >> 8) & 0xFF

		-- File size
		entryData[#entryData + 1] = entry.size & 0xFF
		entryData[#entryData + 1] = (entry.size >> 8) & 0xFF
		entryData[#entryData + 1] = (entry.size >> 16) & 0xFF
		entryData[#entryData + 1] = (entry.size >> 24) & 0xFF
	else
		for i = 1, 32 do
			entryData[i] = 0
		end
	end

	assert(#entryData == 32, "Length is: " .. #entryData)

	-- Check if we actually have space for the entry.
	-- If not, add a cluster.
	if (entryLocation - 1) * 32 >= #stream.clusters * self.header.bytesPerCluster then
		sys.log("Need to append cluster")
		self.stream:appendCluster()
	end

	stream:seek((entryLocation - 1) * 32)
	self.disk:write(entryData)
end

--- Adds an entry to a directory.
-- @param stream The directory stream to add it to.
-- @param entry The entry to add.
function Fat:addEntryToDirectory(stream, entry)
	-- Find a suitable location.
	local rawContent = self:rawList(stream.clusters[1])
	local entryLocation = #rawContent + 1
	for i, v in ipairs(rawContent) do
		if v.deleted then
			entryLocation = i
			break
		end
	end

	self:setEntry(stream, entryLocation, entry)
end

--- Get the value of a cluster in a FAT.
-- @param num The cluster index.
-- @return The value of the cluster.
function Fat:getFATEntry(num)
	-- Find the location of the entry.
	local sectorNum = (num * 4) // self.header.bytesPerSector
	local sectorOffset = num % self.header.bytesPerSector

	if not self.fat[sectorNum + 1] then
		-- Read the sector.
		local offset = (self.header.numReservedSectors + sectorNum) * self.header.bytesPerSector
		local buffer = {}
		local fat = {}
		self.disk:seek("set", offset)
		self.disk:read(512, buffer)

		-- Parse data
		for i = 1, #buffer, 4 do
			fat[#fat + 1] = util.toInt(buffer, i) & 0x0FFFFFFF
		end
		self.fat[sectorNum + 1] = fat
	end

	return self.fat[sectorNum + 1][sectorOffset + 1]
end

--- Set a new value for a cluster in the FAT.
-- @param num The index of the cluster to change.
-- @param value The new value to store.
function Fat:setFATEntry(num, value)
	-- Find the location of the entry.
	local sectorNum = (num * 4) // self.header.bytesPerSector
	local sectorOffset = num % self.header.bytesPerSector

	-- If we have a cached version of the FAT entry, change it.
	if self.fat[sectorNum + 1] then
		self.fat[sectorNum + 1][sectorOffset + 1] = value
	end

	-- Seek to the location to change.
	local offset = (self.header.numReservedSectors + sectorNum) * self.header.bytesPerSector + sectorOffset * 4

	for i = 1, self.header.numFatCopies do
		self.disk:seek("set", offset + (i - 1) * self.header.sectorsPerFat * self.header.bytesPerSector)
		self.disk:write(util.intToStr(value))
	end
end

--- Gets a stream for a specific cluster.
-- If no parameter is passed, a new stream will be made.
-- @param[opt] The stream to get.
function Fat:getStream(num)
	return Stream.new(self, num)
end

--- Finds the starting cluster for the path and
-- gets the stream for the cluster.
-- It will also cache the value for later reuse.
-- @param path The path of the stream.
-- @return stream The stream for the directory or file.
-- @return nil If it path cannot be found.
function Fat:findStream(path)
	if self.dirLookup[path] then
		return self:getStream(self.dirLookup[path])
	else
		local dir, base = fs.dirbasename(path)
		local contents = self:rawList(dir)
		for _, entry in ipairs(contents) do
			if entry.name == base then
				-- We store everything (even files), we change the cache if the cluster changes.
				self.dirLookup[path] = entry.start
				return self:getStream(entry.start)
			end
		end
	end
	error("File not found")
end

function Stream.new(fat, num)
	-- Initialise the stream.
	local stream = util.shallowCopy(Stream)
	stream.fat = fat

	local clusters = {}
	stream.clusters = clusters

	-- If we're opening an existing stream, load it;
	-- Otherwise, we create a new one.
	if num then 
		clusters[1] = num
	else
		clusters[1] = assert(Stream.findEmptyCluster(fat))
		fat:setFATEntry(clusters[1], 0x0FFFFFFF)
	end

	-- Load the cluster list.
	local nextCluster = clusters[1]
	while true do
		nextCluster = fat:getFATEntry(nextCluster)
		if nextCluster == 0 or nextCluster >= 0x0FFFFFF8 then
			break
		else
			clusters[#clusters + 1] = nextCluster
		end
	end

	-- Return the stream
	return stream
end

--- Searches for an empty cluster.
-- @param fat The FAT to search it on.
-- @treturn int An unused cluster.
function Stream.findEmptyCluster(fat)
	for i = 0, fat.header.totalClusters do
		local cluster = fat:getFATEntry(i)
		if cluster == 0 then
			return i
		end
	end
	return nil, "No free clusters"
end

--- Seeks to a location in this stream.
-- If only a clusterIndex is passed, it will be interpreted as a byte
-- offset into the stream.
-- @param clusterIndex The index of the cluster in this stream.
-- @param[opt] offset The number of bytes to seek into the cluster.
function Stream:seek(clusterIndex, offset)
	if not offset then
		offset = clusterIndex % self.fat.header.bytesPerCluster
		clusterIndex = clusterIndex // self.fat.header.bytesPerCluster
	end
	local cluster = self.clusters[clusterIndex + 1] - 2
	local sector = cluster * self.fat.header.bytesPerCluster + offset + self.fat.header.dataStart
	self.fat.disk:seek("set", sector)
end

--- Removes a cluster from this stream.
-- @tparam clusterIndex The index of the cluster to remove.
function Stream:removeCluster(clusterIndex)
	-- Remove the cluster.
	local newClusterValue = 0x0FFFFFFF
	self.fat:setFATEntry(self.clusters[clusterIndex], 0)

	-- If we're not removing the last cluster, we shouldn't place an end-of-file marker.
	if clusterIndex < #self.clusters then
		newClusterValue = self.clusters[clusterIndex - 1]
	end

	-- Unless we're removing the first cluster,
	-- we should link the previous cluster to the new one (or set an end-of-file marker).
	if clusterIndex > 1 then
		self.fat:setFATEntry(self.clusters[clusterIndex - 1], newClusterValue)
	end
end

--- Appends a new cluster to the stream.
function Stream:appendCluster()
	-- Let's find a free cluster.
	local freeCluster = assert(Stream.findEmptyCluster(self.fat))

	-- Append it.
	self.fat:setFATEntry(freeCluster, 0x0FFFFFFF)
	self.fat:setFATEntry(self.clusters[#self.cluster], freeCluster)
end

--- Removes all clusters, except the first.
function Stream:clear()
	-- Set the first cluster to end-of-file.
	self.fat:setFATEntry(self.clusters[1], 0x0FFFFFFF)

	-- Remove all other clusters
	for i = #self.clusters, 2, -1 do
		self.fat:setFATEntry(self.clusters[i], 0)
		self.clusters[i] = nil
	end
end

--- Removes the entire stream.
-- After this, the stream cannot be used again.
function Stream:remove()
	for i, v in ipairs(self.clusters) do
		self.fat:setFATEntry(v, 0)
	end
end

--- List the contents of a directory, but does not filter things such as LFN.
-- @param path The path of the directory to get, or a stream cluster number.
function Fat:rawList(path)
	-- Get the stream.
	local stream
	if type(path) == "string" then
		stream = self:findStream(path)
	else
		stream = self:getStream(path)
	end

	-- Read directory contents
	local contents = {}

	-- Check if we still need to read more
	local directoryIndex = 1
	local clustersRead = 0
	local reachedEnd = false
	while not reachedEnd and clustersRead < #stream.clusters do
		-- Seek to the right location and perform a read.
		stream:seek(clustersRead, 0)
		local buffer = {}
		self.disk:read(self.header.bytesPerCluster, buffer)

		-- Parse the buffer.
		for i = 1, self.header.bytesPerCluster, 32 do
			local file = self:rawStat(buffer, i)
			if file then
				file.index = directoryIndex
				directoryIndex = directoryIndex + 1
				contents[#contents + 1] = file
			else
				reachedEnd = true
				break
			end
		end

		-- Increment the number of clusters read.
		clustersRead = clustersRead + 1
	end

	-- Return the contents.
	return contents
end

function Fat:list(path)
	local rawContents = self:rawList(path)
	local contents = {}
	for _, file in ipairs(rawContents) do
		if not file.lfn and not file.deleted and file.name ~= "." and file.name ~= ".." then
			contents[#contents + 1] = file
		end
	end
	return contents
end

--- Stats a file in a buffer containing a directory entry.
function Fat:rawStat(buffer, i)
	-- Check if it isn't an empty or deleted entry.
	if buffer[i] == 0 then
		return nil, "empty"
	end

	-- Parse the entry.
	local attributes = buffer[i + 11]
	local file = {
		nameOnly = util.toString(buffer, i, i+7),
		extension = util.toString(buffer, i+8, i+10),
		start = util.toShort(buffer, i+26) +
		(util.toShort(buffer, i+20) << 16),
		size = util.toInt(buffer, i+28),
		readOnly = (attributes & 0x01) > 0,
		hidden = (attributes & 0x02) > 0,
		system = (attributes & 0x04) > 0,
		volumeId = (attributes & 0x08) > 0,
		directory = (attributes & 0x10) > 0,
		archive = (attributes & 0x20) > 0,
		lfn = (attributes & 0x0F) > 0,
		deleted = (buffer[i] == 0xE5)
	}

	if file.directory then
		file.type = "dir"
	else
		file.type = "file"
	end

	local trimmedExt = util.trim(file.extension)
	if #trimmedExt > 0 then
		file.name = util.trim(file.nameOnly) .. "." .. trimmedExt
	else
		file.name = util.trim(file.nameOnly)
	end
	file.name = file.name:lower()

	return file
end

function Fat:stat(path)
	if path == "/" then
		return {
			type = "dir",
			size = 0,
			name = "/",
			filesystem = "FAT32"
		}
	end

	local dir, base = fs.dirbasename(path)
	local contents = self:rawList(dir)
	for _, v in ipairs(contents) do
		if v.name == base then
			return v
		end
	end

	return nil, "File not found"
end

function Fat:rm(path)
	local stat, err = self:stat(path)

	-- Check if the file exists.
	if not stat then
		return false, err
	end

	-- Check if it's empty (if it's a directory)
	if stat.type == "dir" then
		local contents, err = self:list(path)
		if not contents then
			return false, err
		elseif #contents > 0 then
			return false, "Directory not empty"
		end
	end

	-- Get the cluster stream and remove it.
	local stream = self:findStream(path)
	stream:remove()

	-- Remove the file entry.
	local dir, base = fs.dirbasename(path)
	local dirStream = self:findStream(dir)
	local contents = self:rawList(dir)
	for i, v in ipairs(contents) do
		if v.name == base then
			v.name = "\xE5" .. v.name:sub(2)
			self:setEntry(dirStream, i, v)
			break
		end
	end

	-- Remove the path from the cache
	self.dirLookup[path] = nil

	return true
end

function Fat:mkfile(path, filename)
	-- Check if the file doesn't already exist.
	local stat = self:stat(fs.concat(path, filename))
	if stat then
		return false, "Path exists"
	end

	-- Open the directory stream.
	local dirStream = self:findStream(path)

	-- Create a new stream for the file.
	local stream = self:getStream()

	-- Create the entry to add.
	local entry = {
		type = "file",
		size = 0,
		name = filename,
		start = stream.clusters[1]
	}

	-- Save the entry.
	self:addEntryToDirectory(dirStream, entry)

	return true
end

function Fat:mkdir(path, dirname)
	-- First we find the old directory.
	local oldDir = self:findStream(path)

	-- First we allocate a new cluster.
	local newDir = self:getStream()

	-- Add the '.' and '..' entries.
	local entry = {
		type = "dir",
		size = 0,
		name = ".",
		start = newDir.clusters[1]
	}

	self:setEntry(newDir, 1, entry)
	entry.name = ".."
	if path == "/" then
		entry.start = 0
	else
		entry.start = oldDir.clusters[1]
	end
	self:setEntry(newDir, 2, entry)
	self:setEntry(newDir, 3)

	-- And add the entry for the new directory itself.
	entry.name = dirname
	entry.start = newDir.clusters[1]
	self:addEntryToDirectory(oldDir, entry)
end

function Fat:mkdev(path, devname, device)
	return false, "Not supported"
end

function Fat:open(path, mode, read, write, append)
	-- Check if the file exists. (and that it is a file)
	local stat, err = self:stat(path)
	if not stat then
		return stat, err
	elseif stat.type ~= "file" then
		return nil, "Not a file"
	end

	-- Create the file descriptor.
	local fd = {
		stream = self:findStream(path),
		stat = stat,
		position = 0,
	}

	-- If opened for any type of writing, get the parent stream.
	if write then
		local parent = fs.dirbasename(path)
		fd.parentStream = self:findStream(parent)
	end

	-- If opened for writing, but not appending, clear the file stream.
	-- Else, if it is opened for appending, move the file pointer.
	if write and not append then
		fd.stream:clear()
	elseif append then
		fd.position = stat.size
	end

	-- Return the file descriptor.
	return fd
end

function Fat:close(fd)
	-- If the file was changed, save the new size.
	if fd.changed then
		self:setEntry(fd.parentStream, fd.stat.index, fd.stat)
	end
	return true
end

function Fat:seek(fd, offset, amount)
	if offset == "set" then
		fd.position = amount
	elseif offset == "cur" then
		fd.position = fd.position + amount
	elseif offset == "end" then
		fd.position = fd.stat.size + amount
	end

	fd.position = math.max(math.min(fd.position, fd.stat.size), 0)
end

function Fat:read(fd, amount, buffer)
	-- Check that we can actually read this much.
	amount = math.min(amount, fd.stat.size - fd.position)

	-- We must only read in blocks of clusters.
	while amount > 0 do
		local bytesLeft = self.header.bytesPerCluster - (fd.position % self.header.bytesPerCluster)
		local read = math.min(amount, bytesLeft)
		fd.stream:seek(fd.position)
		self.disk:read(read, buffer)
		amount = amount - read
		fd.position = fd.position + read
	end
end

function Fat:write(fd, buffer)
	local start = 1
	while start <= #buffer do
		-- Check how much we should write in this cluster.
		local toWrite = math.min(self.header.bytesPerCluster - fd.position, #buffer)

		-- Check if the cluster exists. If not, it should be added.
		local cluster = fd.position % self.header.bytesPerCluster
		if cluster + 1 > #fd.stream.clusters then
			fd.stream:appendCluster()
		end

		-- Write to the cluster.
		fd.stream:seek(fd.position)
		self.disk:write(buffer, start, start + toWrite - 1)

		-- Increase write count
		start = start + toWrite
	end

	fd.changed = true

	fd.position = fd.position + start - 1
	fd.stat.size = math.max(fd.stat.size, fd.position)

	return start - 1
end

fs.registerFilesystem("fat", fsfat)
