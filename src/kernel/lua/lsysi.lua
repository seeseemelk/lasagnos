local lsysi = new.module("lsysi")

local csysi = module("csysi")
local scheduler = module("scheduler")
local fs = module("fs")

local _lsysi = {
	-- LasagnOS libraries
	io = {},
	util = {},

	-- Standard libraries
	table = table,
	string = string,
	math = math,
	_VERSION = _VERSION,
	pairs = pairs,
	ipairs = ipairs,
	tostring = tostring,
	tonumber = tonumber
}

function lsysi.copy(dest, src)
	for name, value in pairs(src) do
		if type(value) == "table" then
			dest[name] = {}
			lsysi.copy(dest[name], value)
		else
			dest[name] = value
		end
	end
end

function lsysi.createEnvironment()
	local env = {}
	lsysi.copy(env, _lsysi)
	return env
end

function lsysi.init(dest, src)
	dest = dest or _lsysi
	src = src or csysi

	for name, value in pairs(src) do
		if type(value) == "table" then
			dest[name] = dest[name] or {}
			lsysi.init(dest[name], value)
		elseif not dest[name] then
			if type(value) == "function" then
				dest[name] = function(...)
					return value(scheduler.lastProcess, ...)
				end
			else
				dest[name] = value
			end
		end
	end
end

function _lsysi.print(...)
	local str = table.concat({...}, "\t")
	local process = scheduler.lastProcess
	csysi.io.write(process, 2, str)
	csysi.io.write(process, 2, "\n")
end

function _lsysi.io.write(buffer)
	return csysi.io.write(scheduler.lastProcess, 2, buffer)
end

function _lsysi.io.read(fdid, amount, buffer)
	fdid = fdid or 1

	if not amount or amount == "*l" then
		buffer = buffer or {}
		local process = scheduler.lastProcess
		repeat
			csysi.io.read(process, 1, 1, buffer)
			csysi.io.write(process, 2, buffer[#buffer])
		until buffer[#buffer] == "\n"
		buffer[#buffer] = nil
		return table.concat(buffer)
	else
		return csysi.io.read(process, fdid, amount, buffer)
	end
end

function _lsysi.io.concat(...)
	return fs.concat(...)
end

function _lsysi.util.split(str, delimiter)
	return util.split(str, delimiter)
end
