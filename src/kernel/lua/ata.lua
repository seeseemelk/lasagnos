local fs = module("fs")

--- ATA Hardrive Driver
-- @module ata
local Ata = {
	selectedDrive = 0, -- The drive number of the currently selected drive.
	ioAddress = 0x1F0, -- Address for the IO ports
	controlAddress = 0x3F6 -- Address for the Control ports
}

--- Instantiates a new instance of the ATA driver.
-- @tparam int ioBase The address of the IO port. (Range 0x0001 - 0xFFFF, inclusive)
-- @tparam int controlBase The address of the Control port. (Range 0x0001 - 0xFFFF, inclusive)
function Ata.new(ioBase, controlBase)
	local obj = {}
	for name, value in pairs(Ata) do
		obj[name] = value
	end
	obj.ioAddress = ioBase
	obj.controlAddress = controlBase
	return obj
end

--- Set the sector count and start address for the next operation.
-- If one wants to read 256 sectors, one should normally set count to 0,
-- this function however takes care of that.
-- It will also send a drive select command to the controller.
-- @tparam int count The number of sectors to transfer, between 1 and 256 (inclusive).
-- @tparam int lbaLow The first 8 bits of the start address, or the entire LBA address.
-- @tparam[opt] int lbaMid The next 8 bits of the start address.
-- @tparam[opt] int lbaHigh The next 8 bits of the start address.
-- @tparam[opt] int lbaVH The final 4 bits of the start address.
-- @raise Count is not between 1 and 256 (inclusive)
function Ata:setSectors(count, lbaLow, lbaMid, lbaHigh, lbaVH)
	assert(count >= 1 and count <= 256, "Count is not between 1 and 256 (inclusive)")
	if count == 256 then
		count = 0
	end
	if not lbaMid then
		lbaVH = (lbaLow >> 24) & 0x0F
		lbaHigh = (lbaLow >> 16)
		lbaMid = (lbaLow >> 8) & 0xFF
		lbaLow = (lbaLow & 0xFF)
	end
	sys.log("LBA LOW/MID/HIGH/VH COUNT: " .. lbaLow .. "/" .. lbaMid .. "/" .. lbaHigh .. "/" .. lbaVH .. " " .. count)
	self:sendDriveSelect(lbaVH)
	sys.outb(self.ioAddress + 2, count)
	sys.outb(self.ioAddress + 3, lbaLow)
	sys.outb(self.ioAddress + 4, lbaMid)
	sys.outb(self.ioAddress + 5, lbaHigh)
end

--- Selects a drive, but will not wait for it to be selected.
-- It will also set the LBA mode bits.
-- @tparam int data A byte of data to OR with the drive select bit.
-- @raise The currently selected drive number is invalid.
function Ata:sendDriveSelect(data)
	if self.selectedDrive == 1 then
		data = data | 0xE0
	elseif self.selectedDrive == 2 then
		data = data | 0xF0
	else
		error("Bad drive selected")
	end
	sys.outb(self.ioAddress + 6, data)
end

--- Selects a drive, and will wait for it to be selected.
-- If the drive is already selected, it will do nothing.
-- @tparam int drive The drive to select. (1 = primary, 2 = secondary)
-- @raise An invalid drive was selected.
function Ata:selectDrive(drive)
	if drive ~= 1 and drive ~= 2 then
		error("Invalid drive number")
	end
	if drive ~= self.selectedDrive then
		self.selectedDrive = drive
		self:sendDriveSelect(0)
	end
end

--- Sends a command to the drive.
-- @tparam int command The command to send.
-- @raise The command is not a byte.
function Ata:sendCommand(command)
	assert(command >= 0 and command <= 256, "Illegal command")
	while self:readStatus().bsy do end
	sys.outb(self.ioAddress + 7, command)
end

--- Reads the Drive Address Register and returns a table with the result.
-- @treturn table Drive Address Register
function Ata:readDriveRegister()
	local data = sys.inb(self.controlAddress + 1)
	local tbl = {
		value = data,
		ds0 = (data & 0x01) > 0,
		ds1 = (data & 0x02) > 0,
		wtg = (data & 0x40) > 0
	}
	return tbl
end

--- The drive status
-- @table Status
-- @tfield int value The raw status value.
-- @tfield bool err The error bit.
-- @tfield bool drq The data ready bit.
-- @tfield bool df The drive fault bit.
-- @tfield bool rdy The ready bit.
-- @tfield bool bsy The busy bit.

--- Reads the status register from the drive.
-- @treturn Status The current status of the drive.
function Ata:readStatus()
	for _ = 1, 4 do
		sys.inb(self.ioAddress + 7)
	end
	local result = sys.inb(self.ioAddress + 7)
	return {
		value = result,
		err = (result & 1) > 0,
		drq = (result & 8) > 0,
		df = (result & 32) > 0,
		rdy = (result & 64) > 0,
		bsy = (result & 128) > 0
	}
end

--- Reads a byte of data from the drive.
-- Waits for the data request signal to go active.
-- @treturn int A byte of data returned.
function Ata:readData()
	while not self:readStatus().drq do end
	return sys.inw(self.ioAddress)
end

--- Reads data into a buffer.
-- @tparam int amount The number of bytes to read into the buffer.
-- This number should always be an even number.
-- @tparam table buffer The buffer that will be appended to.
-- @tparam[opt] int skip Skip the first n-bytes.
-- @tparam[opt] int limit Limit the number of bytes that are copied to the buffer.
function Ata:readDataBuffer(amount, buffer, skip, limit)
	assert(type(amount) == "number", "Amount is not a number")
	assert(type(buffer) == "table", "No buffer given")
	skip = skip or 0
	limit = (limit or amount) + skip
	for i = 1, amount, 2 do
		local data = sys.inw(self.ioAddress)
		if i > skip and i <= limit then
			buffer[#buffer+1] = (data & 0xFF)
		end
		if (i + 1) > skip and (i + 1) <= limit then
			buffer[#buffer+1] = (data >> 8) & 0xFF
		end
	end
end

--- Writes data from a buffer to the drive.
-- @tparam table buffer The buffer to read from.
-- @tparam int start The index into the buffer containing the first byte that should be written.
-- @tparam int stop The index into the buffer containing the last byte that should be written.
function Ata:writeDataBuffer(buffer, start, stop)
	for i = start, stop, 2 do
		local data = buffer[i] | (buffer[i+1] << 8)
		sys.outw(self.ioAddress, data)
	end
end

function Ata:smallWait()
	for _ = 1, 5 do
		sys.inb(self.ioAddress + 7)
	end
end

function Ata.toString(buffer, start, stop)
	local str = {}
	for i = start, stop, 2 do
		str[#str+1] = string.char(buffer[i+1])
		str[#str+1] = string.char(buffer[i])
	end
	return table.concat(str)
end

--- Returns a table containing information about the drive,
-- or nil if no drive is present.
-- @tparam int drive The drive to identify (1 = primary, 2 = secondary)
-- @treturn table A table containing information about the drive.
-- @treturn nil If no drive was detected
function Ata:identify(drive)
	-- Select the correct drive using CHS mode.
	if drive == 1 then
		sys.outb(self.ioAddress + 6, 0xA0)
	elseif drive == 2 then
		sys.outb(self.ioAddress + 6, 0xB0)
	else
		error("No drive selected")
	end
	self.selectedDrive = drive
	self:smallWait()

	-- Disable interrupts.
	sys.outb(self.controlAddress, 0x02)

	-- Set all sector counts to 0.
	-- We can't use setSectors as it will change the drive select signal.
	sys.outb(self.ioAddress + 2, 0)
	sys.outb(self.ioAddress + 3, 0)
	sys.outb(self.ioAddress + 4, 0)
	sys.outb(self.ioAddress + 5, 0)

	-- Make it stop sending interrupts.
	sys.outb(self.controlAddress, 0x02)

	-- Send the Identify command.
	self:sendCommand(0xEC)

	-- Check if the drive exists.
	-- If it doesn't, return nil.
	local status = self:readStatus()
	if status.value == 0 or status.value == 0xFF then
		return
	end

	-- Wait for the busy signal to go low, and for either the data request
	-- signal or the drive error signal to go high.
	while self:readStatus().bsy do end
	status = self:readStatus()
	while status.drq == false and status.err == false do
		status = self:readStatus()
	end
	if status.err then
		--error("Drive error: " .. util.hex(status.value))
		return
	end

	-- Read the Identify result buffer.
	local data = {}
	self:readDataBuffer(512, data)
	local obj = {
		serial = util.trim(Ata.toString(data, 21, 39)),
		firmware = util.trim(Ata.toString(data, 47, 53)),
		name = (Ata.toString(data, 55, 92)),
		numSectors = util.toInt(data, 121),
		wordsPerSector = util.toInt(data, 235)
	}
	if obj.wordsPerSector == 0 then
		obj.wordsPerSector = 256
	end
	obj.bytesPerSector = obj.wordsPerSector * 2
	obj.size = obj.numSectors * obj.bytesPerSector
	return obj
end

local Atadev = {}
local _Atadev = {}
--- Creates a new ATA file device.
-- @param cntrl The controller the drive is connected to.
-- @param drive The drive number.
-- @param id The identity table of the drive.
function Atadev.new(cntrl, drive, id)
	local atadev = {
		controller = cntrl,
		drive = drive,
		id = id
	}
	for name, value in pairs(Atadev) do
		atadev[name] = value
	end
	setmetatable(atadev, _Atadev)
	return atadev
end

function _Atadev:__tostring()
	return "ATA PIO " .. util.hex(self.controller.ioAddress) .. ":" .. self.drive
end

--- Adds the drive information to a stat table.
function Atadev:stat(stat)
	stat.size = self.id.size
	stat.bytesPerSector = self.id.bytesPerSector
	stat.diskName = self.id.name
	stat.firmware = self.id.firmware
	stat.serial = self.id.serial
	stat.numberOfSectors = self.id.numSectors
end

--- Opens the ATA Drive.
function Atadev:open(read, write, append)
	local fd = {
		read = read,
		write = write,
		dev = self,
		loc = 0
	}

	if append then
		fd.loc = self.id.size
	end

	return fd
end

--- Closes a file descriptor.
function Atadev:close(fd)
end

--- Seeks on the drive.
function Atadev:seek(fd, offset, amount)
	if offset == "set" then
		fd.loc = amount
	elseif offset == "cur" then
		fd.loc = fd.loc + amount
	else
		fd.loc = fd.dev.id.size + amount
	end

	-- Make sure that the fd.loc is within a range of [0 - disk_size]
	fd.loc = math.max(math.min(fd.loc, fd.dev.id.size), 0)
	return fd.loc
end

--- Reads data from the drive into a buffer.
function Atadev:read(fd, amount, buffer)
	-- How much data can we still read?
	amount = math.min(amount, fd.dev.id.size - fd.loc)

	-- Set the start sector and the number of sectors to read.
	local startSector = fd.loc // fd.dev.id.bytesPerSector
	local stopSector = (fd.loc + amount - 1) // fd.dev.id.bytesPerSector
	local sectorCount = stopSector - startSector + 1
	while fd.dev.controller:readStatus().bsy do end
	fd.dev.controller:selectDrive(fd.dev.drive)
	fd.dev.controller:setSectors(sectorCount, startSector)

	-- Send the read command
	while fd.dev.controller:readStatus().bsy do end
	fd.dev.controller:sendCommand(0x20)
	while fd.dev.controller:readStatus().bsy do end

	-- Transfer data
	local locEnd = fd.loc + amount
	while fd.loc < locEnd do
		local offsetInSector = fd.loc % fd.dev.id.bytesPerSector
		while not fd.dev.controller:readStatus().drq do end
		fd.dev.controller:readDataBuffer(512, buffer, offsetInSector, locEnd - fd.loc)
		fd.loc = fd.loc + fd.dev.id.bytesPerSector - offsetInSector
	end

	fd.loc = fd.loc + amount
end

--- Writes data from a buffer onto the drive.
function Atadev:write(fd, buffer, start, stop)
	-- How much data can we still write?
	amount = math.min(stop - start + 1, fd.dev.id.size - fd.loc)

	-- Set the start sector and the number of sectors to read.
	local startSector = fd.loc // fd.dev.id.bytesPerSector
	local stopSector = (fd.loc + amount - 1) // fd.dev.id.bytesPerSector
	local sectorCount = stopSector - startSector + 1
	while fd.dev.controller:readStatus().bsy do end
	fd.dev.controller:selectDrive(fd.dev.drive)
	fd.dev.controller:setSectors(sectorCount, startSector)

	-- First read in the old buffer.
	local diskBuffer = {}
	fd.dev.controller:sendCommand(0x20)
	while fd.dev.controller:readStatus().bsy do end
	for _ = startSector, stopSector do
		while not fd.dev.controller:readStatus().drq do end
		fd.dev.controller:readDataBuffer(512, diskBuffer)
	end

	-- Replace the data in the old buffer.
	local locEnd = fd.loc + amount
	local offsetInSector = fd.loc % fd.dev.id.bytesPerSector
	for i = 1, locEnd - fd.loc do
		diskBuffer[i + offsetInSector] = buffer[i + start - 1]
	end

	-- Reset count
	fd.dev.controller:setSectors(sectorCount, startSector)

	-- Write the modified buffer.
	while fd.dev.controller:readStatus().bsy do end
	fd.dev.controller:sendCommand(0x30)
	while fd.dev.controller:readStatus().bsy do end

	-- Transfer data
	for i = 1, sectorCount do
		while not fd.dev.controller:readStatus().drq do end
		fd.dev.controller:writeDataBuffer(diskBuffer, (i - 1) * 512 + 1, i * 512)
	end

	-- Flush cache
	while fd.dev.controller:readStatus().bsy do end
	fd.dev.controller:sendCommand(0xE7)

	-- Update file pointer
	fd.loc = fd.loc + amount
end

local ata = new.module("ata", {
	driveCount = 0
})

--- Creates a file device for a given drive, if the drive exists.
-- If no drive exists, it will do nothing.
-- @param cntrl The controller that the drive is connected to.
-- @param drive The drive number.
function ata.loadDrive(cntrl, drive)
	local id = cntrl:identify(drive)
	if id then
		local dev = Atadev.new(cntrl, drive, id)
		fs.mkdev("/dev/hd" .. string.char(string.byte('a') + ata.driveCount), dev)
		ata.driveCount = ata.driveCount + 1
	end
end

--- Loads all devices connected to a controller.
-- @param io The IO base port of the controller.
-- @param control The Control base port of the controller.
function ata.loadController(io, control)
	local cntrl = Ata.new(io, control)
	ata.loadDrive(cntrl, 1)
	ata.loadDrive(cntrl, 2)
end

--- Initialises the ATA Controllers
function ata.init()
	ata.loadController(0x1F0, 0x3F6)
	ata.loadController(0x170, 0x376)
end
