local ps = module("ps")
local kthreads = module("kthreads")
local scheduler = new.module("scheduler", {
	processIndex = 1,
	lastProcess = nil
})

function scheduler.tick()
	local processes = ps.list()
	local processIndex = scheduler.processIndex

	if not processes[processIndex] then
		processIndex = 1
	end

	scheduler.lastProcess = processes[processIndex]
	scheduler.lastProcess:run()
	scheduler.processIndex = processIndex + 1
end

function scheduler.run()
	while true do
		if #ps.list() > 0 then
			scheduler.tick()
			kthreads.yield()
		else
			sys.panic("No more processes")
		end
	end
end
