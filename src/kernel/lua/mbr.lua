local mbr = new.module("mbr")
local fs = module("fs")

--- Parses a single MBR entry.
-- @param tbl A buffer containing the master boot record.
-- @param index An index into the buffer that contains a partition entry.
function mbr.parseEntry(tbl, index)
	local partition = {}
	partition.active = (tbl[index+1] == 0x80)
	partition.start = util.toInt(tbl, index+9)
	partition.size = util.toInt(tbl, index+13)
	partition.afterLast = partition.start + partition.size
	print("Partition start: " .. partition.start .. " (" .. util.hex(partition.start*512) .. ")")
	return partition
end

--[[
function mbr.getPartition(index)
	local info = mbr[index]
	local part = {}
	return part
end
--]]

local Part = {}
local _Part = {}

--- Creates a new partition device.
-- @param file The file that contains the partition.
-- @param path The path to where a new device should be made.
-- @param part A table containing information about the partition.
function Part.new(file, path, part)
	local dev = {}
	dev.file = file
	dev.mbr = part
	dev.partitionStart = dev.mbr.start * 512
	dev.partitionEnd = dev.mbr.start * 512 + dev.mbr.size * 512
	dev.partitionSize = dev.partitionEnd - dev.partitionStart
	for name, value in pairs(Part) do
		dev[name] = value
	end
	setmetatable(dev, _Part)
	fs.mkdev(path, dev)
end

--- Display device name nicely
function _Part:__tostring()
	return "MBR on " .. self.file
end

--- Opens the partition.
-- @param mode The mode to open the underlying file with.
function Part:open(mode, read, write, append)
	local fd = {
		file = fs.open(self.file, mode),
	}
	fd.file:seek("set", self.partitionStart + 512)
	return fd
end

function Part:close(fd)
	fd.file:close()
end

function Part:read(fd, amount, data)
	local max = self.partitionSize - fd.file:seek()
	local amount = math.min(max, amount)
	fd.file:read(amount, data)
end

function Part:write(fd, data, start, stop)
	local max = self.partitionSize - fd.file:seek()
	local amount = math.min(max, stop - start + 1)
	fd.file:write(data, start, start + amount - 1)
end

function Part:seek(fd, offset, amount)
	if offset == "set" then
		seek = self.partitionStart + amount
	elseif offset == "cur" then
		seek = fd.file:seek() + amount
	elseif offset == "end" then
		seek = self.partitionEnd + amount
	end

	seek = math.max(math.min(seek, self.partitionEnd), self.partitionStart)
	fd.file:seek("set", seek)
	return seek - self.partitionStart
end

function Part:stat(stat)
	stat.size = self.partitionSize
	stat.partitionStart = self.partitionStart
	stat.partitionEnd = self.partitionEnd
	stat.parent = self.file
end


function mbr.probe(file)
	local fh = assert(fs.open(file, "r"))
	local buffer = {}
	assert(fh:read(512, buffer))
	sys.log(tostring(buffer) .. " has size " .. #buffer)
	local partitions = 1
	for i = 1, 4 do
		local part = mbr.parseEntry(buffer, 446 + (i-1) * 16)
		if part.size > 0 then
			Part.new(file, file .. tostring(partitions), part)
			partitions = partitions + 1
		end
	end
	fh:close()
end
