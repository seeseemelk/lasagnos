#ifndef LIBTHREAD_H
#define LIBTHREAD_H

#include "threads.h"
#include <lua.h>

void luaopen_threads(lua_State* L);

#endif
