#include "libmultiboot.h"
#include "multiboot.h"
#include "kernel.h"
#include <lua.h>
#include <lauxlib.h>

#define luaL_setconstant(L, n, c) \
	lua_pushinteger(L, c); \
	lua_setfield(L, -2, n)

multiboot_info_t* mbd;

static int l_modules(lua_State* L)
{
	lua_pushinteger(L, mbd->mods_addr);
	lua_pushinteger(L, mbd->mods_count);
	return 2;
}

static int l_module(lua_State* L)
{
	size_t mod_addr = luaL_checkinteger(L, 1);
	multiboot_module_t* mod = (multiboot_module_t*) mod_addr;

	lua_createtable(L, 0, 2);
	luaL_setconstant(L, "modEnd", mod->mod_end);
	luaL_setconstant(L, "modStart", mod->mod_start);

	return 1;
}

static int l_framebuffer(lua_State* L)
{
	lua_pushinteger(L, mbd->framebuffer_addr);
	lua_pushinteger(L, mbd->framebuffer_pitch);
	lua_pushinteger(L, mbd->framebuffer_width);
	lua_pushinteger(L, mbd->framebuffer_height);
	lua_pushinteger(L, mbd->framebuffer_bpp);
	return 5;
}

static const struct luaL_Reg libmb[] =
{
	{"modules", l_modules},
	{"module", l_module},
	{"framebuffer", l_framebuffer},
	{NULL, NULL}
};

void luaopen_multiboot(lua_State* L, multiboot_info_t* pmbd)
{
	mbd = pmbd;
	luaL_newlib(L, libmb);
	lua_setglobal(L, "multiboot");
}
