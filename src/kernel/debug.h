/*
 * debug.h
 *
 *  Created on: Sep 29, 2018
 *      Author: seeseemelk
 */

#ifndef LUA_DEBUG_H_
#define LUA_DEBUG_H_

void debug_byte(unsigned char b);
void debug_pause();
void debug_string(const char* s);

#endif /* LUA_DEBUG_H_ */
