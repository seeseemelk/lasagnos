/*
 * cpu.c
 *
 *  Created on: Jan 3, 2018
 *      Author: seeseemelk
 */
#include "cpu.h"

#include "kernel.h"
#include "memory.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include "../libc/cdefs.h"
#include "asm.h"

/*
 * GDT structs
 */

typedef struct gdt_entry gdt_entry;
typedef struct gdt_descriptor gdt_descriptor;
typedef struct tss_descriptor tss_descriptor;

struct __attribute__((packed)) gdt_entry
{
	u16 limit0_15;
	u16 base0_15;
	u8 base16_23;
	u8 access;
	u8 limit16_19; /* Also contains flags */
	u8 base24_31;
};

struct __attribute__((packed)) gdt_descriptor
{
	u16 limit;
	u32 address;
};

struct __attribute__((packed)) tss_descriptor
{
	u16 link;
	u16 _1;

	u32 esp0;
	u16 ss0;
	u16 _2;

	u32 esp1;
	u16 ss1;
	u16 _3;

	u32 esp2;
	u16 ss2;
	u16 _4;

	u32 cr3;
	u32 eip;
	u32 eflags;
	u32 eax;
	u32 ecx;
	u32 edx;
	u32 ebx;
	u32 esp;
	u32 ebp;
	u32 esi;
	u32 edi;

	u16 es;
	u16 _5;
	u16 cs;
	u16 _6;
	u16 ss;
	u16 _7;
	u16 ds;
	u16 _8;
	u16 fs;
	u16 _9;
	u16 gs;
	u16 _10;
	u16 ldtr;
	u16 _11;
	u16 _12;
	u16 iopb;
};

gdt_entry gdt_table[6];
gdt_descriptor descriptor;
tss_descriptor tss = {0};

/*
 * GDT functions
 */
static void gdt_set_limit(gdt_entry* entry, u32 limit)
{
	entry->limit0_15 = limit & 0xFFFF;
	entry->limit16_19 = (entry->limit16_19 & 0xF0) | ((limit >> 16) & 0xF);
}

static void gdt_set_base(gdt_entry* entry, u32 base)
{
	entry->base0_15 = base & 0xFFFF;
	entry->base16_23 = (base >> 16) & 0xFF;
	entry->base24_31 = (base >> 24) & 0xFF;
}

static void gdt_set_access(gdt_entry* entry, bool present, int privilege, bool executable, bool dc, bool rw)
{
	// 0x10 -> set reserved bit to 1 and set accessed bit to 0
	entry->access = 0x10 | (present << 7) | (privilege << 5) | (executable << 3) | (dc << 2) | (rw << 1);
}

static void gdt_set_flags(gdt_entry* entry, bool granularity)
{
	// 0x40 -> set size bit to 1 (32-bit protected mode)
	entry->limit16_19 = (entry->limit16_19 & 0x0F) | ((granularity << 7) | 0x40);
}

static void gdt_zero_entry(gdt_entry* entry)
{
	entry->limit0_15 = 0;
	entry->base0_15 = 0;
	entry->base16_23 = 0;
	entry->access = 0;
	entry->limit16_19 = 0;
	entry->base24_31 = 0;
}

/*
void gdt_load(gdt_descriptor* descriptor)
{
	asm __volatile__ (
			"lgdt %0;"
			:
			: "m" (*descriptor));
	asm_reload_segment_registers();
}
*/

void cpu_pause()
{
	asm ("hlt");
}

/*
 * General cpu functions
 */
void cpu_init()
{
	cli();
	gdt_zero_entry(&gdt_table[0]);

	/* Kernel Code entry */
	gdt_set_base(gdt_table+1, 0);
	gdt_set_limit(gdt_table+1, 0xFFFFF);
	gdt_set_flags(gdt_table+1, true);
	gdt_set_access(gdt_table+1, true, 0, true, false, true);

	/* Kernel Data entry */
	gdt_set_base(gdt_table+2, 0);
	gdt_set_limit(gdt_table+2, 0xFFFFF);
	gdt_set_flags(gdt_table+2, true);
	gdt_set_access(gdt_table+2, true, 0, false, false, true);

	/* User Code Entry */
	gdt_set_base(gdt_table+3, 0);
	gdt_set_limit(gdt_table+3, 0xFFFFF);
	gdt_set_flags(gdt_table+3, true);
	gdt_set_access(gdt_table+3, true, 3, true, false, true);

	/* User Data entry */
	gdt_set_base(gdt_table+4, 0);
	gdt_set_limit(gdt_table+4, 0xFFFFF);
	gdt_set_flags(gdt_table+4, true);
	gdt_set_access(gdt_table+4, true, 3, false, false, true);

	/* Setup TSS */
	gdt_set_base(gdt_table+5, (int) &tss);
	gdt_set_limit(gdt_table+5, sizeof(tss_descriptor));
	gdt_set_flags(gdt_table+5, false);
	gdt_table[5].access = 0x89;
	tss.ss0 = 16;

	/* Load GDT */
	descriptor.address = (int) gdt_table;
	descriptor.limit = sizeof(gdt_table) - 1;
	asm_load_gdt(&descriptor);
}

void cpu_init_tss()
{
	/* Load TSS */
	tss.esp0 = (size_t) NULL;
	asm_load_tss((sizeof(gdt_entry) * 5) | 3);
}

void cpu_set_returnpoint(size_t address)
{
	tss.esp0 = address;
}














