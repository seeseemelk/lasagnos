[bits 32]

; Some of this code was taken from the OSDev Wiki
; https://wiki.osdev.org/Fpu

global fpu_available
fpu_available:
	mov edx, cr0
	and edx, (-1) - 0b1100
	fninit
	fnstsw [testword]
	cmp word [testword], 0
	jne .nofpu
	mov eax, 1
	ret
.nofpu:
	mov eax, 0
	ret

testword dw 0x55AA

global fpu_init
fpu_init:
	fldcw [value_37F]   ; writes 0x37f into the control word: the value written by F(N)INIT
	fldcw [value_37E]   ; writes 0x37e, the default with invalid operand exceptions enabled
	fldcw [value_37A]   ; writes 0x37a, both division by zero and invalid operands cause exceptions.

	; Set a bit in CR4. This enables SSE.
	;mov eax, cr4
	;or eax, 0x200
	;mov cr4, eax

	ret

value_37F dw 0x37F
value_37E dw 0x37E
value_37A dw 0x37A
