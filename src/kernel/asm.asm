[bits 32]

global asm_load_cr3_page_dir
asm_load_cr3_page_dir:
	mov eax, [esp+4]
	mov cr3, eax
	ret

global cli
cli:
	cli
	ret

global sti
sti:
	sti
	ret

global asm_load_gdt
asm_load_gdt:
	mov eax, [esp+4]
	lgdt [eax]
	; Set segment registers
	mov ax, 16
	mov ds, ax
	mov es, ax
	mov gs, ax
	mov fs, ax
	mov ss, ax
	;push word 16
	;pop ss
	;push word 16
	;pop fs
	;push word 16
	;pop gs
	;push word 16
	;pop es
	;push word 16
	;pop ds
	; Set CS
	jmp 0x08:.jump
.jump:
	ret

global asm_load_tss
asm_load_tss:
	mov ax, [esp+4]
	ltr ax
	ret

global asm_get_cr2
asm_get_cr2:
	mov eax, cr2
	ret

; These two functions may require some explanation;
; thread_save does not store the call stack, but rather the call stack depth.
; This means that if a function calls thread_save and then thread_enter, the
; .second_exit segment will return as if it part of thread_enter.
; This also means that you shouldn't have any other function calls between
; thread_save and thread_enter.
; thread_enter will return 0 normally, but if the exit routine of thread_save is executed
; it will return 1.
global thread_save
thread_save:
	enter 0, 0

	;Get a pointer to the data structure
	mov edx, [ebp+8]

	;Store DS
	mov ax, ds
	mov [edx], ax

	;Store SS
	mov ax, ss
	mov [edx+2], ax

	;Store CS
	mov ax, cs
	mov [edx+4], ax

	;Store EIP
	mov dword [edx+6], .second_exit

	;Store EFLAGS
	pushfd
	pop eax
	mov [edx+10], eax

	;Store ESP
	mov [edx+14], esp

	;Store EBP
	mov [edx+18], ebp

	;Store ESI
	mov [edx+22], esi

	;Store EDI
	mov [edx+26], edi

	;Return with no value (Returns as thread_save)
	.exit:
	leave
	ret

	;Return with a value of 1 (Returns as thread_enter)
	.second_exit:
	leave
	mov eax, 1
	ret

;global thread_enter
;thread_enter:
;	enter 0, 0
;	;Get a pointer to the data structure.
;	mov edx, [ebp+8]
;
;	;Restore other registers
;	mov esp, [edx+14]
;	mov ebp, [edx+18]
;	mov esi, [edx+22]
;	mov edi, [edx+26]
;
;	;Keep other registers
;	mov bx, [edx+2]
;	mov cx, [edx+0]
;
;	mov ax, cx
;	mov ds, ax
;
;	;Set CS
;	;xor eax, eax
;	;mov ax, [edx+4]
;	;push eax
;	push .continue
;	retf
;.continue:
;
;	;Restore DS and SS
;	;mov ax, bx
;	;mov ss, ax
;
;	;Perform return
;	;xor eax, eax
;	;mov ax, [edx+4]
;	;push eax
;	;push dword [edx+6]
;	;sti
;	;retf
;
;	; Value of DS
;	mov ax, 0x23 ;[edx+0]
;	mov ds, ax
;	mov es, ax
;	mov fs, ax
;	mov gs, ax
;
;	;mov eax, esp
;	;push word 0
;	;push word [edx+2] ;Save value of SS
;	;push dword [edx+14] ;Save the new stack
;	;push dword [edx+10] ;Save flags
;	;push word 0
;	;push word [edx+4] ;Save value of CS
;	;push dword [edx+6] ;Save EIP
;
;	mov eax, esp
;	push 0x23
;	push 0x10000100
;	pushf
;	push 0x1B
;	push 0x10000000
;
;	;pop eax
;	;pop ebx
;
;	; The following lines are for debug information
;	;pop eax
;	;pop ebx
;	;pop ecx
;	;pop edx
;	;pop esp
;	;jmp $
;
;	iretd
;
;	;leave
;	;mov eax, 0
;	;ret

global thread_enter
thread_enter:
	enter 0, 0
	mov edx, [ebp+8]

	;Restore DS and SS
	;mov ax, [edx+0]
	;mov ds, ax
	;mov ax, [edx+2]
	;mov ss, ax

	;Restore other registers
	mov esp, [edx+14]
	mov ebp, [edx+18]
	mov esi, [edx+22]
	mov edi, [edx+26]

	;Perform return
	push dword [edx+6]
	sti
	ret

	leave
	mov eax, 0
	ret

global asm_drop_ring3
asm_drop_ring3:
	mov edx, [esp+4]

	; Set segment registers
	mov ax, [edx+0]
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	; Prepare stack
	;mov eax, esp
	push dword [edx+2]
	push dword 0
	pushf
	push dword [edx+4]
	push dword 0x1000

	; Drop to ring 3
	iretd










