/*
 * libpaging.h
 *
 *  Created on: Aug 17, 2018
 *      Author: seeseemelk
 */

#ifndef SRC_KERNEL_LIBPAGING_H_
#define SRC_KERNEL_LIBPAGING_H_
#include <lua.h>

void luaopen_paging(lua_State* L);

#endif /* SRC_KERNEL_LIBPAGING_H_ */
