#include "kernel.h"
#include "kernel.h"
#include "interrupt.h"
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <threads.h>
#include "../libc/cdefs.h"

thread_t threads[MAX_THREADS] = {0};
size_t next_thread_id = 1;
thread_t* current_thread; /* This is set to threads[0] in thread_init() */

/**
 * Saves the current thread's registers to a given thread_data struct.
 */
void thread_start()
{
	sti();
	current_thread->entry_point();
	kernel_panic("Reached end of thread");
}

void thread_switch(thread_t* thread)
{
	thread_save(&current_thread->data);
	cli();
	current_thread = thread;
	thread_enter(&thread->data);
	if ((current_thread->data.eflags & (1 << 9)) > 0)
		sti();
}

/**
 * Real entry point for any thread.
 */
thread_t* thread_create(void(*entry_point)(void))
{
	char* stack = malloc(KB(16));
	return thread_create_special(entry_point, stack, KB(16), 0x8, 0x10, 0x10);
}

thread_t* thread_create_special(void(*entry_point)(void), char* stack, size_t stack_size, short cs, short ds, short ss)
{
	thread_t* thread = NULL;
	// Find a free thread
	for (int i = 0; i < 64; i++)
	{
		if (threads[i].id == 0)
		{
			thread = threads + i;
			break;
		}
	}
	if (thread == NULL)
		kernel_panic("Too many threads");

	thread->entry_point = entry_point;
	thread->id = next_thread_id++;
	thread_data* data = &thread->data;
	thread_save(data);
	if (cs != 0)
	{
		data->cs = cs;
		data->ds = ds;
		data->ss = ss;
	}
	data->stack = stack;
	data->ebp = (u32) data->stack + stack_size - 4;
	data->esp = (u32) data->stack + stack_size - 4;
	data->eip = (u32) entry_point; //(u32) thread_start;
	((char*)data->esp)[4] = (u32) thread_start;

	return thread;
}

/**
 * This function will execute code at address 0x00000000
 * with usermode permissions.
 */
void thread_drop_ring3()
{
	asm_drop_ring3(&current_thread->data);
}

thread_t* thread_create_user()
{
	char* stack = malloc(512);
	thread_t* thread = thread_create_special(thread_drop_ring3, stack, 512, 0x1B, 0x23, 0x23);
	return thread;
}

void thread_free(thread_t* thread)
{
	kernel_log("Freeing %d, current is %d", thread->id, current_thread->id);
	if (current_thread->id == thread->id)
		kernel_panic("Trying to free currently running thread");
	free(thread->data.stack);
	thread->id = 0;
}

void thread_leave()
{
	thread_switch(threads);
	sti();
}

void thread_init()
{
	/* Stores this call chain as a thread. */
	current_thread = threads;
	current_thread->id = next_thread_id++;
	thread_save(&current_thread->data);
}

thread_t* thread_get(size_t id)
{
	if (id-1 > MAX_THREADS)
		kernel_panic("Thread id %d too high", id);
	return threads+id-1;
}

void thread_set_paused(thread_t* thread, bool paused)
{
	thread->paused = paused;
}

bool thread_is_paused(thread_t* thread)
{
	return thread->paused;
}

void thread_iterator_create(thread_it* it)
{
	it->thread = threads;
	it->index = 0;
}

bool thread_iterator_has_next(thread_it* it)
{
	return it->thread != NULL;
}

thread_t* thread_iterator_next(thread_it* it)
{
	thread_t* thread_to_return = it->thread;
	it->thread = NULL;
	for (int i = it->index + 1; i < MAX_THREADS; i++)
	{
		if (threads[i].id != 0)
		{
			it->thread = threads + i;
			it->index = i;
			break;
		}
	}
	return thread_to_return;
}















