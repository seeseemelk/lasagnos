local args = {...}

local options = {
	hexdump = false,
	help = false
}

local function cat(path)
	local stat = fs.stat(path)
	if not stat then
		print("File " .. path .. " not found")
	elseif stat.type ~= "file" then
		print(path .. " is not a file")
	else
		local fh = fs.open(path, "r")
		if not options.hexdump then
			local content = fh:read(stat.size)
			fh:close()
			io.write(content)
		else
			for i = 1, stat.size, 512 do
				local buffer = {}
				fh:read(512, buffer)
				fh:seek("cur", -1)
				util.hexdump(buffer, {noShowLength = true})
			end
			print()
			fh:close()
		end
	end
end

local files = {}
for _, option in ipairs(args) do
	if option == "-h" then
		options.help = true
	elseif option == "-H" then
		options.hexdump = true
	else
		files[#files+1] = fs.concat(ENV.CWD, option)
	end
end

if options.help then
	print("Usage: cat [options] <files...>")
	print("Options:")
	print(" -h   Display this help message")
	print(" -H   Perform hexdump")
else
	for _, file in ipairs(files) do
		cat(file)
	end
end
