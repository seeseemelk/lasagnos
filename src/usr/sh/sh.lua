local path = os.getenv("PATH") or "/bin/"

local function runNativeCommand(path)
	nprocess.run(path)
end

local function runCommand(cmd)
	local args = util.split(cmd, " ")
	if #args > 0 and #args[1] then
		cmd = args[1]
		table.remove(args, 1)
		local path = io.concat(path, cmd)
		local stat = io.stat(path)
		if stat and stat.type == "file" then
			os.execute(path, args)
		else
			print("Command not found")
			print("")
		end
	end
end

while true do
	io.write(os.getenv("CWD") .. "# ")
	local cmd = io.read() --readCode()
	runCommand(cmd)
end
