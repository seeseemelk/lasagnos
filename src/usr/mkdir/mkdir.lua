local args = {...}

local dirname = args[1]
local path = fs.concat(ENV.CWD, dirname)

fs.mkdir(path)
