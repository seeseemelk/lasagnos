local progName = dir.current:match("([%w_]+)$")
local progDir = fs.concat(dir.usr, progName)

tasks.add("build_each_program", function()
	-- All build objects
	local objects = {var.crt}
	local output = fs.concat(dir.build, progDir, progName..".elf")

	-- Build assembler sources
	local sources = action.nasm.findSources(progDir)
	action.nasm.build(action.nasm.findSources(progDir), objects)

	-- Build C sources
	sources = action.c.findSources(progDir)
	action.c.build(sources, objects)

	-- Link it all together
	local ldflags = var.c.ldexeflags
	tasks.withVar({
		c = {
			ldexeflags = ldflags .. " -Lbuild -l:ulibc.a -lgcc -s"
		}
	}, function()
		local exe = action.c.linkExe(output, objects)
		var.programs[#var.programs + 1] = {exe, progName}
	end)
end)
