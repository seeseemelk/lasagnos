local args = {...}

local options = {
	help = false,
	insertHelloWorld = false
}

local function touch(filename)
	fs.mkfile(filename)
	if options.insertHelloWorld then
		local fd = fs.open(filename, "w")
		fd:write("Hello, world!")
		fd:close()
	end
end

-- Parse options
local files = {}
for _, v in ipairs(args) do
	if v == "-h" then
		options.help = true
	elseif v == "-H" then
		options.insertHelloWorld = true
	else
		files[#files + 1] = fs.concat(ENV.CWD, v)
	end
end

-- Execute options
if options.help then
	print("Usage: touch [options] files...")
	print("Options:")
	print(" -h   Show this help message")
	print(" -H   Put a hello world message into the file")
else
	for _, file in ipairs(files) do
		touch(file)
	end
end
