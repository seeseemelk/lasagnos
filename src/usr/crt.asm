[bits 32]

section .text.prologue
	extern main

	global _start
	_start:
	jmp .header
	db "LOS"

	.header:
		; Setup stack
		mov eax, stack_end
		mov esp, eax
		mov ebp, eax

		; Call main
		call main

		; Exit program
		xor eax, eax
		int 0x82
	
section .bss
	stack resb 512
	stack_end:
