local args = {...}

if #args >= 2 then
	local fsType = args[1]
	local mp = fs.concat(ENV.CWD, args[2])
	if #args == 3 then
		local dev = fs.concat(ENV.CWD, args[3])
		print("Mounting " .. dev .. " at " .. mp .. " as  " .. fsType)
	else
		print("Mounting " .. fsType .. " at " .. mp)
	end

	fs.mount(mp, fsType, dev)
else
	print("Incorrect parameters")
	print("mount [fstype] [mountpoint] [device]")
end
