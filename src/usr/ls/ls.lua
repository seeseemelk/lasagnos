local args = {...}
local path = os.getenv("CWD")

if #args > 0 then
	path = io.concat(path, args[1])
end

local stat = io.stat(path)

if not stat then
	print("Directory " .. path .. " not found")
elseif stat.type == "dir" then
	local contents = io.list(path)
	for _, file in ipairs(contents) do
		if file.type == "file" or file.type == "dev" then
			print(file.name)
		elseif file.type == "dir" then
			print(file.name .. "/")
		else
			print(file.name .. " [" .. tostring(file.type) .. "]")
		end
	end
else
	print("Not a directory")
end
