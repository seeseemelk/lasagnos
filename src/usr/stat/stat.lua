local args = {...}

local function doStat(path)
	local stat = io.stat(path)
	if stat then
		print("Name: " .. stat.name .. ", size: " .. stat.size)
		print("Type: " .. stat.type)
		local first = true
		for name, value in pairs(stat) do
			if name ~= "name" and name ~= "size" and name ~= "type" then
				if first then
					print("")
					print("Other properties:")
					first = false
				end
				print(name .. ": " .. tostring(value))
			end
		end
	else
		print("File " .. path .. " not found")
	end
end

for _, filename in ipairs(args) do
	doStat(io.concat(os.getenv("CWD"), filename))
end
