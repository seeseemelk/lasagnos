-- Builds Lua programs
tasks.add("build_programs", function()
	local files = fs.find(dir.usr, var.lua.pattern, true)

	for _, file in ipairs(files) do
		if not file:match("/make.lua$") then
			var.programs[#var.programs + 1] = {
				file,
				file:match("([%w_]+)%.lua$")
			}
		end
	end
end)

-- Builds crt.o
tasks.add("build_crt", function()
	local output = var.crt
	local source = fs.concat(dir.usr, "crt.asm")
	local object = action.nasm.build(source)
	action.cp(object, output)
end)

-- Builds C and assembler programs
tasks.add("build_programs", function()
	tasks.run("build_ulibc")
	local overlay = {
		c = {
			ldscript = fs.concat(dir.usr, "linker.ld"),
			includes = {dir.libc}
		}
	}

	tasks.withVar(overlay, function(realVar)
		tasks.run("build_crt")
		tasks.run("build_each_program")

		local targetDirectory = fs.concat(dir.klua.headers, "prgfs")
		for _, program in ipairs(var.programs) do
			var.initrd.files[#var.initrd.files + 1] = program[1]
			var.initrd.names[#var.initrd.names + 1] = program[2]
		end
	end)
end)

tasks.add("build_initrd", function()
	tasks.run("build_programs")

	local files = {}
	action.mkdir(dir.initrd)
	for i, file in ipairs(var.initrd.files) do
		local name = var.initrd.names[i]
		local output = fs.concat(dir.initrd, name)
		action.cp(file, output)
		files[#files + 1] = output
	end

	if fs.needsBuild(var.initrd.tar, files) then
		action.execute("tar -c --transform=s:"..dir.initrd.."/::" ,"-f", var.initrd.tar, files)
	end
end)

tasks.add("build", function()
	tasks.run("build_initrd")
end)
