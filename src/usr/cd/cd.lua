local args = {...}
local arg = args[1]

local newPath = io.purecanon(io.concat(os.getpenv("CWD"), arg))
local stat = io.stat(newPath)
if stat.type == "dir" then
	os.setpenv("CWD", newPath)
else
	print("Not a directory")
end
