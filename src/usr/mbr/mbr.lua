local args = {...}

local path = io.concat(os.getenv("CWD"), args[1])
local stat = io.stat(path)
if stat then
	if stat.type == "file" or stat.type == "dev" then
		print("Probing file for mbr partitions and adding to /dev")
		os.kmod("mbr").probe(path)
	else
		print("Not a file")
	end
else
	print("File not found")
end
