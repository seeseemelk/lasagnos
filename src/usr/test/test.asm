;[org 1000h]
[bits 32]

section .text

global main
main:
	; Find io namespace
	mov edx, nss_io
	int 80h
	mov byte [ns_io], ah

	; Find io.write function
	mov ah, [ns_io]
	mov edx, nss_io_write
	int 81h
	mov byte [ns_io_write], al

	; Print
	call print
	ret

print:
	mov ah, [ns_io]
	mov al, [ns_io_write]
	mov edx, msg_hello
	int 82h
	ret

section .data
	nss_io db "io", 0
	nss_io_write db "write", 0
	msg_hello db "Hello, World!", 0

section .bss
	ns_io resb 1
	ns_io_write resb 1
