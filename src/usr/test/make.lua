local progName = "test"
local progDir = fs.concat(dir.usr, progName)

tasks.add("build_each_program", function()
	local sources = action.nasm.findSources(progDir)
	local output = fs.concat(dir.build, progDir, progName..".elf")
	local object = action.nasm.build(action.nasm.findSources(progDir))
	object[#object + 1] = var.crt
	local exe = action.c.linkExe(output, object)
	var.programs[#var.programs + 1] = {exe, "test"}
end)
