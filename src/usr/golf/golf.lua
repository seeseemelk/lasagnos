local screen = {}
local graphics = {}
local colors = {
	background = 0x004400,
	b = 0x000000,
	g = 0x777777,
	G = 0xEEEEEE
}
local input = {}
local exit = false

local sprites = {
	ball = {
		" gggg ",
		"ggGGgg",
		"gGGGGg",
		"gGGGGg",
		"ggGGgg",
		" gggg ",
	},
	hole = {
		"   ggg ",
		"  ggggg ",
		" ggbbbgg",
		"ggbbbbbgg",
		"ggbbbbbgg",
		"ggbbbbbgg",
		" ggbbbgg",
		"  ggggg ",
		"   ggg  ",
	}
}

local levels = {
	{
		"##############",
		"#            #",
		"#      P     #",
		"#            #",
		"#            #",
		"#            #",
		"#            #",
		"#            #",
		"#            #",
		"#            #",
		"#            #",
		"#      o     #",
		"#            #",
		"##############",
	},
	{
		"##############",
		"#            #",
		"#            #",
		"#        P   #",
		"#            #",
		"#            #",
		"#            #",
		"#      #######",
		"#            #",
		"#            #",
		"#            #",
		"#        o   #",
		"#            #",
		"##############"
	},
	{
		"##############",
		"#            #",
		"#            #",
		"#   P        #",
		"#            #",
		"#            #",
		"########   ###",
		"#            #",
		"#            #",
		"#            #",
		"####   #######",
		"#            #",
		"#            #",
		"#            #",
		"#  o         #",
		"#            #",
		"##############"
	}
}
local currentLevel = {
	player = {
		x = 0,
		y = 0,
		speed = 0,
		direction = 0,
		power = 100,
		radius = #sprites.ball / 16,
		tries = 0
	},
	hole = {
		x = 0,
		y = 0
	},
	level = 1,
	club = 0,
	data = {},
	entities = {},
}
local offsetX, offsetY = 64, 64

local function newBall(x, y)
	return {
		x = x,
		y = y
	}
end

local function newHole(x, y)
	return {
		x = x,
		y = y
	}
end

local function loadLevel(level)
	currentLevel.level = level
	level = levels[level]

	local data = {}
	local entities = {}
	currentLevel.data = data
	currentLevel.entities = entities
	currentLevel.won = false
	currentLevel.player.speed = 0
	currentLevel.player.direction = 0
	currentLevel.player.tries = 0

	for y, row in ipairs(level) do
		local rowData = {}
		data[y] = rowData
		for i = 1, #row do
			local char = row:sub(i,i)
			if char == "o" then
				local ball = newBall(i + 0.5, y + 0.5)
				currentLevel.player.x = ball.x
				currentLevel.player.y = ball.y
				rowData[i] = " "
			elseif char == "P" then
				local hole = newHole(i + 0.5, y + 0.5)
				currentLevel.hole.x = hole.x
				currentLevel.hole.y = hole.y
				rowData[i] = " "
			else
				rowData[i] = char
			end
		end
	end
end

local function getLevelData(x, y)
	local row = currentLevel.data[y]
	if row then
		return row[x] or " "
	else
		return " "
	end
end

function graphics.clear()
	screen.rectangle(colors.background, 0, 0, screen.width, screen.height)
end

function graphics.drawSprite(x, y, sprite)
	local width = #sprite
	local height = #sprite[1]

	x = math.floor(x * 8 - width / 2) + offsetX
	y = math.floor(y * 8 - height / 2) + offsetY

	for iy, row in ipairs(sprite) do
		for ix, color in ipairs(row) do
			if color ~= -1 then
				screen.rectangle(color, x + ix, y + iy, 1, 1)
			end
		end
	end
end

function graphics.drawItem(x, y, item)
	x = x * 8 + offsetX
	y = y * 8 + offsetY

	if item == "#" then
		screen.rectangle(0xBBBBBB, x, y, 8, 8)
	elseif item == "P" then
		screen.rectangle(0x444444, x, y, 8, 8)
	end
end

function graphics.drawClub()
	local direction = currentLevel.player.direction + math.pi
	local centerX, centerY = currentLevel.player.x, currentLevel.player.y
	local length = currentLevel.player.power / 25
	local endX = math.cos(direction) * length + centerX
	local endY = math.sin(direction) * length + centerY

	screen.line(0x00FF00, math.floor(centerX * 8) + offsetX, math.floor(centerY * 8) + offsetY,
		math.floor(endX * 8) + offsetX, math.floor(endY * 8) + offsetY)
end

function graphics.paint()
	graphics.clear()
	if not currentLevel.won then
		for y, row in ipairs(currentLevel.data) do
			for x, item in ipairs(row) do
				graphics.drawItem(x, y, item)
			end
		end

		for _, entity in ipairs(currentLevel.entities) do
			graphics.drawSprite(entity.x, entity.y, entity.sprite)
		end

		graphics.drawSprite(currentLevel.hole.x, currentLevel.hole.y, sprites.hole)
		graphics.drawSprite(currentLevel.player.x, currentLevel.player.y, sprites.ball)

		if currentLevel.player.speed == 0 then
			graphics.drawClub()
		end

		screen.moveCursor(1, 1)
		local direction = math.floor(math.deg(currentLevel.player.direction))
		local power = currentLevel.player.power
		screen.write(string.format("Direction: %3d   Power: %3d", direction, power))
	else
		screen.moveCursor(1, 1)
		screen.write("You won!\n")
		screen.write("It took you " .. currentLevel.player.tries .. " tries.\n")

		if currentLevel.level < #levels then
			screen.write("Press enter to go to the next level.\n")
		end
	end

	screen.flush()
end

local function parseSprite(sprite)
	local data = {}
	for y, row in ipairs(sprite) do
		local rowData = {}
		data[y] = rowData
		for x = 1, #row do
			local char = row:sub(x, x)
			rowData[x] = colors[char] or -1
		end
	end
	return data
end

local function parseAllSprites()
	for name, sprite in pairs(sprites) do
		sprites[name] = parseSprite(sprite)
	end
end

local function fireBall()
	currentLevel.player.speed = currentLevel.player.power
end


local function updateBall(dt)
	local x = currentLevel.player.x
	local y = currentLevel.player.y
	local direction = currentLevel.player.direction
	local distance = currentLevel.player.speed * dt

	-- Calculate the new player position
	local nx = math.cos(direction) * distance + x
	local ny = math.sin(direction) * distance + y

	-- Collision detection
	local right = math.floor(nx + currentLevel.player.radius + 1/8)
	local left = math.floor(nx - currentLevel.player.radius)
	local bottom = math.floor(ny + currentLevel.player.radius + 1/8)
	local top = math.floor(ny - currentLevel.player.radius)

	if getLevelData(right, math.floor(ny)) == "#" then
		sys.log("COLLISION RIGHT")
		local dx = right - nx
		nx = right - currentLevel.player.radius - 1/8
		direction = math.pi - direction
	elseif getLevelData(left, math.floor(ny)) == "#" then
		sys.log("COLLISION LEFT")
		local dx = nx - left
		nx = left + currentLevel.player.radius + 1
		direction = math.pi - direction
	end

	if getLevelData(math.floor(nx), bottom) == "#" then
		sys.log("COLLISION BOTTOM")
		local dy = bottom - ny
		ny = bottom - currentLevel.player.radius - 1/8
		direction = 2*math.pi - direction
	elseif getLevelData(math.floor(nx), top) == "#" then
		sys.log("COLLISION TOP")
		local dy = ny - top
		ny = top + currentLevel.player.radius + 1
		direction = 2*math.pi - direction
	end

	-- Store the player position
	currentLevel.player.x = nx
	currentLevel.player.y = ny
	currentLevel.player.direction = direction

	-- Reduce the player speed
	currentLevel.player.speed = currentLevel.player.speed * 0.9
	if currentLevel.player.speed <= 0.05 then
		currentLevel.player.speed = 0
	end

	-- If the ball is close to the hole, move it towards it
	local dhx = nx - currentLevel.hole.x
	local dhy = ny - currentLevel.hole.y
	local distanceToHole = math.sqrt(dhx*dhx + dhy*dhy)
	if distanceToHole < 0.1 then
		currentLevel.won = true
	elseif distanceToHole < 2*currentLevel.player.radius then
		currentLevel.player.speed = distanceToHole * 10
		currentLevel.player.direction = math.pi + math.atan(dhy, dhx)
	end
end

local function updateGame()
	local dt = 0.01
	if input.down[input.ESCAPE] then
		exit = true
	elseif currentLevel.won and input.down[input.ENTER] then
		if currentLevel.level == #levels then
			exit = true
		else
			loadLevel(currentLevel.level + 1)
		end
	else
		if currentLevel.player.speed == 0 then
			if input.down[input.RIGHT] then
				currentLevel.player.direction = (currentLevel.player.direction - 15 * dt) % (2 * math.pi)
			elseif input.down[input.LEFT] then
				currentLevel.player.direction = (currentLevel.player.direction + 15 * dt) % (2 * math.pi)
			end

			if input.down[input.UP] then
				currentLevel.player.power = math.min((currentLevel.player.power + 5), 150)
			elseif input.down[input.DOWN] then
				currentLevel.player.power = math.max((currentLevel.player.power - 5), 10)
			end

			if input.down[input.FIRE] then
				currentLevel.player.tries = currentLevel.player.tries + 1
				fireBall()
			end
		else
			updateBall(dt)
		end
	end
end

local function readInputs()
	local event = input.read()
	while event do
		if event.pressed then
			input.down[event.scancode] = true
		elseif event.released then
			input.down[event.scancode] = false
		end

		event = input.read()
	end
end

local function main()
	-- Load video
	-- Set video scale
	local stat = io.stat("/dev/vga")
	local originalScale = stat.scale
	stat.setScale(1)
	stat.setCursorEnabled(false)
	-- Load values
	stat = io.stat("/dev/vga")
	screen.width = stat.resolution.width
	screen.height = stat.resolution.height
	screen.rectangle = stat.rectangle
	screen.line = stat.line
	screen.flush = stat.flush
	screen.moveCursor = stat.moveCursor
	screen.write = stat.write
	screen.cursorToNextLine = stat.cursorToNextLine
	screen.setTextColor = stat.setTextColor
	stat.setBufferEnabled(true)
	screen.setTextColor(0xFFFFFF, colors.background)

	-- Load input
	local kbd = io.stat("/dev/kbd")
	input.scancodes = kbd.scancodes
	input.read = kbd.readScancode
	input.LEFT = input.scancodes.CURLEFT
	input.RIGHT = input.scancodes.CURRIGHT
	input.UP = input.scancodes.CURUP
	input.DOWN = input.scancodes.CURDOWN
	input.FIRE = input.scancodes.SPACE
	input.ENTER = input.scancodes.ENTER
	input.ESCAPE = input.scancodes.ESCAPE
	input.down = {}

	parseAllSprites()
	loadLevel(currentLevel.level)

	while not exit do
		readInputs()
		updateGame()
		graphics.paint()
	end

	stat.setBufferEnabled(stat.bufferEnabled)
	stat.rectangle(0x000000, 0, 0, screen.width, screen.height)
	stat.setScale(originalScale)
	stat.setTextColor(0xFFFFFF, 0x000000)
	stat.moveCursor(1, 1)
	stat.setCursorEnabled(true)
end

main()
