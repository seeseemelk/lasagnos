local gsrv = {}

function gsrv.openScreen()
	local stat = io.stat("/dev/vga")
	gsrv.screen = stat
end

function gsrv.invalidateAll()
	gsrv.screen.rectangle(0xFF0000, 0, 0, gsrv.screen.resolution.width, gsrv.screen.resolution.height)
end

local function main()
	print("Starting gsrv")

	gsrv.openScreen()
	gsrv.invalidateAll()
	io.read()

	print("Stopping gsrv")
end

main({...})
