local args = {...}

local function cp(src, dest)
	local stat = fs.stat(src)
	local srcfd = assert(fs.open(src, "r"))
	local destfd = assert(fs.open(dest, "w"))

	local buff = assert(srcfd:read(stat.size))
	local success= assert(destfd:write(buff))

	assert(srcfd:close())
	assert(destfd:close())
end

if #args == 2 then
	local source = fs.concat(ENV.CWD, args[1])
	local destination = fs.concat(ENV.CWD, args[2])
	local success, msg = pcall(cp, source, destination)
	if not success then
		print("Fail: " .. msg)
	end
else
	print("Usage: cp source destination")
end
