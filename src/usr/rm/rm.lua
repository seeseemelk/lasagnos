local args = {...}

local function rm(filename)
	local success, err = fs.rm(fs.concat(ENV.CWD, filename))
	if not success then
		print("Failed to remove " .. filename .. ": " .. err)
	end
end

for i, v in ipairs(args) do
	rm(v)
end
