#ifndef TEST2_SYSCALL_H_
#define TEST2_SYSCALL_H_

extern int syscall80(const char* ns);
extern int syscall81(int ns, const char* module);

extern int syscall82_1();
extern int syscall82_2();
extern int syscall82_3();

#endif /* TEST2_SYSCALL_H_ */
