/*
 * stdlib.c
 *
 *  Created on: Jan 3, 2018
 *      Author: seeseemelk
 */
#include "stdlib.h"
#include <stddef.h>
#ifdef KERNEL
#include <kernel.h>
#else
#include "namespace.h"
#include "syscall.h"
#endif

#ifdef KERNEL // Kernel functions

void __attribute__((noreturn)) abort()
{
	kernel_panic("System aborted");
}

#else // Userspace functions

char buf[256] = {0};
char* getenv(const char* name)
{
	int ns = ns_os();
	int fn = ns_os_getenv();
	int length = syscall82_2(ns|fn, name, buf);
	return (length > 0) ? buf : NULL;
}

char* getpenv(const char* name)
{
	int ns = ns_os();
	int fn = ns_os_getpenv();
	int length = syscall82_2(ns|fn, name, buf);
	return (length > 0) ? buf : NULL;
}

int setenv(const char* name, const char* value, int overwrite)
{
	if (!overwrite && getenv(name) == NULL)
		return -1;

	int ns = ns_os();
	int fn = ns_os_setenv();
	syscall82_3(ns|fn, name, value);
	return 0;
}

int setpenv(const char* name, const char* value, int overwrite)
{
	if (!overwrite && getpenv(name) == NULL)
		return -1;

	int ns = ns_os();
	int fn = ns_os_setpenv();
	syscall82_3(ns|fn, name, value);
	return 0;
}

#endif
