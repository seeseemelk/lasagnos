/*
 * string.h
 *
 *  Created on: Jan 3, 2018
 *      Author: seeseemelk
 */

#ifndef STRING_H_
#define STRING_H_

#include <stddef.h>

int memcmp(const void* dest, const void* src, size_t num);
void* memcpy(void* dest, const void* src, size_t num);
void* memchr(const void* str, int c, size_t n);
void* memmove(void* dest, const void* src, size_t num);
void* memset(void* ptr, int value, size_t num);
size_t strlen(const char* str);
int strcmp(const char* str1, const char* str2);
int strncmp(const char* str1, const char* str2, size_t n);
char* strcpy(char* dest, const char* src);
char* strncpy(char* dest, const char* src, size_t num);
char* strndup(const char* str, size_t len);
char* strdup(const char* str);
const char* strstr(const char* haystack, const char* needle);
const char* strchr(const char* str, char c);
unsigned long int strtoul(const char* nptr, char** endptr, int base);
double strtod(const char* nptr, char** endptr);
char* strcat(char* dest, const char* src);
char* strncat(char* dest, const char* src, size_t n);
size_t strspn(const char* str, const char* accept);
size_t strcspn(const char* str, const char* reject);
char* strpbrk(const char* str, const char* accept);
#define strcoll(s1, s2) strcmp(s1, s2)

#endif /* STRING_H_ */
