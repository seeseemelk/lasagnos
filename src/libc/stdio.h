/*
 * stdio.h
 *
 *  Created on: Jan 2, 2018
 *      Author: seeseemelk
 */

#ifndef STDIO_H_
#define STDIO_H_

#include <stdarg.h>
#include "printf.h"

// The k prefix
#ifdef KERNEL
int kputs(const char* str);
int kputchar(char character);
size_t swrite(const void* ptr, size_t size, size_t nmemb);
char* fgets(char* s, int size);
//int kvprintf(const char* format, va_list arg);
//int kprintf(const char* format, ...);

#else // The usermode functions

int puts(const char* str);
int putchar(char character);

typedef int FILE;
FILE fopen(const char* path, const char* mode);
void fclose(FILE fd);
int fwrite(const void* ptr, size_t size, size_t nmemb, FILE fd);

#endif

#endif /* STDIO_H_ */
