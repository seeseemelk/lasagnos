/*
 * stdio.c
 *
 *  Created on: Jan 2, 2018
 *      Author: seeseemelk
 */
#include "stdio.h"
#include "cdefs.h"
#include "syscall.h"

#include <stddef.h>
#include <stdarg.h>
#include <stdbool.h>

#ifdef KERNEL // if kernel
#include <kernel.h>
#include <tty.h>

#define USE_BOCHSOUT

#ifdef USE_BOCHSOUT
#include "io.h"
#define BOCHSOUT(c) outb(0xE9, c)
#endif
#else // if userspace
#include "namespace.h"
#endif


#ifdef KERNEL // if kernel
int kputchar(char c)
{
#ifdef USE_BOCHSOUT
	BOCHSOUT(c);
#endif
	tty_put_char(c);
	return c;
}
#define putchar(c) kputchar(c)
#else // if userspace
int puts(const char* str)
{
	NS io = ns_io();
	FN write = ns_io_write();
	syscall82_1(write | io, str);
	return 0;
}


int putchar(char c)
{
	char str[2] = {c, 0};
	puts(str);
	return c;
}

#endif

size_t swrite(const void* ptr, size_t size, size_t nmemb)
{
	const char* cptr = ptr;
	for (size_t i = 0; i < nmemb; i++)
	{
		putchar((char) *cptr);
		cptr += size;
	}
	return nmemb;
}

char* fgets(char* s, int size)
{
	UNUSED(s);
	UNUSED(size);
#ifdef KERNEL
	kernel_panic("Not supported");
#endif
	return s;
}

#ifndef KERNEL
FILE fopen(const char* path, const char* mode)
{
	NS io = ns_io();
	FN open = ns_io_open();
	return syscall82_2(io|open, path, mode);
}

void fclose(FILE fd)
{
	NS io = ns_io();
	FN close = ns_io_close();
	syscall82_1(io|close, fd);
}

int fwrite(const void* ptr, size_t size, size_t nmemb, FILE fd)
{
	NS io = ns_io();
	FN write = ns_io_write();
	size *= nmemb;
	return syscall82_3(io|write, fd, ptr, size);
}
#endif













