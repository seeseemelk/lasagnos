/*
 * setjmp.h
 *
 *  Created on: Jun 25, 2018
 *      Author: seeseemelk
 */

#ifndef SETJMP_H_
#define SETJMP_H_

typedef int jmp_buf[2];
typedef struct __jmp_buf __jmp_buf;

struct __jmp_buf
{
	//int val;
	int esp;
	int ebp;
};

int setjmp(jmp_buf env);
void longjmp(jmp_buf env, int val);

#endif /* SETJMP_H_ */
