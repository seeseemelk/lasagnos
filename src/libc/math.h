/*
 * math.h
 *
 *  Created on: Jun 25, 2018
 *      Author: seeseemelk
 */

#ifndef MATH_H_
#define MATH_H_

#include <stddef.h>
#define HUGE_VAL __builtin_huge_val()

#define isnan(x) (x != x)

size_t ceildiv(size_t a, size_t b);
int abs(int j);
double fabs(double x);
double floor(double t);
double ceil(double t);
double pow(double x, double y);
double sin(double x);
double sinh(double x);
double cos(double x);
double cosh(double x);
double tan(double x);
double tanh(double x);
double asin(double x);
double acos(double x);
double atan(double x);
double atan2(double x, double y);
double fmod(double x, double y);
double modf(double x, double* iptr);
double sqrt(double x);
double log(double x);
double log10(double x);
double log2(double x);
double exp(double x);
double ldexp(double x, int exp);
double frexp(double x, int* exp);
double scalbn(double x, int n);

#endif /* MATH_H_ */
