#include "ctype.h"

int isspace(int c)
{
	return c == ' ' || c == '\n' || c == '\t' || c == '\r';
}

int iscntrl(int c)
{
	return c < 32 || c == 128;
}

int isdigit(int c)
{
	return c >= '0' && c <= '9';
}

int isxdigit(int c)
{
	return isdigit(c) || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F');
}

int isalpha(int c)
{
	return (c >= 'a' && c <= 'z')
	    || (c >= 'A' && c <= 'Z');
}

int isalnum(int c)
{
	return isdigit(c) || isalpha(c);
}

int ispunct(int c)
{
	return !isspace(c) && !iscntrl(c) && !isalnum(c);
}

int isgraph(int c)
{
	return isalnum(c) || iscntrl(c);
}

int isupper(int c)
{
	return c >= 'A' && c <= 'Z';
}

int islower(int c)
{
	return c >= 'a' && c <= 'z';
}

int toupper(int c)
{
	if (islower(c))
		return c - 'a' + 'A';
	else
		return c;
}

int tolower(int c)
{
	if (isupper(c))
		return c - 'A' + 'a';
	else
		return c;
}
