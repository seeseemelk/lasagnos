/*
 * ctype.h
 *
 *  Created on: Jun 25, 2018
 *      Author: seeseemelk
 */

#ifndef CTYPE_H_
#define CTYPE_H_

int isspace(int c);
int iscntrl(int c);
int isdigit(int c);
int isxdigit(int c);
int isalpha(int c);
int isalnum(int c);
int ispunct(int c);
int isgraph(int c);
int isupper(int c);
int islower(int c);

int toupper(int c);
int tolower(int c);

#endif /* CTYPE_H_ */
