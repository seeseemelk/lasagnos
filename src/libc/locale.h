/*
 * locale.h
 *
 *  Created on: Jun 25, 2018
 *      Author: seeseemelk
 */

#ifndef LOCALE_H_
#define LOCALE_H_

typedef struct lconv lconv;

struct lconv
{
	char* decimal_point;
};

lconv* localeconv();

#endif /* LOCALE_H_ */
