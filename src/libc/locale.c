#include "locale.h"
#include <stddef.h>

char decimal_points = '.';
lconv system_locale = {
	.decimal_point = &decimal_points
};

lconv* localeconv()
{
	return &system_locale;
}
