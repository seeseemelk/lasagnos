#include "setjmp.h"

int setjmp(jmp_buf env)
{
	__jmp_buf* jenv = (__jmp_buf*) env;

	register int esp asm("esp");
	register int ebp asm("ebp");

	jenv->esp = esp;
	jenv->ebp = ebp;
	return 0;
}

void longjmp(jmp_buf env, int val)
{
	__jmp_buf* jenv = (__jmp_buf*) env;

	asm(
		"mov %0, %%esp\n"
		"mov %1, %%ebp\n"
		"mov %2, %%eax\n"
		"leave\n"
		"ret"
		:
		: "r" (jenv->esp), "r" (jenv->ebp), "r" (val)
	);
}
