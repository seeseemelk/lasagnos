/*
 * namespaces.h
 *
 *  Created on: Oct 16, 2018
 *      Author: seeseemelk
 */

#ifndef NAMESPACE_H_
#define NAMESPACE_H_
#ifndef KERNEL

#include "cdefs.h"
typedef u16 NS;
typedef u16 FN;

#ifndef NSF
#define NSF(name, str) \
NS ns_##name();

#define FNF(nsname, name, str) \
FN ns_##nsname##_##name();
#endif

// IO Namespace
NSF(io, "io")
FNF(io, open, "open")
FNF(io, close, "close")
FNF(io, write, "write")
FNF(io, read, "read")

NSF(os, "os")
FNF(os, getenv, "getenv")
FNF(os, setenv, "setenv")
FNF(os, getpenv, "getpenv")
FNF(os, setpenv, "setpenv")

#endif
#endif /* NAMESPACE_H_ */
