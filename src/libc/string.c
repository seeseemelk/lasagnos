/*
 * string.c
 *
 *  Created on: Jan 3, 2018
 *      Author: seeseemelk
 */
#include "string.h"
#include "stdio.h"
#include "stdlib.h"
#include "ctype.h"
#include "cdefs.h"
#include <stdbool.h>

#ifdef KERNEL
#include <memory.h>
#endif

#ifndef __STDC_LIMIT_MACROS
#define __STDC_LIMIT_MACROS
#endif
#include <stdint.h>
#ifndef SIZE_MAX
#define SIZE_MAX 4
#endif

int memcmp(const void* dest, const void* src, size_t num)
{
	const u8* cdest = dest;
	const u8* csrc = src;

	for (size_t i = 0; i < num; i++)
	{
		u8 distance = cdest[i] - csrc[i];
		if (distance != 0)
			return distance;
	}
	return 0;
}

void* memcpy(void* dest, const void* src, size_t num)
{
	char* c_dest = (char*) dest;
	char* c_src = (char*) src;

	for (size_t i = 0; i < num; i++)
		c_dest[i] = c_src[i];

	return dest;
}

void* memchr(const void* str, int c, size_t n)
{
	const u8* bstr = str;
	u8 b = (u8) c;

	for (size_t i = 0; i < n; i++)
	{
		if (bstr[i] == b)
			return (void*) &bstr[i];
	}

	return NULL;
}

void* memmove(void* dest, const void* src, size_t num)
{
	char* c_dest = (char*) dest;
	char* c_src = (char*) src;

	if (src > dest)
	{
		for (size_t i = 0; i < num; i++)
			c_dest[i] = c_src[i];
	}
	else
	{
		for (size_t i = num-1; i > 0; i--)
			c_dest[i] = c_src[i];
		c_dest[0] = c_src[0];
	}
	return dest;
}

void* memset(void* ptr, int value, size_t num)
{
	char* c_ptr = (char*) ptr;
	for (size_t i = 0; i < num; i++)
	{
		c_ptr[i] = (char) value;
	}
	return ptr;
}

size_t strlen(const char* str)
{
	int i = 0;
	while (str[i] != 0)
		i++;
	return i;
}

int strcmp(const char* str1, const char* str2)
{
	size_t len1 = strlen(str1);
	size_t len2 = strlen(str2);
	size_t len = len1 < len2 ? len1 : len2;
	int v = strncmp(str1, str2, len);
	if (v == 0 && len1 != len2)
		return len1 < len2 ? -1 : 1;
	else
		return v;
}

int strncmp(const char* str1, const char* str2, size_t n)
{
	size_t len1 = strlen(str1);
	size_t len2 = strlen(str2);
	if (len1 < n)
		n = len1;
	if (len2 < n)
		n = len2;

	for (size_t i = 0; i < n; i++)
	{
		if (str1[i] < str2[i])
			return -1;
		else if (str1[i] > str2[i])
			return 1;
	}
	return 0;
}

char* strcpy(char* dest, const char* src)
{
	size_t len = strlen(src);
	/* We do '+ 1' to include the null-terminator. */
	return strncpy(dest, src, len + 1);
}

char* strncpy(char* dest, const char* src, size_t num)
{
	bool ended = false;
	for (size_t i = 0; i < num; i++)
	{
		if (!ended)
		{
			dest[i] = src[i];
			if (src[i] == 0)
				ended = true;
		}
		else
			dest[i] = 0;
	}
	return dest;
}

#ifdef KERNEL
char* strndup(const char* src, size_t len)
{
	size_t srclen = strlen(src);
	len = (len < srclen) ? len : srclen;
	char* dest = malloc(len);

	/* '<=' is used to also copy the null terminator */
	for (size_t i = 0; i <= len; i++)
		dest[i] = src[i];

	return dest;
}

char* strdup(const char* src)
{
	return strndup(src, SIZE_MAX);
}
#endif

const char* strstr(const char* haystack, const char* needle)
{
	const char* beginning = NULL;
	const char* substr = needle;
	while (haystack != 0)
	{
		if (*haystack == *substr)
		{
			if (beginning == NULL)
				beginning = haystack;
			substr++;
		}
		else
		{
			beginning = NULL;
			substr = needle;
		}

		if (*substr == '\0')
			return haystack;

		haystack++;
	}
	return (char*) NULL;
}

const char* strchr(const char* str, char c)
{
	while (*str != 0)
	{
		if (*str == c)
			return str;
		str++;
	}
	return NULL;
}

unsigned long int strtoul(const char* nptr, char** endptr, int base)
{
	unsigned long int num = 0;
	bool valid = true;
	bool negate = false;

	// Ignore leading spaces
	while (isspace(*nptr))
		nptr++;

	// Negate if it starts with a '-'. Ignore a '+'
	if (nptr[0] == '-')
	{
		nptr++;
		negate = true;
	}
	else if (nptr[0] == '+')
		nptr++;

	// Move nptr if necessary
	if (base == 16)
	{
		if (nptr[0] == '0' && (nptr[1] == 'x' || nptr[1] == 'X'))
			nptr += 2;
	}
	else if (base == 0)
	{
		if (nptr[0] == '0')
		{
			if (nptr[1] == 'x' || nptr[1] == 'X')
			{
				nptr += 2;
				base = 16;
			}
			else
				base = 8;
		}
		else
			base = 10;
	}

	char c = 0;
	do
	{
		c = *(nptr++);
		if (c >= '0' && c <= '9')
		{
			if (c - '0' >= base)
			{
				valid = false;
			}
			else
			{
				num = (num * base) + (c - '0');
			}
		}
		else if (c >= 'A' && c <= 'F')
		{
			if (c - 'A' + 10 >= base)
			{
				valid = false;
			}
			else
			{
				num = (num * base) + (c - 'A' + 10);
			}
		}
	}
	while (valid && c != 0);
	*endptr = (char*) nptr - 1;

	if (negate)
		return (unsigned long int) (-(long int)num);
	else
		return num;
}

char* strcat(char* dest, const char* src)
{
	char* beginning = dest;

	dest += strlen(dest);
	while (*src != 0)
	{
		*(dest++) = *(src++);
	}
	*dest = '\0';

	return beginning;
}

char* strncat(char* dest, const char* src, size_t n)
{
	char* beginning = dest;

	dest += strlen(dest);
	while (*src != 0 && n > 0)
	{
		*(dest++) = *(src++);
		n--;
	}
	*dest = '\0';

	return beginning;
}

size_t strspn(const char* str, const char* accept)
{
	size_t n = 0;

	size_t len_accept = strlen(accept);
	while (str[n] != 0)
	{
		for (size_t i = 0; i < len_accept; i++)
		{
			if (str[n] != accept[n])
				goto break_fully;
		}
		n++;
	}
	break_fully:

	return n;
}

size_t strcspn(const char* str, const char* reject)
{
	size_t n = 0;

	size_t len_reject = strlen(reject);
	while (str[n] != 0)
	{
		for (size_t i = 0; i < len_reject; i++)
		{
			if (str[n] == reject[n])
				goto break_fully;
		}
		n++;
	}
	break_fully:

	return n;
}

char* strpbrk(const char* str, const char* accept)
{
	size_t n = 0;

	size_t len_accept = strlen(accept);
	while (str[n] != 0)
	{
		for (size_t i = 0; i < len_accept; i++)
		{
			if (str[n] == accept[n])
				return *((char**) &str[n]);
		}
		n++;
	}
	return NULL;
}
























