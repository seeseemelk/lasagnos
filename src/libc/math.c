#include "math.h"
#include "cdefs.h"

#ifdef KERNEL
#include <kernel.h>
#endif

size_t ceildiv(size_t a, size_t b)
{
	return (a - 1) / b + 1;
}

int abs(int j)
{
	if (j >= 0)
		return j;
	else
		return -j;
}

double fabs(double x)
{
	if (x >= 0)
		return x;
	else
		return -x;
}

double floor(double t)
{
	//return (double) (long long) t;
	return __builtin_floor(t);
}

double ceil(double t)
{
	UNUSED(t);
	//kernel_panic("Unsupported command");
	//return -999.0;
	return __builtin_ceil(t);
}

long double powl(long double x, long double y)
{
	return __builtin_powl(x, y);
}

double pow(double x, double y)
{
	return (double) powl((long double) x, (long double) y);
}

/*
double sin(double x)
{
	UNUSED(x);
#ifdef KERNEL
	kernel_panic("Unsupported command");
#endif
	return -999.0;
}
*/

double sinh(double x)
{
	UNUSED(x);
#ifdef KERNEL
	kernel_panic("Unsupported command");
#endif
	return -999.0;
}

/*
double cos(double x)
{
	UNUSED(x);
#ifdef KERNEL
	kernel_panic("Unsupported command");
#endif
	return -999.0;
}
*/

double cosh(double x)
{
	UNUSED(x);
#ifdef KERNEL
	kernel_panic("Unsupported command");
#endif
	return -999.0;
}

double tan(double x)
{
	UNUSED(x);
#ifdef KERNEL
	kernel_panic("Unsupported command");
#endif
	return -999.0;
}

double tanh(double x)
{
	UNUSED(x);
#ifdef KERNEL
	kernel_panic("Unsupported command");
#endif
	return -999.0;
}

double asin(double x)
{
	UNUSED(x);
#ifdef KERNEL
	kernel_panic("Unsupported command");
#endif
	return -999.0;
}

double acos(double x)
{
	UNUSED(x);
#ifdef KERNEL
	kernel_panic("Unsupported command");
#endif
	return -999.0;
}

/*
double atan(double x)
{
	UNUSED(x);
#ifdef KERNEL
	kernel_panic("Unsupported command");
#endif
	return -999.0;
}

double atan2(double x, double y)
{
	UNUSED(x);
	UNUSED(y);
#ifdef KERNEL
	kernel_panic("Unsupported command");
#endif
	return -999.0;
}
*/

double modf(double x, double* iptr)
{
	UNUSED(x);
	UNUSED(iptr);
#ifdef KERNEL
	kernel_panic("Unsupported command");
#endif
	return -999.0;
}

double log(double x)
{
	UNUSED(x);
#ifdef KERNEL
	kernel_panic("Unsupported command");
#endif
	return -999.0;
}

double log10(double x)
{
	UNUSED(x);
#ifdef KERNEL
	kernel_panic("Unsupported command");
#endif
	return -999.0;
}

double log2(double x)
{
	return __builtin_log2(x);
}

double exp(double x)
{
	UNUSED(x);
#ifdef KERNEL
	kernel_panic("Unsupported command");
#endif
	return -999.0;
}

double ldexp(double x, int exp)
{
	UNUSED(x);
	UNUSED(exp);
#ifdef KERNEL
	kernel_panic("Unsupported command");
#endif
	return -999.0;
}
/*
double frexp(double x, int* exp)
{
	UNUSED(x);
	UNUSED(exp);
#ifdef KERNEL
	kernel_panic("Unsupported command");
#endif
	return -999.0;
}
*/

























