/*
 * errno.h
 *
 *  Created on: Jun 25, 2018
 *      Author: seeseemelk
 */

#ifndef ERRNO_H_
#define ERRNO_H_

extern int errno;

#define ERANGE 1
#define ENOMEM 2

#endif /* ERRNO_H_ */
