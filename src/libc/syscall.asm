;extern int syscall80(const char* ns);
;extern int syscall81(int ns, const char* module);
;extern int syscall82_3(int ns, int module, int a, int b, int c);
;extern int syscall82_2(int ns, int module, int a, int b);
;extern int syscall82_1(int ns, int module, int a);

global syscall80
global syscall81
global syscall82_1
global syscall82_2
global syscall82_3

syscall80:
	enter 0, 0

	xor eax, eax
	mov edx, [ebp+8]
	int 0x80

	leave
	ret

syscall81:
	enter 0, 0

	mov eax, [ebp+8]
	mov edx, [ebp+12]
	int 0x81

	leave
	ret

syscall82_1:
	enter 0, 0
	mov eax, [ebp+8]
	mov edx, [ebp+12]
	int 0x82
	leave
	ret

syscall82_2:
	enter 0, 0
	mov eax, [ebp+8]
	mov edx, [ebp+12]
	mov ecx, [ebp+16]
	int 0x82
	leave
	ret

syscall82_3:
	enter 0, 0
	push ebx
	mov eax, [ebp+8]
	mov edx, [ebp+12]
	mov ecx, [ebp+16]
	mov ebx, [ebp+20]
	int 0x82
	pop ebx
	leave
	ret
