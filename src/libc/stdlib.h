/*
 * stdlib.h
 *
 *  Created on: Jun 27, 2018
 *      Author: seeseemelk
 */

#ifndef STDLIB_H_
#define STDLIB_H_

#ifdef KERNEL
#include <memory.h>
#endif

#ifdef KERNEL // Kernel functions

void __attribute__((noreturn)) abort();

#else // Userspace functions

char* getenv(const char* name);
char* getpenv(const char* name);
int setenv(const char* name, const char* value, int overwrite);
int setpenv(const char* name, const char* value, int overwrite);

#endif

#endif /* STDLIB_H_ */
