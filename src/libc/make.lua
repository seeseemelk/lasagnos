tasks.add("build_libc", function()
	local output = fs.concat(dir.build, "libc.o")
	local input = dir.libc

	local files = action.c.findSources(input)
	action.c.findSources(fs.concat(input, "musl/internal"), files)
	action.c.findSources(fs.concat(input, "musl/math"), files)

	local objects = action.c.build(files, nil, {
		defines = {"KERNEL"}
	})
	util.concat(var.objects, action.c.linkObj(output, objects))
end)

tasks.add("build_ulibc", function()
	tasks.withVar({
		fsprefix = "ulibc"
	}, function()
		local archive = var.ulibc
		local output = fs.concat(dir.build, dir.src, "ulibc", "ulibc.o")

		local input = dir.libc
		local sources = action.c.findSources(input)
		local objects = action.c.build(sources, nil, {
			build = fs.concat(dir.build, dir.src, "ulibc"),
			includes = {}
		})
		action.nasm.build(action.nasm.findSources(input), objects)

		action.c.linkObj(output, objects)
		action.c.staticLib(archive, output)
	end)
end)
