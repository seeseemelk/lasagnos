#ifndef KERNEL
#include "syscall.h"

// Macros to define namespaces.
#define NSF(name, ns) \
	NS ns_##name() \
	{ \
		static NS n = 0; \
		if (n == 0) \
			n = syscall80(ns); \
		return n; \
	}

#define FNF(nsname, name, fn) \
	FN ns_##nsname##_##name() \
	{ \
		static NS n = 0; \
		if (n == 0) \
			n = syscall81(ns_##nsname(), fn); \
		return n; \
	}

// Namespace definitions are in namespace.h.
#include "namespace.h"

#endif
