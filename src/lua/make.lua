tasks.add("build_lua", function()
	local output = fs.concat(dir.build, "lua.o")
	local input = dir.lua
	local objects = action.c.build(action.c.findSources(input), {}, {
		cflags = "-DKERNEL"	
	})
	util.concat(var.objects, action.c.linkObj(output, objects))
end)
