#!/bin/lua
package.path = package.path .. ";tools/makefont/?.lua"
require("png")

local args = {...}
local input = args[1]
local output = args[2]

local image = pngImage(input)

local function isOn(x, y)
	return image:getPixel(x, y).R ~= 255
end

local letterWidth = image.width / 16
local letterHeight = image.height / 16

local fh = io.open(output, "w")

local function writeLetter(x, y)
	local ox = (x-1) * letterWidth
	local oy = (y-1) * letterHeight

	for iy = 1, letterHeight do
		fh:write("\t\t")
		for ix = 1, letterWidth do
			if isOn(ox+ix, oy+iy) then
				fh:write("1,")
			else
				fh:write("0,")
			end
		end
		fh:write("\n")
	end
end

fh:write("local font = new.module(\"font\", {\n")
fh:write("\twidth = " .. string.format("%d", letterWidth) .. ",\n")
fh:write("\theight = " .. string.format("%d", letterHeight) .. ",\n")

for y = 1, 16 do
	local high = (y - 1) * 0x10
	for x = 1, 16 do
		local low = (x - 1)
		local byte = high | low
		local char = string.char(byte)
		fh:write("\t[string.char(" .. byte .. ")] = {\n")
		writeLetter(x, y)
		fh:write("\t},\n")
	end
end
fh:write("})\n")
fh:close()
