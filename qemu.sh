#!/bin/sh
set -e
./make.lua build_iso
if [[ -f disk.img ]]
then
	qemu-system-i386 -cdrom lasagnos.iso -boot order=dc -m 128M -gdb tcp::1234 -serial stdio -hda disk.img $*
else
	qemu-system-i386 -cdrom lasagnos.iso -boot order=dc -m 128M -gdb tcp::1234 -serial stdio $*
fi

